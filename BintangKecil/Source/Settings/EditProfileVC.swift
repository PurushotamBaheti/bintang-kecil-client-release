//
//  EditProfileVC.swift
//  BintangKecil
//
//  Created by Purushotam on 23/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import MobileCoreServices

protocol editProfileDelegate{
    func profileUpdatedSuccessfully()
}

class EditProfileVC: BaseViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var profileImgView: UIImageView!
    var picker:UIImagePickerController?=UIImagePickerController()
    var userImage : UIImage? = nil
    var delegate : editProfileDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomiseUI()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    func CustomiseUI(){
        nameTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        emailTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        mobileTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))

        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
        let mobilenum = decodedData.mobile!
        let trimmedMobile  = "+" + mobilenum
        mobileTxtField.text = trimmedMobile
        emailTxtField.text = decodedData.email
        nameTxtField.text = decodedData.name
        if(decodedData.image.isEmpty){
            profileImgView.image = UIImage.init(named: "noImage")
        }
        else{
            profileImgView.sd_setImage(with: URL(string: decodedData.image), placeholderImage: UIImage(named: "noImage"))
        }
    }

    @IBAction func SubmitBtnClicked(){
        self.view.endEditing(true)
        if(self.ValidateTextFields() == true){
            self.CallEditProfileApi()
        }
    }

    @IBAction func EditBtnClicked(){
        nameTxtField.becomeFirstResponder()
    }

    func ValidateTextFields() -> Bool{
        if((nameTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_NAME)
            return false
        }
        return true
    }

    func CallEditProfileApi(){
        var parameter = [String:Any]()
        parameter["name"] = self.nameTxtField.text!

        var TotalArray = [[String : Any]]()
        if(self.userImage != nil){
            let dict = ["img":self.userImage as Any,"key":"image"]
            TotalArray.append(dict)
        }

        showLoader()

        uploadData(strUrl: "profile-update", arrImage: TotalArray, param: parameter, success: { (json) in
            if json["status"].stringValue == "200"{
                let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
                let userWrapper = UserModal.init(id: json["data"]["id"].intValue, image: json["data"]["image"].stringValue, email: json["data"]["email"].stringValue, image_thumbnail: json["data"]["image_thumbnail"].stringValue, accessToken: decodedData.accessToken, user_code: json["data"]["user_code"].stringValue, name: json["data"]["name"].stringValue, mobile: json["data"]["mobile"].stringValue)

                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userWrapper)
                UserDefaults.standard.set(encodedData, forKey: "userdata")
                UserDefaults.standard.synchronize()
                self.delegate?.profileUpdatedSuccessfully()
                self.navigationController?.popViewController(animated: true)
                self.showToast(message: "Profile updated successfully!")

            }
            self.hideLoader()

        }, failure: {
            (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg as! String)
        })
    }

    @IBAction func ImageBtnAction(){
        let alert:UIAlertController=UIAlertController(title: "Edit Image Options", message: nil, preferredStyle: UIAlertController.Style.actionSheet)

        let gallaryAction = UIAlertAction(title: "Open Gallery", style: UIAlertAction.Style.default){
            UIAlertAction in self.openGallary()
        }
        let CameraAction = UIAlertAction(title: "Open Camera", style: UIAlertAction.Style.default){
            UIAlertAction in self.openCamera()
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel){
            UIAlertAction in self.cancel()

        }
        alert.addAction(gallaryAction)
        alert.addAction(CameraAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func openGallary(){
        picker?.delegate = self
        picker!.allowsEditing = true
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerController.SourceType.camera;
            imag.mediaTypes = [kUTTypeImage] as [String]
            imag.allowsEditing = true

            self.present(imag, animated: true, completion: nil)
        }
    }
    func cancel(){
        print("Cancel Clicked")
    }

    private func imagePickerControllerDidCancel(picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        let image = info[.originalImage] as! UIImage
        userImage = image
        self.profileImgView.image = image
    }
}
