//
//  ChangePasswordVC.swift
//  BintangKecil
//
//  Created by Purushotam on 22/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class ChangePasswordVC: BaseViewController {
    
    @IBOutlet weak var oldPasswordTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var confirmPasswordTxtField: UITextField!
    @IBOutlet weak var showOldPasswordBtn: UIButton!
    @IBOutlet weak var showNewPasswordBtn: UIButton!
    @IBOutlet weak var showConfirmPasswordBtn: UIButton!
    var validation = Validation()
    var isShowOldPassword : Bool = false
    var isShowNewPassword : Bool = false
    var isShowConfirmPassword : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func customiseUI(){
        oldPasswordTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        passwordTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        confirmPasswordTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
    }
    
    @IBAction func ShowNewPasswordClicked(){
        if(isShowNewPassword == false){
            isShowNewPassword = true
            self.passwordTxtField.isSecureTextEntry = false
            showNewPasswordBtn.setImage(UIImage.init(named:"password_hide_black"), for: .normal)
        }
        else{
            isShowNewPassword = false
            self.passwordTxtField.isSecureTextEntry = true
            showNewPasswordBtn.setImage(UIImage.init(named:"password_visible_black"), for: .normal)
        }
    }
    
    @IBAction func ShowConfirmPasswordClicked(){
        if(isShowConfirmPassword == false){
            isShowConfirmPassword = true
            self.confirmPasswordTxtField.isSecureTextEntry = false
            showConfirmPasswordBtn.setImage(UIImage.init(named:"password_hide_black"), for: .normal)
        }
        else{
            isShowConfirmPassword = false
            self.confirmPasswordTxtField.isSecureTextEntry = true
            showConfirmPasswordBtn.setImage(UIImage.init(named:"password_visible_black"), for: .normal)
        }
    }
    @IBAction func ShowOldPasswordClicked(){
        if(isShowOldPassword == false){
            isShowOldPassword = true
            self.oldPasswordTxtField.isSecureTextEntry = false
            showOldPasswordBtn.setImage(UIImage.init(named:"password_hide_black"), for: .normal)
        }
        else{
            isShowOldPassword = false
            self.oldPasswordTxtField.isSecureTextEntry = true
            showOldPasswordBtn.setImage(UIImage.init(named:"password_visible_black"), for: .normal)
        }
    }
    
    @IBAction func SubmitBtnClicked(){
        self.view.endEditing(true)
        if(self.ValidateTextFields() == true){
            self.callChangePasswordApi()
        }
    }
    
    func ValidateTextFields() -> Bool{
        if((oldPasswordTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_OLD_PASSWORD)
            return false
        }
        else if((passwordTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_NEW_PASSWORD)
            return false
        }
        else if(validation.validatePassword(password:self.passwordTxtField.text!) == false){
            self.showToast(message: MessageString.INVALID_PASSWORD)
            return false
        }
        else if((confirmPasswordTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_CONFIRM_PASSWORD)
            return false
        }
        else if(passwordTxtField.text != confirmPasswordTxtField.text){
            self.showToast(message: MessageString.MISMATCH_NEW_CONFIRM_PASSWORD)
            return false
        }
        return true
    }
    
    func callChangePasswordApi(){
        var parameter = [String:Any]()
        parameter["old_password"] = self.oldPasswordTxtField.text!
        parameter["new_password"] = self.passwordTxtField.text!
        
        showLoader()
        postRequest(strUrl: "change-password", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                
                let alertController = UIAlertController(title: "Alert", message: "Password changed successfully.Please login again to verify.", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                    UserDefaults.standard.setLogin(false)
                    UserDefaults.standard.synchronize()
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                    appDelegate.window?.rootViewController = dashboard
                }
                
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }
            
        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
}
