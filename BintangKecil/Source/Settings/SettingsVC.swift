//
//  SettingsVC.swift
//  BintangKecil
//
//  Created by Purushotam on 22/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class SettingsVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var navView: MyView!
    @IBOutlet weak var tblView: UITableView!
    var dataArray = ["", "Change Password", "Logout"]
    var isNotificationOn : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.estimatedRowHeight = 80
        self.tblView.rowHeight = UITableView.automaticDimension

        // Do any additional setup after loading the view.
    }


    override func viewWillAppear(_ animated: Bool) {

        self.tabBarController?.tabBar.isHidden = true
        self.getUserSettings()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "notifyCell", for: indexPath as IndexPath) as! notifyCell
            if(isNotificationOn == true){
                cell.notificationSwitch.isOn = true
            }
            else{
                cell.notificationSwitch.isOn = false
            }
            cell.notificationSwitch.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath as IndexPath) as! ProfileCell
            cell.titleLbl.text = self.dataArray[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if(indexPath.row == 1){
            let isChild = UserDefaults.standard.getChild()
            if isChild{
                self.showToast(message: "You are not authorized to access this feature. You must have parent account access for this.")
            }
            else{
                CheckIfPasswordExists()
            }
        }
        else if(indexPath.row == 2){
            let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to Logout?", preferredStyle: .alert)

            // Create OK button with action handler
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                self.CallLogoutApi()

            })

            // Create Cancel button with action handlder
            let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in

            }

            //Add OK and Cancel button to dialog message
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)

            // Present dialog message to user
            self.present(dialogMessage, animated: true, completion: nil)
        }
    }

    @objc func switchValueDidChange(_ sender: UISwitch!) {

        let isChild = UserDefaults.standard.getChild()
        if isChild{

            self.tblView.reloadData()
            self.showToast(message: "You are not authorized to access this feature. You must have parent account access for this.")
        }
        else{

            if (sender.isOn == true){
                self.isNotificationOn = true
                self.saveUserSettings()
            }
            else{
                self.isNotificationOn = false
                self.saveUserSettings()
            }
        }

    }

    func CheckIfPasswordExists(){
        showLoader()
        getRequest(strUrl: "is-password-set", success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "changepassword") as! ChangePasswordVC
                self.navigationController?.pushViewController(vc, animated: false)

            }
            else{
                let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "setpassword") as! SetPasswordVC
                vc.userMobileNumber = decodedData.mobile
                vc.isFromChangePassword = true
                self.navigationController?.pushViewController(vc, animated: false)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func getUserSettings(){
        showLoader()
        getRequest(strUrl: "user-settings", success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                let notificationvalue = json["data"]["user_settings"]["user_notifications"]["value"].stringValue
                if(notificationvalue == "1"){
                    self.isNotificationOn = true
                }
                else{
                    self.isNotificationOn = false
                }
                self.tblView.reloadData()
            }
            else{

            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func saveUserSettings(){

        var  tempparameter = [String:Any]()
        if(self.isNotificationOn == true){
            tempparameter["user_notifications"] = 1
        }
        else{
            tempparameter["user_notifications"] = 0
        }

        var parameter = [String:Any]()
        parameter["user_settings"] = tempparameter

        showLoader()
        postRequest(strUrl: "user-settings", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                self.showToast(message:"Notifications settings updated successfully.")

            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func CallLogoutApi(){
        showLoader()
        let urlstr = "logout?tokenId=" + DEVICE_ID!

        getRequest(strUrl: urlstr, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                UserDefaults.standard.removeObject(forKey:"childdata")
                UserDefaults.standard.removeObject(forKey: "userdata")
                UserDefaults.standard.setChild(false)
                UserDefaults.standard.setLogin(false)
                UserDefaults.standard.synchronize()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                appDelegate.window?.rootViewController = dashboard

            }
            else{
                UserDefaults.standard.removeObject(forKey:"childdata")
                UserDefaults.standard.removeObject(forKey: "userdata")
                UserDefaults.standard.setChild(false)
                UserDefaults.standard.setLogin(false)
                UserDefaults.standard.synchronize()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                appDelegate.window?.rootViewController = dashboard

            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

}

class notifyCell: UITableViewCell {

    @IBOutlet weak var notificationSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }

}
