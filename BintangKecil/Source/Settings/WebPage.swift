//
//  WebPage.swift
//  BintangKecil
//
//  Created by Purushotam on 14/09/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import WebKit

protocol paymentDelegate: class {
    //always triggers when the OTP field is valid
    func paymentFailed()
}

class WebPage: BaseViewController,WKNavigationDelegate, WKUIDelegate,WKScriptMessageHandler {

    @IBOutlet weak var navView: MyView!
    @IBOutlet  var headingLbl : UILabel!
    var webView : WKWebView!
    weak var delegate: paymentDelegate?

    var pageUrl = ""
    var pageTitle = ""
    let doStuffMessageHandler = "doStuffMessageHandler"
    var isFromAddChild : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomiseUI()

        // Do any additional setup after loading the view.
    }

    func CustomiseUI(){
        var ycoordinate :CGFloat = 65
        if(ScreenHeight > 811){
            ycoordinate = 85
        }

        showLoader()

        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.userContentController.add(self, name: doStuffMessageHandler)
        configuration.preferences = preferences

        let frame : CGRect = CGRect(x: 0, y: ycoordinate, width: ScreenWidth, height: ScreenHeight - ycoordinate)
        webView = WKWebView(frame: frame, configuration: configuration)


        self.view.addSubview(webView)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        let url = URL(string: pageUrl)
        webView.load(URLRequest(url: url!))
        self.view.bringSubviewToFront(webView)



        self.headingLbl.text = self.pageTitle
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

         self.hideLoader()
    }

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {

        if(message.name == doStuffMessageHandler)
        {
            let dict = message.body as? [String : AnyObject]
            let response  = dict!["param1"] as? String
            var message = ""
            if(response == "success"){
               message = "Subscription Purchased successfully."
                let dialogMessage = UIAlertController(title: "Bintang Kecil", message: message, preferredStyle: .alert)

                // Create OK button with action handler
                let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in

                    if(self.isFromAddChild == true){
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: AddChildVC.self) {
                                _ =  self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                    else{
                        self.navigationController?.popToRootViewController(animated: true)
                    }

                })

                //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)

                // Present dialog message to user
                self.present(dialogMessage, animated: true, completion: nil)

            }else {

                self.delegate?.paymentFailed()
                self.navigationController?.popViewController(animated: true)

            }




        }

    }

}
