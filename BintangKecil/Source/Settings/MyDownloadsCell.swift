//
//  MyDownloadsCell.swift
//  BintangKecil
//
//  Created by Purushotam on 10/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class MyDownloadsCell: UITableViewCell {

    @IBOutlet weak var songNameLbl: UILabel!
    @IBOutlet weak var songDateLbl: UILabel!
    @IBOutlet weak var songImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
