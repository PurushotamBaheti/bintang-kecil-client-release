//
//  MyDownloadVC.swift
//  BintangKecil
//
//  Created by Purushotam on 10/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage

class MyDownloadVC: BaseViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,AVPlayerViewControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    var dataAudioArray = [MediaModel]()
    var dataVideoArray = [MediaModel]()
    var playerController = AVPlayerViewController()
    @IBOutlet weak var ImageTop: NSLayoutConstraint!
    @IBOutlet weak var userImg: UIImageView!
    let margin: CGFloat = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadImage()
        self.customiseProfileImgView()
        self.InitialiseData()

        // Do any additional setup after loading the view.
    }

    func customiseProfileImgView(){
        if(ScreenHeight > 811){
            ImageTop.constant = 52
        }
    }

    func loadImage() {
        let isChild = UserDefaults.standard.getChild()
        if isChild{

            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            if(decodedData.image_url! == "") {
                userImg.image = UIImage.init(named: "noImage")
            }
            else {
                userImg.sd_setImage(with: URL(string: decodedData.image_url!), placeholderImage: UIImage(named: "noImage"))
            }
        }
        else {
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
            if(decodedData.image.isEmpty) {
                userImg.image = UIImage.init(named: "noImage")
            }
            else {
                userImg.sd_setImage(with: URL(string: decodedData.image), placeholderImage: UIImage(named: "noImage"))
            }
        }
    }



    func InitialiseData(){

        // Do any additional setup after loading the view.

        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)

        if let _ = UserDefaults.standard.object(forKey: "video"){
            self.dataVideoArray.removeAll()
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "video") as! Data) as! [MediaModel]
            self.dataVideoArray.append(contentsOf: decodedData)
            self.collectionView.reloadData()

        }
        else{
            self.dataVideoArray.removeAll()
            self.collectionView.reloadData()
            self.CreateNoFiltersView()

        }
    }

    func CreateNoFiltersView() {
        let popupview = UIView()
        popupview.backgroundColor = UIColor.clear
        self.collectionView.backgroundView = popupview

        let ycor = (ScreenHeight - self.collectionView.frame.origin.y)/2

        let subview = UIView()
        subview.frame = CGRect.init(x:30, y: ycor - 160, width: popupview.frame.size.width - 60, height: 160)
        subview.backgroundColor = UIColor.clear
        popupview.addSubview(subview)

        let logo = UIImageView.init()
        logo.frame = CGRect.init(x:(subview.frame.size.width - 80)/2, y:20, width: 80, height: 80)
        logo.image = UIImage.init(named:"logo")
        logo.contentMode = .scaleAspectFit
        subview.addSubview(logo)

        let messageLbl = UILabel()
        messageLbl.frame = CGRect.init(x: 5, y: 90, width: subview.frame.size.width - 10, height: 50)
        messageLbl.backgroundColor = UIColor.clear
        messageLbl.text = "No records found."
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.init(name: "Metropolis-ExtraLight", size: 13.0)
        messageLbl.textColor = UIColor.black
        messageLbl.numberOfLines = 3
        subview.addSubview(messageLbl)
    }


// MARK: - TableView Delegate Methods

func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.dataVideoArray.count
}


func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyDownloadCollectionCell", for: indexPath) as! MyDownloadCollectionCell

    cell.contentView.clipsToBounds = true
    let wrapper = self.dataVideoArray[indexPath.row]
    cell.lblItemLbl.text = wrapper.name
    cell.lblCategory.text = wrapper.category
    if(wrapper.image == ""){
        cell.lblImage.image = UIImage.init(named:"video-dummy")
        cell.lblImage.contentMode = .scaleAspectFit
    }
    else{
        cell.lblImage.sd_setImage(with: URL(string: wrapper.image!), placeholderImage: UIImage(named: "video-dummy"))
        cell.lblImage.contentMode = .scaleAspectFill
    }
    cell.heartBtn.tag = indexPath.row
    cell.heartBtn.addTarget(self, action: #selector(deleteData(_:)), for: .touchUpInside)
    cell.dropShadow(UIColor.lightGray)
    return cell
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

    if (collectionView.bounds.width < 320) {
        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size , height: size - 30)

    }
    else {
        let noOfCellsInRow = 3   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size + 20)

    }

}

func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    let wrapper = self.dataVideoArray[indexPath.row]
    let  newUrl = self.cacheDirectoryPath().appendingPathComponent("video").appendingPathComponent(wrapper.file)
    let player = AVPlayer(url:newUrl)
    playerController = AVPlayerViewController()
    playerController.player = player
    playerController.allowsPictureInPicturePlayback = true
    playerController.delegate = self
    playerController.player?.play()
    self.present(playerController,animated:true,completion:nil)

}


/*
 func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
 let deleteAction = UIContextualAction(style: .destructive, title: "Delete") {  (contextualAction, view, boolValue) in
 self.deleteData(at: indexPath)
 }
 let swipeActions = UISwipeActionsConfiguration(actions: [deleteAction])
 return swipeActions
 }
 */




@objc func deleteData(_ sender: UIButton) {
    let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to remove this video?", preferredStyle: .alert)

    // Create OK button with action handler
    let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in

        let wrapper = self.dataVideoArray[sender.tag]
        self.removeFileFromCacheDirectory(fileName: wrapper.file, type:"video")

        self.dataVideoArray.remove(at: sender.tag)
        if(self.dataVideoArray.count > 0){
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.dataVideoArray)
            UserDefaults.standard.set(encodedData, forKey: "video")
            UserDefaults.standard.synchronize()
            self.InitialiseData()
        }
        else{
            UserDefaults.standard.removeObject(forKey: "video")
            UserDefaults.standard.synchronize()
            self.InitialiseData()
        }

    })

    // Create Cancel button with action handlder
    let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in

    }

    //Add OK and Cancel button to dialog message
    dialogMessage.addAction(ok)
    dialogMessage.addAction(cancel)

    // Present dialog message to user
    self.present(dialogMessage, animated: true, completion: nil)


}

func removeFileFromCacheDirectory(fileName : String,type:String){
    var newUrl:URL
    newUrl = self.cacheDirectoryPath().appendingPathComponent(type)
    let documentsPath = newUrl.path
    let fileManager = FileManager.default

    do {
        let filePaths = try fileManager.contentsOfDirectory(atPath: "\(documentsPath)")
        for filePath in filePaths {
            if (filePath == fileName){
                let filePathName = "\(documentsPath)/\(fileName)"
                try fileManager.removeItem(atPath: filePathName)
            }
        }
    } catch {
        print("Could not clear temp folder: \(error)")
    }
}

func cacheDirectoryPath() -> URL {
    let cachePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
    return URL(fileURLWithPath: cachePath)
}

override func viewWillAppear(_ animated: Bool) {
    self.tabBarController?.tabBar.isHidden = true
}

override func viewWillDisappear(_ animated: Bool) {
    self.tabBarController?.tabBar.isHidden = false
}

}

class MyDownloadCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblItemLbl: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblImage: UIImageView!
    @IBOutlet weak var downloadBgView: UIImageView!
    @IBOutlet weak var heartBtn: UIButton!

}
