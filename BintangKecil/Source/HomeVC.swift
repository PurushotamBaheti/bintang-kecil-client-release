//
//  HomeVC.swift
//  BintangKecil
//
//  Created by Owebest on 13/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage

class HomeVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AVPlayerViewControllerDelegate, UITextFieldDelegate,categoriesAddDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtSearchField: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    private let refreshControl = UIRefreshControl()
    private let downloadManager = SDDownloadManager.shared
    @IBOutlet weak var categoryViewHeight: NSLayoutConstraint!
    @IBOutlet  var categoryView: UIView!

    var categoryScroll = UIScrollView()
    let margin: CGFloat = 10
    var dataArray = [AudioSheet]()
    var playerController = AVPlayerViewController()
    var page = 1
    var sort = "2"
    var order = "asc"
    var searchStr = ""
    var isFromSearch : Bool = false
    var totalrecords : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.fetchVideoList(category_id: "", page_no: self.page, search: self.searchStr, sort_by: sort)
        txtSearchField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        btnCancel.isHidden = true

        self.categoryScroll.frame = CGRect.init(x:0, y: 0, width: ScreenWidth, height: 48.0)
        self.categoryView.addSubview(self.categoryScroll)
        categoryViewHeight.constant = 0
        categoryView.isHidden = true


        // Do any additional setup after loading the view.
        
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = 20
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)

        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        collectionView.alwaysBounceVertical = true
        collectionView.refreshControl = refreshControl // iOS 10+

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(!appDelegate.CategoriesNames.isEmpty) {
            self.configureCategoryView()
        }
    }

    override func viewDidAppear(_ animated: Bool) {

        if(isFromSearch == true){
            txtSearchField.becomeFirstResponder()
            isFromSearch = false
        }
    }

    func configureCategoryView() {
        categoryViewHeight.constant = 48
        categoryView.isHidden = false

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let tempArr = appDelegate.CategoriesNames.components(separatedBy:",")

        let subViews = self.categoryScroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }

        var xcoordinate : CGFloat = 10
        let yCoordinate: CGFloat = 0
        let gap : CGFloat = 16

        for ind in 0...tempArr.count - 1 {
            let category = tempArr[ind]
            let size = category.size(withAttributes:[.font: UIFont.init(name:"Noteworthy-Bold", size: 15.0)!])
            let textsize = size.width  + 30


            let boxView = UIView()
            boxView.frame = CGRect.init(x:xcoordinate, y: yCoordinate, width: textsize, height: 40.0)
            boxView.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            boxView.layer.cornerRadius = 19
            self.categoryScroll.addSubview(boxView)

            let nameLbl = UILabel()
            nameLbl.frame = CGRect.init(x:0, y: yCoordinate, width: textsize, height: 40)
            nameLbl.text = category
            nameLbl.textColor = UIColor.white
            nameLbl.font = UIFont.init(name:"Noteworthy-Bold", size: 15.0)
            nameLbl.backgroundColor = UIColor.clear
            nameLbl.textAlignment = .center
            boxView.addSubview(nameLbl)

            xcoordinate = xcoordinate + textsize + gap

        }
        self.categoryScroll.contentSize = CGSize.init(width:xcoordinate, height: 48)
    }

    override func backAction(sender: AnyObject) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.CategoriesNames = ""
        appDelegate.Categories = ""

        self.navigationController?.popViewController(animated: true)
    }

    @objc
    private func didPullToRefresh(_ sender: Any) {
        // Do you your api calls in here, and then asynchronously remember to stop the
        // refreshing when you've got a result (either positive or negative)

        self.page = 1
        dataArray.removeAll()
        self.collectionView.reloadData()
        self.fetchVideoList(category_id: "", page_no: page, search: self.searchStr, sort_by: sort)
    }

    override func viewWillAppear(_ animated: Bool) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.Categories.isEmpty) {
            btnFilter.setImage(UIImage.init(named:"filter"), for: .normal)
        }
        else {
            btnFilter.setImage(UIImage.init(named:"filter_selected"), for: .normal)
        }

        self.tabBarController?.tabBar.isHidden = true

        if(!appDelegate.CategoriesNames.isEmpty) {
            self.configureCategoryView()
        }
        else {
            categoryViewHeight.constant = 0
            categoryView.isHidden = true
        }

    }

    override func viewWillDisappear(_ animated: Bool) {

        self.tabBarController?.tabBar.isHidden = false
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.text!.count > 0) {
            btnCancel.isHidden = false
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if let textFieldString = textField.text, let swtRange = Range(range, in: textFieldString) {
            let fullString = textFieldString.replacingCharacters(in: swtRange, with: string)
            let trimmed = fullString.trimmingCharacters(in: .whitespacesAndNewlines)

            if(!trimmed.isEmpty) {
                btnCancel.isHidden = false
            }
            else {
                btnCancel.isHidden = true
            }
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if(textField.text!.count > 0) {
            self.txtSearchField.endEditing(true)
            self.searchStr = txtSearchField.text!
            dataArray.removeAll()
            self.collectionView.reloadData()
            self.fetchVideoList(category_id: "", page_no: page, search: self.searchStr, sort_by: sort)

        }
        return true
    }

    @IBAction func FilterAction(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "categories") as! CategoriesVC
        vc.category_type = 1
        vc.type = 2
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)

    }

    func categoriesUpdatedSuccessfully() {
        self.page = 1
        self.txtSearchField.text = ""
        self.searchStr = ""
        dataArray.removeAll()
        self.collectionView.reloadData()
        self.fetchVideoList(category_id: "", page_no: page, search: self.searchStr, sort_by: sort)

    }

    @objc func doneButtonClicked(_ sender: Any) {
        self.txtSearchField.endEditing(true)
        self.searchStr = self.txtSearchField.text!
        dataArray.removeAll()
        self.collectionView.reloadData()
        self.fetchVideoList(category_id: "", page_no: page, search: self.searchStr, sort_by: sort)
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCollectionViewCell", for: indexPath) as! MyCollectionViewCell

        cell.layer.cornerRadius = 5
        let wrapper = self.dataArray[indexPath.row]
        cell.lblItem.text = wrapper.name
        cell.lblCategory.text = wrapper.category

        cell.downloadBtn.tag = indexPath.row
        cell.downloadBtn.addTarget(self, action: #selector(downloadBtnClicked(_:)), for: .touchUpInside)
        cell.downloadBtn.isEnabled = true
        if(wrapper.isDownloaded == true) {
            cell.downloadImgView.image = UIImage.init(named: "tick")
            cell.downloadBtn.isHidden = true
        }
        else {
            cell.downloadImgView.image = UIImage.init(named: "download")
            cell.downloadBtn.isHidden = false
        }

        if(wrapper.video_poster_image_url!.isEmpty) {
            cell.lblImage.backgroundColor = UIColor.init(patternImage: UIImage.init(named:"layer")!)
            cell.lblImage.image = UIImage.init(named: "video-dummy")
        }
        else {
            cell.lblImage.contentMode = .scaleAspectFill
            cell.lblImage.sd_setImage(with: URL(string: wrapper.video_poster_image_url!), placeholderImage: UIImage(named: "video-dummy"))
        }
        cell.dropShadow(UIColor.lightGray)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if (collectionView.bounds.width < 320) {
            let noOfCellsInRow = 2   //number of column you want
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
            return CGSize(width: size , height: size - 30)

        }
        else {
            let noOfCellsInRow = 3   //number of column you want
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
            return CGSize(width: size, height: size + 30)

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let wrapper = self.dataArray[indexPath.row]
        let urldata = URL(string: wrapper.videoFileURL)
        let player = AVPlayer(url:urldata!)
        playerController = AVPlayerViewController()
        playerController.player = player
        playerController.allowsPictureInPicturePlayback = true
        playerController.delegate = self
        let session = AVAudioSession.sharedInstance()
               do{
                   // try session.setCategory(AVAudioSession.Category.playback)
                try session.setCategory(.playAndRecord, options: [.defaultToSpeaker,.allowBluetoothA2DP,.allowAirPlay])
                   try       session.setActive(true, options: .init())

               }
               catch{
               }

        playerController.player?.play()

        self.present(playerController,animated:true,completion:nil)
        
    }

    @objc func downloadBtnClicked(_ sender: UIButton) {
        sender.alpha = 0.3
        sender.isEnabled = false
        self.showToast(message: "Download Started..")
        let wrapper = self.dataArray[sender.tag]
        var urlstr = ""
        var directory = ""
        urlstr = wrapper.videoFileURL
        directory = "video"

        let audioUrl = URL(string: urlstr)
        let dir = audioUrl?.lastPathComponent

        let request = URLRequest(url: URL(string: urlstr)!)

        self.downloadManager.showLocalNotificationOnBackgroundDownloadDone = true
        self.downloadManager.localNotificationText = "Downloads Are Completed."

        let _ = self.downloadManager.downloadFile(withRequest: request, inDirectory: directory, withName: dir, shouldDownloadInBackground: true, onProgress: { (progress) in
            let percentage = String(format: "%.1f %", (progress * 100))
            debugPrint("Background progress : \(percentage)")
        }) { [weak self] (error, url) in
            if let error = error {
                print("Error is \(error as NSError)")
            } else {
                if let url = url {
                    print("Downloaded file's url is \(url.path)")
                    DispatchQueue.main.async { [unowned self] in

                        if let _ = UserDefaults.standard.object(forKey: directory) {
                            var decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: directory) as! Data) as! [MediaModel]
                            let mediaWrapper = MediaModel.init(id: wrapper.id, image: wrapper.video_poster_image_url, name: wrapper.name, file: dir!, category: wrapper.category, date: (self?.currentDate())!,type:directory)
                            decodedData.append(mediaWrapper)
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: decodedData)
                            UserDefaults.standard.set(encodedData, forKey: directory)
                            UserDefaults.standard.synchronize()

                        }
                        else {
                            let mediaWrapper = MediaModel.init(id: wrapper.id, image: wrapper.video_poster_image_url, name: wrapper.name, file: dir!, category: wrapper.category, date: (self?.currentDate())!,type:directory)
                            var mediaArray = [MediaModel]()
                            mediaArray.append(mediaWrapper)
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: mediaArray)
                            UserDefaults.standard.set(encodedData, forKey: directory)
                            UserDefaults.standard.synchronize()

                        }
                        wrapper.isDownloaded = true
                        self!.dataArray[sender.tag] = wrapper
                        self!.collectionView.reloadData()
                    }

                }
            }
        }

    }

    func currentDate() -> String {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM YYYY"
        dateFormatter.locale = Locale(identifier: NSLocale.current.identifier)
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        return dateString
    }

    @IBAction func btncrossClicked(_ sender: Any) {
        self.txtSearchField.text = ""
        self.searchStr = ""
        self.btnCancel.isHidden = true
        self.page = 1
        dataArray.removeAll()
        self.collectionView.reloadData()
        self.fetchVideoList(category_id: "", page_no: page, search: self.searchStr, sort_by: sort)
    }
    
    @IBAction func SortingControl(_ sender: Any) {

        let alert = UIAlertController(title: "Sort By", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        alert.addAction(UIAlertAction(title: "Name (A-Z)", style: .default , handler:{ (UIAlertAction)in
            self.order = "asc"
            self.sort = "1"
            self.dataArray.removeAll()
            self.fetchVideoList(category_id: "", page_no: self.page, search: self.searchStr, sort_by:self.sort)
            
        }))

        alert.addAction(UIAlertAction(title: "Name (Z-A)", style: .default , handler:{ (UIAlertAction)in
            self.order = "desc"
            self.sort = "1"
            self.dataArray.removeAll()
            self.fetchVideoList(category_id: "", page_no: self.page, search: self.searchStr, sort_by:self.sort)

        }))

        alert.addAction(UIAlertAction(title: "Date (Ascending)", style: .default , handler:{ (UIAlertAction)in
            self.sort = "2"
            self.order = "asc"
            self.dataArray.removeAll()
            self.fetchVideoList(category_id: "", page_no: self.page, search: self.searchStr, sort_by:self.sort)
            
        }))

        alert.addAction(UIAlertAction(title: "Date (Descending)", style: .default , handler:{ (UIAlertAction)in
            self.sort = "2"
            self.order = "desc"
            self.dataArray.removeAll()
            self.fetchVideoList(category_id: "", page_no: self.page, search: self.searchStr, sort_by:self.sort)


        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))

        self.present(alert, animated: true, completion: {
        })
        
    }
    

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.dataArray.count - 1 && self.dataArray.count < self.totalrecords {
            getMoreImages(page: page)
        }
    }
    
    func getMoreImages(page:Int){
        self.page = self.page + 1
        self.fetchVideoList(category_id: "", page_no: self.page, search: self.searchStr, sort_by:self.sort)
        
    }
    func fetchVideoList( category_id: String, page_no: Int, search: String, sort_by:String ) {
        self.collectionView.backgroundView = nil
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        var parameter = [String:Any]()
        parameter["category_ids"] = appDelegate.Categories
        parameter["pageNumber"] = page_no
        parameter["search"] = search
        parameter["sort_by"] = sort_by
        parameter ["sort_order"] = self.order
        let isChild = UserDefaults.standard.getChild()
        if isChild{
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            parameter["child_id"] = decodedData.id

        }
        showLoader()
        postRequest(strUrl: "videos/list", param: parameter, success: { (json) in
            print(json)
            if json["success"].stringValue == "true" {
                self.refreshControl.endRefreshing()

                if json["message"].stringValue == "No Records Found" {
                    self.CreateNoFiltersView()
                }
                else {

                    self.totalrecords =  json["data"]["totalRecords"].intValue
                    print(self.totalrecords)
                    let arrData = json["data"].dictionaryValue["records"]!.arrayValue
                    if(arrData.count > 0) {
                        for ind in 0...arrData.count - 1 {
                            let wrapper = AudioSheet.init(file: arrData[ind]["file"].stringValue,
                                                          fileName: arrData[ind]["fileName"].stringValue,
                                                          category: arrData[ind]["category"].stringValue,
                                                          tags: arrData[ind]["tags"].stringValue,
                                                          name: arrData[ind]["name"].stringValue,
                                                          filteredContent: arrData[ind]["filteredContent"].stringValue,
                                                          audioFileURL: arrData[ind]["audio_file_url"].stringValue,
                                                          id: arrData[ind]["id"].intValue,
                                                          videoFileURL: arrData[ind]["video_file_url"].stringValue, video_poster_image_url: arrData[ind]["video_poster_image_url"].stringValue)

                            self.dataArray.append(wrapper)
                        }

                        if let _ = UserDefaults.standard.object(forKey: "video") {
                            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "video") as! Data) as! [MediaModel]
                            for ind in 0...decodedData.count - 1 {
                                let savedWrapper = decodedData[ind]
                                for indy in 0...self.dataArray.count - 1 {
                                    let wrapper = self.dataArray[indy]
                                    if(wrapper.id == savedWrapper.id) {
                                        wrapper.isDownloaded = true
                                        self.dataArray[indy] = wrapper
                                    }
                                }
                            }
                        }

                        self.collectionView.reloadData()
                    }
                }
            }


            self.hideLoader()
        })
        {
            (msg) in

            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func CreateNoFiltersView() {
        let popupview = UIView()
        popupview.backgroundColor = UIColor.clear
        self.collectionView.backgroundView = popupview

        let ycor = (ScreenHeight - self.collectionView.frame.origin.y)/2

        let subview = UIView()
        subview.frame = CGRect.init(x:30, y: ycor - 160, width: popupview.frame.size.width - 60, height: 160)
        subview.backgroundColor = UIColor.clear
        popupview.addSubview(subview)

        let logo = UIImageView.init()
        logo.frame = CGRect.init(x:(subview.frame.size.width - 80)/2, y:20, width: 80, height: 80)
        logo.image = UIImage.init(named:"logo")
        logo.contentMode = .scaleAspectFit
        subview.addSubview(logo)

        let messageLbl = UILabel()
        messageLbl.frame = CGRect.init(x: 5, y: 90, width: subview.frame.size.width - 10, height: 50)
        messageLbl.backgroundColor = UIColor.clear
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(!appDelegate.Categories.isEmpty) {
            messageLbl.text = "No records found for the given criteria.Please try with different filters."

        }
        else {
            messageLbl.text = "No records found."

        }
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.init(name: "Metropolis-ExtraLight", size: 13.0)
        messageLbl.textColor = UIColor.white
        messageLbl.numberOfLines = 3
        subview.addSubview(messageLbl)
    }
}

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblImage: UIImageView!

    @IBOutlet weak var downloadView: UIView!
    @IBOutlet weak var downloadBtn: UIButton!
    @IBOutlet weak var downloadImgView: UIImageView!

}

// MARK: - Audio_List Model
class AudioSheet {
    let file, fileName, category, tags: String
    let name, filteredContent: String
    let audioFileURL: String
    let id: Int
    let videoFileURL: String
    var video_poster_image_url : String? = ""
    var isDownloaded: Bool

    init(file: String, fileName: String, category: String, tags: String, name: String, filteredContent: String, audioFileURL: String, id: Int, videoFileURL: String,video_poster_image_url:String?) {
        self.file = file
        self.fileName = fileName
        self.category = category
        self.tags = tags
        self.name = name
        self.filteredContent = filteredContent
        self.audioFileURL = audioFileURL
        self.id = id
        self.videoFileURL = videoFileURL
        self.video_poster_image_url = video_poster_image_url
        self.isDownloaded = false
    }
}
