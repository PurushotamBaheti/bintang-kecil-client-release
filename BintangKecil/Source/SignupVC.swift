//
//  SignupVC.swift
//  BintangKecil
//
//  Created by Owebest on 04/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON
import GoogleSignIn
import TwitterKit
import TwitterCore
import SKCountryPicker
import AuthenticationServices

class SignupVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var showPasswordBtn: UIButton!
    @IBOutlet weak var checkboxBtn: UIButton!
    @IBOutlet weak var passwordViewHeight: NSLayoutConstraint!
    @IBOutlet weak var countryCodeTxtField: UITextField!

    var isShowPassword : Bool = false
    var isSocialSignup : Bool = false
    var validation = Validation()
    var socialId = ""
    var userName = ""
    var userEmail = ""
    var userMobile = ""
    var type = 0
    var isTermsConditionsSelected : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.PageName = "SignUp"
        self.navigationController?.navigationBar.isHidden = true
        self.IntialiseUI()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userDidSignUpGoogle(_:)),
                                               name: .signUpGoogleCompleted,
                                               object: nil)
        GIDSignIn.sharedInstance()?.presentingViewController = self

        // Do any additional setup after loading the view.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    @IBAction func CountryBtnClicked(){
      presentCountryPickerScene()

    }

    func IntialiseUI() {
        userNameTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        emailTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        mobileTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        if(self.isSocialSignup == true) {
            userNameTxtField.text = self.userName
            emailTxtField.text = self.userEmail
            mobileTxtField.text = self.userMobile
            passwordViewHeight.constant = 0

        }
        else{
            passwordTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }

    }

    @IBAction func SignUpClicked(){
        self.view.endEditing(true)
        if(self.ValidateTextFields() == true){
            self.callCredentialsExistApi()
        }
    }

    @IBAction func PrivacyPolicyClicked() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
        vc.pageTitle = "Privacy Policy"
        vc.pageUrl = PRIVACY_URL
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func TermsConditionsClicked() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
        vc.pageTitle = "Our Terms"
        vc.pageUrl = TERMS_URL
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func termsConditionCheckBoxAction() {
        if(self.isTermsConditionsSelected == false) {
            self.isTermsConditionsSelected = true
            self.checkboxBtn.setImage(UIImage.init(named:"checkbox_selected"), for: .normal)
        }
        else {
            self.checkboxBtn.setImage(UIImage.init(named:"checkbox"), for: .normal)
            self.isTermsConditionsSelected = false
        }
    }

    func ValidateTextFields() -> Bool {
        if((userNameTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_NAME)
            return false
        }
        else if((emailTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_EMAILADDRESS)
            return false
        }
        else if(validation.validateEmailId(emailID: self.emailTxtField.text!) == false) {
            self.showToast(message: MessageString.INVALID_EMAIL)
            return false
        }
        else if((mobileTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_MOBILE)
            return false
        }
            /*
             else if(mobileTxtField.text?.prefix(1) != "+")
             {
             self.showToast(message: MessageString.INVALID_MOBILE_FORMAT)
             return false
             }
             */
        else if(validation.validaPhoneNumber(phoneNumber: self.mobileTxtField.text!) == false) {
            self.showToast(message: MessageString.INVALID_MOBILE)
            return false
        }
        else if((passwordTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!  && self.isSocialSignup == false) {
            self.showToast(message: MessageString.EMPTY_PASSWORD)
            return false
        }
        else if(validation.validatePassword(password:self.passwordTxtField.text!) == false  && self.isSocialSignup == false) {
            self.showToast(message: MessageString.INVALID_PASSWORD)
            return false
        }
        else if(self.isTermsConditionsSelected == false) {
            self.showToast(message: MessageString.SELECT_TERMS)
            return false
        }

        return true
    }

    func callCredentialsExistApi() {
        var parameter = [String:Any]()
        parameter["email"] = self.emailTxtField.text!
        var mobilenum = self.countryCodeTxtField.text! + self.mobileTxtField.text!
        mobilenum = mobilenum.replacingOccurrences(of:"+", with: "")
        parameter["mobile"] = mobilenum

        showLoader()
        postRequest(strUrl: "check-email-mobile-exist", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {
                let mobilenumb = self.countryCodeTxtField.text! + self.mobileTxtField.text!
                //  mobilenumb = mobilenumb.replacingOccurrences(of:"-", with: "")

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "otp") as! OtpVC
                vc.isFromSignUp = true
                vc.mobile = mobilenumb
                vc.socialId = self.socialId
                vc.name = self.userNameTxtField.text!
                if(self.isSocialSignup == false) {
                    vc.password = self.passwordTxtField.text!
                }
                else {
                    vc.password = ""
                }

                vc.email = self.emailTxtField.text!
                vc.signupType = self.type
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else {
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    @IBAction func LoginClicked() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func ShowPasswordClicked() {
        if(isShowPassword == false) {
            isShowPassword = true
            self.passwordTxtField.isSecureTextEntry = false
            showPasswordBtn.setImage(UIImage.init(named:"password_hide_black"), for: .normal)
        }
        else {
            isShowPassword = false
            self.passwordTxtField.isSecureTextEntry = true
            showPasswordBtn.setImage(UIImage.init(named:"password_visible_black"), for: .normal)
        }
    }

    @IBAction func FacebookBtnClicked() {
        let loginManager = LoginManager()

        if let _ = AccessToken.current {
            // Access token available -- user already logged in
            // Perform log out

            loginManager.logOut()

        }
        // Access token not available -- user already logged out
        // Perform log in

        loginManager.logIn(permissions: ["public_profile","email"], from: self) { [weak self] (result, error) in

            // Check for error
            guard error == nil else {
                // Error occurred
                return
            }

            // Check for cancel
            guard let result = result, !result.isCancelled else {
                return
            }

            guard let accessToken = FBSDKLoginKit.AccessToken.current else { return }
            let graphRequest = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                          parameters: ["fields": "email, name"],
                                                          tokenString: accessToken.tokenString,
                                                          version: nil,
                                                          httpMethod: .get)
            graphRequest.start { (connection, result, error) -> Void in
                if error == nil {

                    guard let Info = result as? [String: Any] else { return }

                    var fbId = ""
                    var fbEmail = ""
                    var FbName = ""
                    if let userName = Info["name"] as? String{
                        FbName = userName
                    }
                    if let userId = Info["id"] as? String{
                        fbId = userId
                    }
                    if let userEmail = Info["email"] as? String{
                        fbEmail = userEmail
                    }
                    self?.socialId = fbId
                    self?.userEmail = fbEmail
                    self?.userName = FbName
                    self?.type = 1
                    self?.callSocialLoginApi()
                }
                else {
                    print("error \(String(describing: error))")
                }
            }

        }
        
    }
    @IBAction func GoogleBtnClicked() {
        GIDSignIn.sharedInstance()?.signIn()
    }

    @IBAction func TwitterBtnClicked() {
        TWTRTwitter.sharedInstance().logIn { [weak self] (session, error) in
            guard let self = self  else { return }
            if error != nil {
                return
            }

            var TwitterEmail = ""

            let client = TWTRAPIClient.withCurrentUser()
            client.requestEmail { email, error in
                if (email != nil) {
                    let recivedEmailID = email ?? ""
                    TwitterEmail = recivedEmailID

                    self.type = 3
                    self.socialId = session!.userID
                    self.userName = session!.userName
                    self.userEmail = TwitterEmail

                    let store = TWTRTwitter.sharedInstance().sessionStore
                    store.logOutUserID(session!.userID)
                    self.callSocialLoginApi()


                }else {
                    print("error--: \(String(describing: error?.localizedDescription))");
                }
            }
        }
    }

    // MARK:- Notification
    @objc private func userDidSignUpGoogle(_ notification: Notification) {
        // Update screen after user successfully signed in
        if let user = GIDSignIn.sharedInstance()?.currentUser {

            self.type = 2
            self.userName = user.profile.name!
            self.userEmail = user.profile.email!
            self.socialId = user.userID

            GIDSignIn.sharedInstance()?.signOut()
            self.callSocialLoginApi()

        }
    }
    func callSocialLoginApi() {

        var token = ""
        if let tokenstr = UserDefaults.standard.value(forKey: "fcmId"){
            token = tokenstr as! String
        }

        var parameter = [String:Any]()
        parameter["type"] = type
        parameter["password"] = ""
        parameter["osType"] = "ios"
        parameter["macId"] = DEVICE_ID
        parameter["tokenId"] = token
        parameter["social_id"] = socialId
        parameter["email"] = userEmail
        parameter["mobile"] = ""

        showLoader()
        postRequest(strUrl: "login", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {
                let userWrapper = UserModal.init(id: json["data"]["id"].intValue, image: json["data"]["image"].stringValue, email: json["data"]["email"].stringValue, image_thumbnail: json["data"]["image_thumbnail"].stringValue, accessToken: json["data"]["accessToken"].stringValue, user_code: json["data"]["user_code"].stringValue, name: json["data"]["name"].stringValue, mobile: json["data"]["mobile"].stringValue)

                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userWrapper)
                UserDefaults.standard.set(encodedData, forKey: "userdata")

                UserDefaults.standard.setLogin(true)
                UserDefaults.standard.synchronize()
                self.view.endEditing(true)
                let window = UIApplication.shared.keyWindow
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "tabbar")
                window?.rootViewController = dashboard
            }
            else if(json["status"].stringValue == "201") {

                if(self.type > 0) {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "social") as! SocialSignupVC
                    vc.socialId = self.socialId
                    vc.userName = self.userName
                    vc.userEmail = self.userEmail
                    vc.userMobile = ""
                    vc.type = self.type
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    let message = json["message"].stringValue
                    self.showToast(message: message)
                }
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func callAppleLoginApi() {

        var token = ""
        if let tokenstr = UserDefaults.standard.value(forKey: "fcmId"){
            token = tokenstr as! String
        }

        var parameter = [String:Any]()
        parameter["type"] = type
        parameter["password"] = ""
        parameter["osType"] = "ios"
        parameter["macId"] = DEVICE_ID
        parameter["tokenId"] = token
        parameter["social_id"] = socialId
        parameter["email"] = userEmail
        parameter["mobile"] = ""

        showLoader()
        postRequest(strUrl: "ios-social-email-login", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {
                let userWrapper = UserModal.init(id: json["data"]["id"].intValue, image: json["data"]["image"].stringValue, email: json["data"]["email"].stringValue, image_thumbnail: json["data"]["image_thumbnail"].stringValue, accessToken: json["data"]["accessToken"].stringValue, user_code: json["data"]["user_code"].stringValue, name: json["data"]["name"].stringValue, mobile: json["data"]["mobile"].stringValue)

                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userWrapper)
                UserDefaults.standard.set(encodedData, forKey: "userdata")

                UserDefaults.standard.setLogin(true)
                UserDefaults.standard.synchronize()
                self.view.endEditing(true)
                let window = UIApplication.shared.keyWindow
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "tabbar")
                window?.rootViewController = dashboard
            }
            else if(json["status"].stringValue == "201") {

                if(self.type > 0) {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "social") as! SocialSignupVC
                    vc.socialId = self.socialId
                    vc.userName = self.userName
                    vc.userEmail = self.userEmail
                    vc.userMobile = ""
                    vc.type = self.type
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    let message = json["message"].stringValue
                    self.showToast(message: message)
                }
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

}

/// MARK: - Private Methods
private extension SignupVC {

    func presentCountryPickerScene(withSelectionControlEnabled selectionControlEnabled: Bool = true) {
        switch selectionControlEnabled {
        case true:
            // Present country picker with `Section Control` enabled
            _ = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
                guard let self = self else { return }
                self.countryCodeTxtField.text = country.dialingCode!
            }

        case false:
            _ = CountryPickerController.presentController(on: self) { [weak self] (country: Country) in
                guard let self = self else { return }
                self.countryCodeTxtField.text = country.dialingCode!
            }

        }
    }
}


extension SignupVC: ASAuthorizationControllerPresentationContextProviding,ASAuthorizationControllerDelegate {

    // For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }

    @IBAction func actionHandleAppleSignin() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }else{
            self.showToast(message: "Not supoorted.Require iOS version 13 or greater.")
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Get user data with Apple ID credentitial
            let userId = appleIDCredential.user
            let userFirstName = appleIDCredential.fullName?.givenName
            let userLastName = appleIDCredential.fullName?.familyName
            let userEmail = appleIDCredential.email
            if(userEmail != nil){
               self.userEmail = userEmail!
            } else {
               self.userEmail = ""
            }

            if(userFirstName == nil){
                 self.userName = ""
            }else {
                 self.userName = userFirstName! + " " + userLastName!
            }

            self.socialId = userId
            self.type = 4
            self.callAppleLoginApi()


            // Write your code here
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Get user data using an existing iCloud Keychain credential
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password

            self.userEmail = ""
            self.userName = ""
            self.socialId = appleUsername
            self.type = 4
            self.callAppleLoginApi()

            // Write your code here
        }
    }



}
