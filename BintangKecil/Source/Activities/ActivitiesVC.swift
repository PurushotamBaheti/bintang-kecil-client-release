//
//  ActivitiesVC.swift
//  BintangKecil
//
//  Created by Purushotam on 13/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class ActivitiesVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    let margin: CGFloat = 10
    var dataArray = [PayFeatureModel]()
    @IBOutlet weak var noActiviesView: UIView!

    @IBOutlet weak var noActiviesEngLbl: UILabel!
    @IBOutlet weak var noActivieBhashaLbl: UILabel!
    @IBOutlet weak var noActiviesSubscribeBtn: UIButton!


    var KaraokeArray = [PayFeatureModel]()
    let popupview = UIView()
    var funnyEmojiCount = 0
    var funnyVoiceCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.tabBarController?.tabBar.layer.shadowRadius = 5
        self.tabBarController?.tabBar.layer.shadowOpacity = 0.65
        self.tabBarController?.tabBar.layer.masksToBounds = false


        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)

        // Do any additional setup after loading the view.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override func viewWillAppear(_ animated: Bool) {
        noActiviesView.removeFromSuperview()
        self.tabBarController?.tabBar.isHidden = false
        self.KaraokeArray.removeAll()
        self.dataArray.removeAll()
        self.collectionView.reloadData()
        self.fetchActivitiesList()
    }

    func fetchActivitiesList() {
        popupview.removeFromSuperview()
        var parameter = [String:Any]()
        let isChild = UserDefaults.standard.getChild()
        if isChild{

            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            parameter["child_id"] = decodedData.id
        }
        showLoader()
        postRequest(strUrl: "user-plan", param: parameter, success: { (json) in
            self.hideLoader()
            print(json)
            if json["status"].stringValue == "200"{
                let arrData = json["data"]["features"].arrayValue
                if(arrData.count > 0){
                    for ind in 0...arrData.count - 1{
                        let wrapper = PayFeatureModel.init(fromJson: arrData[ind])
                        if(wrapper.type == "boolean" && wrapper.value == "1"){
                            if(wrapper.name.uppercased() == "KARAOKE"){
                                self.KaraokeArray.append(wrapper)
                            }
                            else {
                                self.dataArray.append(wrapper)
                            }
                        }
                        else if(wrapper.type == "integer" && wrapper.slug == "allow-number-of-funny-emoji"){
                            self.funnyEmojiCount = Int(wrapper.value)!
                            if(Int(wrapper.value)! > 17){
                                self.funnyEmojiCount = 0
                            }
                        }
                        else if(wrapper.type == "integer" && wrapper.slug == "allow-number-of-funny-voices"){
                            self.funnyVoiceCount = Int(wrapper.value)!
                            if(Int(wrapper.value)! > 5){
                                self.funnyVoiceCount = 0
                            }
                        }
                    }
                    self.collectionView.reloadData()
                }
                else{
                    self.CreateNoActivitiesView()
                }

            }
            else if(json["status"].stringValue == "201"){
                let message = json["message"].stringValue
                if(message == "User does not have active subscription plan!"){
                    self.noActiviesView.frame = CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)
                    self.noActiviesEngLbl.text = MessageString.NO_Activities_English_Msg
                    self.noActivieBhashaLbl.text = MessageString.NO_Activities_Bhasha_Msg
                    self.noActiviesSubscribeBtn.isHidden = false
                    self.view.addSubview(self.noActiviesView)
                }
                else{
                    self.showToast(message: message)
                    if(message == "Your account may be deactivated from admin. please contact to admin support!"){
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                            UserDefaults.standard.removeObject(forKey:"childdata")
                            UserDefaults.standard.removeObject(forKey: "userdata")
                            UserDefaults.standard.setChild(false)
                            UserDefaults.standard.setLogin(false)
                            UserDefaults.standard.synchronize()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                            appDelegate.window?.rootViewController = dashboard
                        }
                    }
                }


            }
            else if(json["status"].stringValue == "202"){
                let message = json["message"].stringValue
                self.showToast(message: message)
                if(message == "Your account may be deactivated from admin. please contact to admin support!"){
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0){
                        UserDefaults.standard.removeObject(forKey:"childdata")
                        UserDefaults.standard.removeObject(forKey: "userdata")
                        UserDefaults.standard.setChild(false)
                        UserDefaults.standard.setLogin(false)
                        UserDefaults.standard.synchronize()
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                        appDelegate.window?.rootViewController = dashboard
                    }
                }
            }
            else if(json["status"].stringValue == "403"){
                print("Payment Status Pending")
                //let methodName = json["data"]["payment_name"].stringValue
                let engMessage = "Your transaction status is still pending, buddy. Wait a minute. If you have successfully made a payment, there is no need to transact again."
                let bhashaMessage = "Status transaksimu masih tertunda, Sobat. Tunggu sebentar yaa. Kalau kamu sudah berhasil melakukan pembayaran, tidak perlu bertransaksi lagi."

                self.noActiviesView.frame = CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)
                self.noActiviesEngLbl.text = engMessage
                self.noActivieBhashaLbl.text = bhashaMessage
                self.noActiviesSubscribeBtn.isHidden = false
                self.view.addSubview(self.noActiviesView)
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func CreateNoActivitiesView(){

        popupview.frame = CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight - 64)
        popupview.backgroundColor = UIColor.clear
        self.view.addSubview(popupview)

        let subview = UIView()
        subview.frame = CGRect.init(x:30, y: (popupview.frame.size.height - 160)/2, width: popupview.frame.size.width - 60, height: 160)
        subview.backgroundColor = UIColor.clear
        popupview.addSubview(subview)

        let logo = UIImageView.init()
        logo.frame = CGRect.init(x:(subview.frame.size.width - 80)/2, y:20, width: 80, height: 80)
        logo.image = UIImage.init(named:"logo")
        logo.contentMode = .scaleAspectFit
        subview.addSubview(logo)

        let messageLbl = UILabel()
        messageLbl.frame = CGRect.init(x: 5, y: 110, width: subview.frame.size.width - 10, height: 50)
        messageLbl.backgroundColor = UIColor.clear
        messageLbl.text = "No Activies available for your Age group."

        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.init(name: "Metropolis-Medium", size: 13.0)
        messageLbl.textColor = UIColor.white
        messageLbl.numberOfLines = 2
        subview.addSubview(messageLbl)

    }

    @IBAction func btnPurchaseClicked(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPlanVC") as! PaymentPlanVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        return CGSize.init(width: 0, height: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {

        return CGSize.init(width: 0, height: 0)
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if(section == 0){
            return self.KaraokeArray.count
        }
        return self.dataArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if(indexPath.section == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyActivityViewCell", for: indexPath) as! MyActivityViewCell
            let wrapper = self.KaraokeArray[indexPath.row]

            cell.itemName.text = wrapper.name + " & " + "\nExpresi"
            let bgImage = wrapper.name + "_bg"
            cell.bgImage.image = UIImage.init(named: bgImage)
            cell.itemImage.image = UIImage.init(named: wrapper.name)
            cell.contentView.layer.borderWidth = 0.5
            cell.contentView.layer.borderColor = UIColor.init(red: 215.0/255.0, green: 215.0/255.0, blue: 215.0/255.0, alpha: 0.5).cgColor
            cell.contentView.layer.cornerRadius = 8
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyActivityViewCell", for: indexPath) as! MyActivityViewCell
            let wrapper = self.dataArray[indexPath.row]

            cell.itemName.text = wrapper.name
            let bgImage = wrapper.name + "_bg"
            cell.bgImage.image = UIImage.init(named: bgImage)
            cell.itemImage.image = UIImage.init(named: wrapper.name)
            cell.contentView.layer.borderWidth = 0.5
            cell.contentView.layer.borderColor = UIColor.init(red: 215.0/255.0, green: 215.0/255.0, blue: 215.0/255.0, alpha: 0.5).cgColor
            cell.contentView.layer.cornerRadius = 8
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          let noOfCellsInRow = 1   //number of column you want
          let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
          let totalSpace = flowLayout.sectionInset.left
              + flowLayout.sectionInset.right
              + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

          let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
          return CGSize(width: size, height: 160 )

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if(indexPath.section == 0){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "KaraokeListVC") as! KaraokeListVC
            vc.EmojiCount = self.funnyEmojiCount
            vc.VoicesCount = self.funnyVoiceCount
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let wrapper = self.dataArray[indexPath.row]
            if(wrapper.name.uppercased() == "ARTICLES"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ArticlesVC") as! ArticlesVC
                self.navigationController?.pushViewController(vc, animated: true)

            }
            else if(wrapper.name.uppercased() == "FORUM"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForumVC") as! ForumVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if(wrapper.name.uppercased() == "KARAOKE"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "KaraokeListVC") as! KaraokeListVC
                print(self.funnyEmojiCount)
                vc.EmojiCount = self.funnyEmojiCount
                self.navigationController?.pushViewController(vc, animated: true)
            }

            else if(wrapper.name.uppercased() == "COLORING"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColoringVC") as! ColoringVC
                vc.pageTitle = NSLocalizedString("Coloring", comment: "")
                vc.isForColoring = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if(wrapper.name.uppercased() == "TRACING"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColoringVC") as! ColoringVC
                vc.pageTitle = NSLocalizedString("Tracing", comment: "")
                vc.isForColoring = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if(wrapper.name.uppercased() == "FUNNY VOICE CHANGER"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FunnyVoiceVC") as! FunnyVoiceVC
                vc.filterCount = self.funnyVoiceCount
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if(wrapper.name.uppercased() == "FUNNY EMOJI VIDEO RECORDER"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FunnyEmojiVC") as! FunnyEmojiVC
                vc.EmojiCount = self.funnyEmojiCount
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                self.showToast(message: "This feature is not developed yet.")
            }
        }

    }
}

class MyActivityViewCell: UICollectionViewCell {
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
}
