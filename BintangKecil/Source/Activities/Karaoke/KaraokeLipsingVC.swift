//
//  KaraokeLipsingVC.swift
//  BintangKecil
//
//  Created by Purushotam on 07/08/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import AVFoundation
import AssetsLibrary
import Photos
import MarqueeLabel
import DeepAR


class KaraokeLipsingVC: BaseViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,AVAudioRecorderDelegate,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var overLayView2: UIView!
    var picker : UIImagePickerController!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var recordView: UIView!
    var isRecordingStarted : Bool = false
    var isRecordingDone : Bool = false
    @IBOutlet weak var recordLbl :  UILabel!
    var mp3FilePath = ""
    var mp3File  = ""
    var soundPlayer : AVPlayer!
    var karaoke : DEKaraokeController!
    var karaokeURL : NSURL!
    var VideoData : Data!
    var recorder : AVAudioRecorder!

    var playerr : AVAudioPlayer!
    var videoUrl : NSURL!
    var mergedAudioUrl : NSURL!

    @IBOutlet  var titleLbl: MarqueeLabel!

    var songName = ""

    var isKaraokeUploaded : Bool = false
    var isKaraokeSaved : Bool = false

    private var AllfilterArray = ["None", "Alien","Aviators","Beard","Big Mouth","Dalmatian","Fatify","Flowers","Grumpy cat","Koala","Lion","Mud Mask","Obama","Pug","Slash","Small face","Teddy Cigar","Topology","Triple face"]
    
    @IBOutlet weak var collectionView: UICollectionView!
    let margin: CGFloat = 4
    @IBOutlet weak var transparentOverlay: UIView!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    private var deepAR: DeepAR!
    private var arView: ARView!
    private var cameraController: CameraController!
    @IBOutlet weak var arViewContainer: UIView!

    private var maskIndex: Int = 0
    private var maskPaths: [String?] {
        return Masks.allCases.map { $0.rawValue.path }
    }

    private var currentMode: Mode!
    private var buttonRecordingModePairs: [(UIButton, RecordingMode)] = []
    private var currentRecordingMode: RecordingMode!
    private var isRecordingInProcess: Bool = false
    @IBOutlet weak var funnyFilterView: UIView!
    var EmojiCount = 0
    var VoiceFilterCount = 0
    var filterArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupFilters()

        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        collectionView.reloadData()
        collectionView.selectItem(at: NSIndexPath(item: 0, section: 0) as IndexPath, animated: true, scrollPosition:[])

        self.addTargets()
        self.customiseUI()

        // Do any additional setup after loading the view.
    }
    func setupFilters(){

        if(self.EmojiCount == 0){
            self.filterArray.append(contentsOf: self.AllfilterArray)
        }
        else{
            for ind in 0...self.EmojiCount{
                self.filterArray.append(self.AllfilterArray[ind])
            }
        }
    }

    override func backAction(sender: AnyObject) {
        if(self.isRecordingDone == true){
            if(self.isKaraokeSaved == false && self.karaokeURL != nil){
                let dialogMessage = UIAlertController(title: "Confirm", message: "You have an unsaved Expresi. Once you exit from this page, all your activity will be lost. Do you want to save your Expresi ?", preferredStyle: .alert)

                // Create OK button with action handler
                let ok = UIAlertAction(title: "Save", style: .default, handler: { (action) -> Void in
                    let btn = UIButton()
                    self.SaveAction(sender: btn)

                })

                // Create Cancel button with action handlder
                let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                    self.cameraController.stopCamera()
                    self.cameraController.stopAudio()
                    self.cameraController.deepAR = nil
                    self.arView.removeFromSuperview()
                    self.deepAR.shutdown()
                    self.deepAR = nil
                    self.navigationController?.popViewController(animated: true)
                }

                //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                dialogMessage.addAction(cancel)

                // Present dialog message to user
                self.present(dialogMessage, animated: true, completion: nil)
            }
            else{
                cameraController.stopCamera()
                cameraController.stopAudio()
                cameraController.deepAR = nil
                self.arView.removeFromSuperview()
                self.deepAR.shutdown()
                self.deepAR = nil
                self.navigationController?.popViewController(animated: true)
            }
        }
        else{
            cameraController.stopCamera()
            cameraController.stopAudio()
            cameraController.deepAR = nil
            self.arView.removeFromSuperview()
            self.deepAR.shutdown()
            self.deepAR = nil
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func ApplyFilerClicked(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VoiceFilterVC") as! VoiceFilterVC
        vc.karaokeUrl = self.mergedAudioUrl
        vc.msgStr = "Please choose the voice filter to apply on the recorded audio of your Expresi song."
        vc.VoiceFilterCount = self.VoiceFilterCount
        self.navigationController?.pushViewController(vc, animated: true)
    }

    private func addTargets() {
        previousButton.addTarget(self, action: #selector(didTapPreviousButton), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(didTapNextButton), for: .touchUpInside)
    }

    func customiseUI(){

        btnSave.isHidden = true
        btnShare.isHidden = true
        btnUpload.isHidden = true
        let loadmessage  = "Please wait!. Preparing Song for you.\nTunggu sebentar!.Lagumu sedang disiapkan ya Sobat"
        self.showLoaderwithMessage(message: loadmessage)

        loadFileAsync(url: URL.init(string: self.mp3FilePath)!) { (path, error) in
            self.mp3File = path!
            DispatchQueue.main.async {
                self.createLyricsView()
                self.hideLoader()
            }
        }

        currentMode = .masks
        currentRecordingMode = .video

        self.deepAR = DeepAR()
        self.deepAR.delegate = self
        self.deepAR.setLicenseKey(LICENSE_KEY)

        cameraController = CameraController()
        cameraController.deepAR = self.deepAR
        cameraController.deepAR.initialize()

        let frame = CGRect.init(x: 0.0, y: 0.0, width: ScreenWidth, height: ScreenHeight)
        self.arView = self.deepAR.createARView(withFrame: frame) as? ARView

        self.arViewContainer.addSubview(self.arView)
        cameraController.startCamera()
    }

    func recordFileURL ()-> NSURL{
        let documentsUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let destinationUrl = documentsUrl.appendingPathComponent("karaoke.wav")
        return destinationUrl as NSURL
    }

    func createLyricsView(){
        self.karaoke = DEKaraokeController.init(bgMusic: URL.init(string: self.mp3File), outputURL: self.recordFileURL() as URL)
        do {
            let marqueText = "Title: " + self.songName
            self.titleLbl.text = marqueText

            let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let outputFileURL = documentsUrl.appendingPathComponent("karaoke.m4a")
            let session = AVAudioSession.sharedInstance()

            do {
                try session.setCategory(.playAndRecord)
            }catch{
            }

            let recordSetting = NSMutableDictionary()
            recordSetting.setValue(NSNumber.init(value: kAudioFormatMPEG4AAC), forKey: AVFormatIDKey)
            recordSetting.setValue(NSNumber.init(value: 2), forKey: AVNumberOfChannelsKey)
            recordSetting.setValue(NSNumber.init(value: 44100.0), forKey: AVSampleRateKey)

            do {
                try recorder = AVAudioRecorder.init(url: outputFileURL, settings: recordSetting as! [String : Any])
                self.recorder.delegate = self
                self.recorder.isMeteringEnabled = true
                self.recorder.prepareToRecord()
            }catch{
            }

        } catch {
            // contents could not be loaded
        }

    }

    @IBAction func SaveAction(sender: AnyObject) {
        if(self.karaokeURL != nil){
            if(self.isKaraokeSaved == false){
                DispatchQueue.main.async {
                    self.showLoader()
                }
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.karaokeURL as URL)
                }) { saved, error in
                    if saved {
                        DispatchQueue.main.async {
                            self.hideLoader()
                        }
                        self.isKaraokeSaved = true
                        let alertController = UIAlertController(title: "Your Expresi video is successfully saved in your device gallery.", message: nil, preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else{
                        DispatchQueue.main.async {
                            self.hideLoader()
                        }
                    }
                }
            }
            else{
                self.showToast(message: "You karaoke is already saved in device gallery.")
            }
        }
        else{
            DispatchQueue.main.async {
                self.hideLoader()
            }
        }
    }

    @IBAction func ShareAction(sender: AnyObject) {

        if(self.karaokeURL != nil){
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let filePath="\(documentsPath)/tempFile.mp4"

            do{

                try FileManager.default.removeItem(atPath: filePath)

            }catch{

            }
            DispatchQueue.global(qos: .background).async {
                if let urlData = NSData(contentsOf: self.karaokeURL as URL){

                    DispatchQueue.main.async {
                        urlData.write(toFile: filePath, atomically: true)

                        //Hide activity indicator

                        let activityVC = UIActivityViewController(activityItems: [NSURL(fileURLWithPath: filePath)], applicationActivities: nil)
                        activityVC.excludedActivityTypes = [.addToReadingList, .assignToContact,.airDrop,.copyToPasteboard,.message,.openInIBooks,.markupAsPDF,.mail]
                        self.present(activityVC, animated: true, completion: nil)
                    }
                }
            }
        }
        else{
            DispatchQueue.main.async {
                self.hideLoader()
            }
        }
    }

    @IBAction func UploadAction(sender: AnyObject) {

        if(self.isKaraokeUploaded == false){
            let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to upload this Expresi for Karaoke of the Day Contest?", preferredStyle: .alert)

            // Create OK button with action handler
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                self.uploadKaraoke()
            })
            // Create Cancel button with action handlder
            let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            }

            //Add OK and Cancel button to dialog message
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)
            // Present dialog message to user
            self.present(dialogMessage, animated: true, completion: nil)
        }
        else{
            self.showToast(message: "This karaoke is already uploaded.")
        }

    }

    func uploadKaraoke(){
        var parameter = [String:Any]()
        let isChild = UserDefaults.standard.getChild()
        if isChild{
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            parameter["child_id"] = decodedData.id
        }

        DispatchQueue.main.async() {
            do {
                let loadmessage  = "Uploading!. This may take a while.\nSedang Diunggah. Sabar ya Sobat Bintang."
                self.showLoaderwithMessage(message: loadmessage)
                self.VideoData = try Data(contentsOf: self.karaokeURL as URL, options: .mappedIfSafe)
                var TotalArray = [[String : Any]]()
                if(self.VideoData != nil){
                    let dict = ["video":self.VideoData as Any,"key":"file"]
                    TotalArray.append(dict)
                }

                uploadData(strUrl: "karaoke/upload", arrImage: TotalArray, param: parameter, success: { (json) in
                    if json["status"].stringValue == "200"
                    {
                        let alertController = UIAlertController(title: "Expresi uploaded successfully for Karaoke of the day contest. Winner name will be notified through notifications.", message: nil, preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                        self.isKaraokeUploaded = true
                    }
                    self.hideLoader()

                }, failure: {
                    (msg) in
                    self.hideLoader()
                    self.ShowAlert(message: msg as! String)
                })
            } catch {
                return
            }
        }
    }

    @IBAction func RecordAction(sender: AnyObject) {
        if(self.isRecordingDone == true){
            if(self.karaokeURL != nil){
                DispatchQueue.main.async {

                    let player = AVPlayer(url:self.karaokeURL as URL)
                    let vcPlayer = AVPlayerViewController()
                    vcPlayer.player = player
                    vcPlayer.player?.play()
                    self.present(vcPlayer, animated: true, completion: nil)

                }
            }
        }
        else{
            let width: Int32 = Int32(deepAR.renderingResolution.width)
            let height: Int32 =  Int32(deepAR.renderingResolution.height)

            if(isRecordingStarted == false){
                deepAR.startVideoRecording(withOutputWidth: width, outputHeight: height)
                isRecordingStarted = true
                self.recordLbl.text = "Stop"
                do{
                    try playerr = AVAudioPlayer(contentsOf: URL(fileURLWithPath: self.mp3File))
                    let session = AVAudioSession.sharedInstance()
                    do{
                        // try session.setCategory(AVAudioSession.Category.playback)
                        try session.setCategory(.playAndRecord, options: [.mixWithOthers,.duckOthers,.allowBluetooth,.allowAirPlay,.defaultToSpeaker])
                        try       session.setActive(true, options: .init())

                    }
                    catch{
                    }
                    playerr.play()
                    self.recorder.record()

                } catch {
                    print("File is not Loaded")
                }
            }
            else{
                self.recorder.stop()
                self.playerr.stop()
                deepAR.finishVideoRecording()

                var ycoodinate = 84
                if(ScreenHeight > 811)
                {
                    ycoodinate = 84 +  30
                }
                funnyFilterView.frame = CGRect.init(x: 0, y: ycoodinate, width: Int(ScreenWidth), height: 160)
                funnyFilterView.backgroundColor = UIColor.clear
                self.view.addSubview(funnyFilterView)

                isRecordingDone = true
                self.btnSave.isHidden = false
                self.btnShare.isHidden = false
                self.btnUpload.isHidden = false
                self.recordLbl.text = "Preview"
                self.mergeAudioFilesWithUrl(videoUrl: self.recorder!.url as NSURL, audioUrl: URL.init(fileURLWithPath:self.mp3File) as NSURL)
                self.titleLbl.isHidden = true

            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    //MARK: Collection View Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.filterArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FunnyEmojiFilterCell", for: indexPath) as! FunnyEmojiFilterCell
        cell.lblItem.text = self.filterArray[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height:80)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var path: String?
        maskIndex = indexPath.row
        path = maskPaths[maskIndex]
        switchMode(path)
    }


    //MARK: DeepAr Methods
    private func switchMode(_ path: String?) {
        deepAR.switchEffect(withSlot: currentMode.rawValue, path: path)
    }

    @objc
    private func didTapSwitchCameraButton() {
        cameraController.position = cameraController.position == .back ? .front : .back
    }

    @objc
    private func didTapPreviousButton() {
        var path: String?
        maskIndex = (maskIndex - 1 < 0) ? (filterArray.count - 1) : (maskIndex - 1)
        collectionView.selectItem(at: NSIndexPath(item: maskIndex, section: 0) as IndexPath, animated: false, scrollPosition:.right)
        path = maskPaths[maskIndex]
        switchMode(path)
    }

    @objc
    private func didTapNextButton() {
        var path: String?
        maskIndex = (maskIndex + 1 > filterArray.count - 1) ? 0 : (maskIndex + 1)
        self.collectionView.scrollToItem(at:  NSIndexPath(item: maskIndex, section: 0) as IndexPath, at: .left, animated: true)
        collectionView.selectItem(at: NSIndexPath(item: maskIndex, section: 0) as IndexPath, animated: true, scrollPosition:[])
        path = maskPaths[maskIndex]
        switchMode(path)

    }

    func mergeFilesWithUrl(videoUrl:NSURL, audioUrl:NSURL){
        let mixComposition : AVMutableComposition = AVMutableComposition()
        var mutableCompositionVideoTrack : [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack : [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction : AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()

        //start merge
        let aVideoAsset : AVAsset = AVAsset(url: videoUrl as URL)
        let aAudioAsset : AVAsset = AVAsset(url: audioUrl as URL)

        mutableCompositionVideoTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        mutableCompositionAudioTrack.append( mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!)

        let aVideoAssetTrack : AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioAssetTrack : AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]

        do{
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)

            //In my case my audio file is longer then video file so i took videoAsset duration
            //instead of audioAsset duration

            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
            mutableCompositionVideoTrack[0].preferredTransform = aVideoAssetTrack.preferredTransform

        }catch{

        }

        totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero,duration: aVideoAssetTrack.timeRange.duration )

        let mutableVideoComposition : AVMutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        mutableVideoComposition.renderSize = CGSize.init(width: 1280, height:720)

        let documentsUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let savePathUrl = documentsUrl.appendingPathComponent("newVideo.mp4")
        self.karaokeURL = savePathUrl as NSURL
        do{
            try FileManager.default.removeItem(at: self.karaokeURL as URL)
        }catch{

        }

        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.outputURL = savePathUrl as URL
        assetExport.shouldOptimizeForNetworkUse = true

        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {
            case AVAssetExportSessionStatus.completed:
                //Uncomment this if u want to store your video in asset
                DispatchQueue.main.async {
                    self.hideLoader()
                }

            case  AVAssetExportSessionStatus.failed:
                DispatchQueue.main.async {
                    self.hideLoader()
                }
            case AVAssetExportSessionStatus.cancelled:
                DispatchQueue.main.async {
                    self.hideLoader()
                }
            default:
                DispatchQueue.main.async {
                    self.hideLoader()
                }
            }
        }
    }

    func mergeAudioFilesWithUrl(videoUrl:NSURL, audioUrl:NSURL)
    {
        let mixComposition : AVMutableComposition = AVMutableComposition()
        var mutableCompositionVideoTrack : [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack : [AVMutableCompositionTrack] = []

        let aVideoAsset : AVAsset = AVAsset(url: videoUrl as URL)
        let aAudioAsset : AVAsset = AVAsset(url: audioUrl as URL)

        mutableCompositionVideoTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        mutableCompositionAudioTrack.append( mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!)

        let aVideoAssetTrack : AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.audio)[0]
        let aAudioAssetTrack : AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]

        do{
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)

            mutableCompositionVideoTrack[0].preferredTransform = aVideoAssetTrack.preferredTransform

            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)

        }catch{
        }

        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let savePathUrl = documentsUrl.appendingPathComponent("newAudio.m4a")
        self.mergedAudioUrl = savePathUrl as NSURL

        do{
            try FileManager.default.removeItem(at: savePathUrl as URL)
        }catch{
        }

        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetAppleM4A)!
        assetExport.outputFileType = AVFileType.m4a
        assetExport.outputURL = savePathUrl as URL
        assetExport.shouldOptimizeForNetworkUse = true

        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {

            case AVAssetExportSessionStatus.completed:

                //Uncomment this if u want to store your video in asset
                DispatchQueue.main.async {
                    if(self.videoUrl != nil){
                        self.mergeFilesWithUrl(videoUrl: self.videoUrl, audioUrl: self.mergedAudioUrl)
                    }
                    else{
                        self.hideLoader()
                    }
                }

            case  AVAssetExportSessionStatus.failed:
                DispatchQueue.main.async {
                }
            case AVAssetExportSessionStatus.cancelled:
                DispatchQueue.main.async {
                }
            default:
                DispatchQueue.main.async {
                }
            }
        }
    }
}

extension KaraokeLipsingVC: DeepARDelegate {
    func didFinishPreparingForVideoRecording() { }

    func didStartVideoRecording() { }

    func didFinishVideoRecording(_ videoFilePath: String!) {

        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let components = videoFilePath.components(separatedBy: "/")
        guard let last = components.last else { return }
        let destination = URL(fileURLWithPath: String(format: "%@/%@", documentsDirectory, last))
        self.videoUrl = destination as NSURL

        DispatchQueue.main.async() {
            let loadmessage  = "Good Job!. Preparing preview.\nKereeennn !! Lihat  yuk.."
            self.showLoaderwithMessage(message: loadmessage)
        }
    }
    
    func recordingFailedWithError(_ error: Error!) {}
    func didTakeScreenshot(_ screenshot: UIImage!) {}
    func didInitialize() {}
    func faceVisiblityDidChange(_ faceVisible: Bool) {}
}
