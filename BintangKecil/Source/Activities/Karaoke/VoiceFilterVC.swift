//
//  VoiceFilterVC.swift
//  collectionview
//
//  Created by Devesh on 02/10/20.
//

import UIKit
import AVFoundation

class VoiceFilterVC: BaseViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!

    let margin: CGFloat = 10
    let AllarrVoiceFilter = ["Original", "Slow", "Fast","High Pitch","Low Pitch","Echo","Reverb"]

    var karaokeUrl : NSURL!

    var audioFile:AVAudioFile!
    var audioEngine:AVAudioEngine!
    var audioPlayerNode: AVAudioPlayerNode!
    var stopTimer: Timer!

    var outputUrl : URL!
    var controller = UIDocumentInteractionController()
    var selectedIndex : Int  = 0

    @IBOutlet weak var msgLbl: UILabel!
    var msgStr = ""
    var VoiceFilterCount = 0
    var arrVoiceFilter = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseNavBar()
        self.setupFilters()
        self.msgLbl.text = self.msgStr

        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        collectionView.reloadData()
        collectionView.selectItem(at: NSIndexPath(item: 0, section: 0) as IndexPath, animated: true, scrollPosition:[])

        self.setupAudio()

    }
    func setupFilters(){
        if(self.VoiceFilterCount == 0){
            self.arrVoiceFilter.append(contentsOf: self.AllarrVoiceFilter)
        }
        else{
            for ind in 0...self.VoiceFilterCount{
                self.arrVoiceFilter.append(self.AllarrVoiceFilter[ind])
            }
        }
    }

    override func backAction(sender: AnyObject) {
        if(audioPlayerNode != nil){
            if(audioPlayerNode.isPlaying == true){
                self.stopAudio()
            }
        }
        self.navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    @IBAction func btnShare(sender: AnyObject) {
        if(audioPlayerNode != nil){
            if(audioPlayerNode.isPlaying == true){
                self.stopAudio()
            }
        }

        switch(self.selectedIndex) {
        case 0:
            shareSound(rate: 1.0)
        case 1:
            shareSound(rate: 0.5)
        case 2:
            shareSound(rate: 1.5)
        case 3:
            shareSound(pitch: 1000)
        case 4:
            shareSound(rate: 0.95, pitch: -368, echo: false, reverb: true, vader: true)
        case 5:
            shareSound(echo: true)
        case 6:
            shareSound(reverb: true)
        default:
            shareSound(rate: 1.0)
        }

    }

    @IBAction func btnDownload(sender: AnyObject) {

        if(audioPlayerNode != nil){
            if(audioPlayerNode.isPlaying == true){
                self.stopAudio()
            }
        }

        switch(self.selectedIndex) {
        case 0:
            shareSound(rate: 1.0)
        case 1:
            shareSound(rate: 0.5)
        case 2:
            shareSound(rate: 1.5)
        case 3:
            shareSound(pitch: 1000)
        case 4:
            shareSound(rate: 0.95, pitch: -368, echo: false, reverb: true, vader: true)
        case 5:
            shareSound(echo: true)
        case 6:
            shareSound(reverb: true)
        default:
            shareSound(rate: 1.0)
        }

    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.arrVoiceFilter.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! collectionCell

        cell.lblFilter.text = self.arrVoiceFilter[indexPath.item]
        cell.imgFilter.image = UIImage.init(named:self.arrVoiceFilter[indexPath.row])
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 3   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size + 30)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if(audioPlayerNode != nil){
            if(audioPlayerNode.isPlaying == true){
                self.stopAudio()
            }
        }

        self.selectedIndex = indexPath.row

        switch(self.selectedIndex) {
        case 0:
            playSound(rate: 1.0)
        case 1:
            playSound(rate: 0.5)
        case 2:
            playSound(rate: 1.5)
        case 3:
            playSound(pitch: 1000)
        case 4:
            playSound(rate: 0.95, pitch: -368, echo: false, reverb: true, vader: true)
        case 5:
            playSound(echo: true)
        case 6:
            playSound(reverb: true)
        default:
            playSound(rate: 1.0)
        }
    }
}

class collectionCell: UICollectionViewCell{
    @IBOutlet weak var imgFilter: UIImageView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var lblFilter: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgBG.layer.borderWidth = 0.5
        self.imgBG.layer.borderColor = UIColor.white.cgColor

    }
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                self.imgBG.layer.borderWidth = 5.0
                self.imgBG.layer.borderColor = UIColor.white.cgColor
            }
            else
            {
                self.imgBG.layer.borderWidth = 0.5
                self.imgBG.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
}


extension VoiceFilterVC: AVAudioPlayerDelegate {

    func setupAudio() {
        // initialize (recording) audio file
        do {
            audioFile = try AVAudioFile(forReading: self.karaokeUrl as URL)
        } catch {
            showAlert(Alerts.AudioFileError, message: String(describing: error))
        }
    }

    // Mark: Play Sound

    func playSound(rate: Float? = nil, pitch: Float? = nil, echo: Bool = false, reverb: Bool = false, vader: Bool = false) {

        // initialize audio engine components
        audioEngine = AVAudioEngine()
        // node for playing audio
        audioPlayerNode = AVAudioPlayerNode()
        audioEngine.attach(audioPlayerNode)

        // node for adjusting rate/pitch
        let changeRatePitchNode = AVAudioUnitTimePitch()
        if let pitch = pitch {
            changeRatePitchNode.pitch = pitch
        }
        if let rate = rate {
            changeRatePitchNode.rate = rate
        }
        audioEngine.attach(changeRatePitchNode)

        // node for echo
        let echoNode = AVAudioUnitDistortion()
        echoNode.loadFactoryPreset(.multiEcho1)
        audioEngine.attach(echoNode)

        let reverbNode = AVAudioUnitReverb()
        if vader {
            // node for reverb
            reverbNode.loadFactoryPreset(.mediumHall)
            reverbNode.wetDryMix = 16
            audioEngine.attach(reverbNode)
        } else {
            // node for reverb
            reverbNode.loadFactoryPreset(.cathedral)
            reverbNode.wetDryMix = 50
            audioEngine.attach(reverbNode)
        }
        // connect nodes
        if echo == true && reverb == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, reverbNode, audioEngine.outputNode)
        } else if echo == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, audioEngine.outputNode)
        } else if reverb == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, reverbNode, audioEngine.outputNode)
        } else {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, audioEngine.outputNode)
        }

        // schedule to play and start the engine!
        audioPlayerNode.stop()
        audioPlayerNode.scheduleFile(audioFile, at: nil) {

            var delayInSeconds: Double = 0

            if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {

                if let rate = rate {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                } else {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                }
            }

            // schedule a stop timer for when audio finishes playing
            self.stopTimer = Timer(timeInterval: delayInSeconds, target: self, selector: #selector(FunnyVoiceVC.stopAudio), userInfo: nil, repeats: false)
            RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
        }

        do {
            try audioEngine.start()
        } catch {
            showAlert(Alerts.AudioEngineError, message: String(describing: error))
            return
        }

        // play the recording!
        audioPlayerNode.play()
    }

    @objc func stopAudio() {

        if let audioPlayerNode = audioPlayerNode {
            audioPlayerNode.stop()
        }
        if let stopTimer = stopTimer {
            stopTimer.invalidate()
        }
        if let audioEngine = audioEngine {
            audioEngine.stop()
            audioEngine.reset()
        }
    }

    // MARK: Connect List of Audio Nodes

    func connectAudioNodes(_ nodes: AVAudioNode...) {
        for x in 0..<nodes.count-1 {
            audioEngine.connect(nodes[x], to: nodes[x+1], format: audioFile.processingFormat)
        }
    }
    func shareSound(rate: Float? = nil, pitch: Float? = nil, echo: Bool = false, reverb: Bool = false, vader: Bool = false) {

        let loadmessage  = "Please wait!. This may take a while.\nTunggu sebentar!. Ini mungkin memakan waktu cukup lama."
        self.showLoaderwithMessage(message: loadmessage)

        // initialize audio engine components
        audioEngine = AVAudioEngine()

        // node for playing audio
        audioPlayerNode = AVAudioPlayerNode()
        audioEngine.attach(audioPlayerNode)

        // node for adjusting rate/pitch
        let changeRatePitchNode = AVAudioUnitTimePitch()
        if let pitch = pitch {
            changeRatePitchNode.pitch = pitch
        }
        if let rate = rate {
            changeRatePitchNode.rate = rate
        }
        audioEngine.attach(changeRatePitchNode)

        // node for echo
        let echoNode = AVAudioUnitDistortion()
        echoNode.loadFactoryPreset(.multiEcho1)
        audioEngine.attach(echoNode)

        let reverbNode = AVAudioUnitReverb()
        if vader {
            // node for reverb
            reverbNode.loadFactoryPreset(.mediumHall)
            reverbNode.wetDryMix = 16
            audioEngine.attach(reverbNode)
        } else {
            // node for reverb
            reverbNode.loadFactoryPreset(.cathedral)
            reverbNode.wetDryMix = 50
            audioEngine.attach(reverbNode)
        }


        // connect nodes
        if echo == true && reverb == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, reverbNode, audioEngine.outputNode)
        } else if echo == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, audioEngine.outputNode)
        } else if reverb == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, reverbNode, audioEngine.outputNode)
        } else {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, audioEngine.outputNode)
        }

        // schedule to play and start the engine!

        var delayInSeconds: Double = 0
        audioPlayerNode.stop()
        audioPlayerNode.scheduleFile(audioFile, at: nil) {
            if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {

                if let rate = rate {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                } else {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                }
            }

            // schedule a stop timer for when audio finishes playing
            self.stopTimer = Timer(timeInterval: delayInSeconds, target: self, selector: #selector(FunnyVoiceVC.stopAudio), userInfo: nil, repeats: false)
            RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
        }
        do {
            let maxNumberOfFrames: AVAudioFrameCount = 4096 // maximum number of frames the engine will be asked to render in any single render call
            try audioEngine.enableManualRenderingMode(.offline, format: audioFile.processingFormat, maximumFrameCount: maxNumberOfFrames)
        } catch {
            fatalError("could not enable manual rendering mode, \(error)")
        }

        do {
            try audioEngine.start()
            audioPlayerNode.play()
        } catch {
            showAlert(Alerts.AudioEngineError, message: String(describing: error))
            return
        }

        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0] as String
        let recordingName = "effectedSound2.caf"
        let pathArray = [dirPaths, recordingName]
        let filePath = URL(string: pathArray.joined(separator: "/"))!
        let filePathString: String = pathArray.joined(separator: "/")
        self.outputUrl = filePath

        let outputFile: AVAudioFile
        do {
            let outputURL = filePath
            outputFile = try AVAudioFile(forWriting: outputURL, settings: audioFile.fileFormat.settings)
        } catch {
            fatalError("could not open output audio file, \(error)")
        }

        // buffer to which the engine will render the processed data
        let buffer: AVAudioPCMBuffer = AVAudioPCMBuffer(pcmFormat: audioEngine.manualRenderingFormat, frameCapacity: audioEngine.manualRenderingMaximumFrameCount)!
        //: ### Render loop
        //: Pull the engine for desired number of frames, write the output to the destination file
        while audioEngine.manualRenderingSampleTime < audioFile.length {
            do {
                let framesToRender = min(buffer.frameCapacity, AVAudioFrameCount(audioFile.length - audioEngine.manualRenderingSampleTime))
                let status = try audioEngine.renderOffline(framesToRender, to: buffer)
                switch status {
                case .success:
                    // data rendered successfully
                    try outputFile.write(from: buffer)

                case .insufficientDataFromInputNode:
                    break
                case .cannotDoInCurrentContext:
                    break
                case .error:
                    break
                }
            } catch {
                fatalError("render failed, \(error)")
            }
        }

        // code
        audioPlayerNode.stop()
        audioEngine.stop()
        audioEngine.reset()
        self.hideLoader()

        controller = UIDocumentInteractionController(url: NSURL(fileURLWithPath: filePathString) as URL)
        controller.presentOpenInMenu(from: CGRect.zero,
                                     in: self.view,
                                     animated: true)
    }

}
