//
//  KaraokeListVC.swift
//  BintangKecil
//
//  Created by Devesh on 23/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage

class KaraokeListVC: BaseViewController, UITableViewDelegate, UITableViewDataSource, AVPlayerViewControllerDelegate, UITextFieldDelegate, categoriesAddDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearchField: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    private let refreshControl = UIRefreshControl()
    
    let margin: CGFloat = 10
    var playerController = AVPlayerViewController()
    var sort = "2"
    var order = "asc"
    var searchStr = ""
    var page = 1
    var dataArray = [KaraokeListModal]()
    
    var selectedIndex = 0
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var songNameLbl: UILabel!
    @IBOutlet weak var songdescLbl: UILabel!
    @IBOutlet weak var btnKaraoke: UIButton!
    @IBOutlet weak var btnLipsing: UIButton!
    var optionSelected = 0
    var EmojiCount = 0
    var VoicesCount = 0
    
    var totalRecords : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 120
        self.tableView.rowHeight = UITableView.automaticDimension
        self.txtSearchField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.KaraokeId = ""
        appDelegate.KaraokeNames = ""
        
        btnCancel.isHidden = true
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        tableView.alwaysBounceVertical = true
        tableView.refreshControl = refreshControl // iOS 10+
        
        self.getKaraokeData(page_no: 1, search: searchStr, sort_by: self.sort)
        
        popupView.frame = self.view.frame
        self.view.addSubview(popupView)
        popupView.isHidden = true
    }
    
    
    @objc
    private func didPullToRefresh(_ sender: Any) {
        self.page = 1
        dataArray.removeAll()
        self.tableView.reloadData()
        self.getKaraokeData(page_no: 1, search: searchStr, sort_by: self.sort)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.KaraokeId.isEmpty){
            btnFilter.setImage(UIImage.init(named:"filter"), for: .normal)
        }
        else{
            btnFilter.setImage(UIImage.init(named:"filter_selected"), for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.text!.count > 0){
            btnCancel.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let textFieldString = textField.text, let swtRange = Range(range, in: textFieldString) {
            let fullString = textFieldString.replacingCharacters(in: swtRange, with: string)
            let trimmed = fullString.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if(!trimmed.isEmpty){
                btnCancel.isHidden = false
            }
            else{
                btnCancel.isHidden = true
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField.text!.count > 0){
            self.txtSearchField.endEditing(true)
            self.searchStr = txtSearchField.text!
            dataArray.removeAll()
            self.tableView.reloadData()
            self.getKaraokeData(page_no: page, search: self.searchStr, sort_by: self.sort)
        }
        return true
    }
    
    @IBAction func FilterAction(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "categories") as! CategoriesVC
        vc.type = 1
        vc.category_type = 2
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func categoriesUpdatedSuccessfully() {
        self.page = 1
        self.txtSearchField.text = ""
        self.searchStr = ""
        self.dataArray.removeAll()
        self.tableView.reloadData()
        self.getKaraokeData(page_no: page, search: self.searchStr, sort_by: self.sort)
    }
    
    @objc func doneButtonClicked(_ sender: Any){
        self.txtSearchField.endEditing(true)
        self.searchStr = self.txtSearchField.text!
        dataArray.removeAll()
        self.tableView.reloadData()
        self.getKaraokeData(page_no: page, search: self.searchStr, sort_by: self.sort)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "KaraokeCell", for: indexPath as IndexPath) as! KaraokeCell
        
        let wrapper = self.dataArray[indexPath.row]
        
        cell.lblKaraokeTitle.text = wrapper.title
        cell.lblKaraokeDesc.text = wrapper.content
        cell.btnKaraokePlay.tag = indexPath.row
        cell.btnKaraokePlay.addTarget(self, action: #selector(PlayBtnClicked(_:)), for: .touchUpInside)
        cell.btnKaraokeStart.tag = indexPath.row
        cell.btnKaraokeStart.addTarget(self, action: #selector(StartBtnClicked(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        cell.dropShadow(UIColor.lightGray)
        return cell
        
    }
    
    @objc func StartBtnClicked(_ sender: UIButton){
        let wrapper = self.dataArray[sender.tag]
        self.selectedIndex = sender.tag
        if(!wrapper.lrcFile!.isEmpty){
            self.configurePopupView()
        }
        else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "KaraokeLipsingVC") as! KaraokeLipsingVC
            vc.mp3FilePath = wrapper.original_file_url!
            vc.songName = wrapper.title ?? ""
            vc.EmojiCount = self.EmojiCount
            vc.VoiceFilterCount = self.VoicesCount
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func configurePopupView(){
        let wrapper = self.dataArray[self.selectedIndex]
        songNameLbl.text = wrapper.title
        songdescLbl.text = wrapper.content
        self.optionSelected = 1
        btnKaraoke.setImage(UIImage.init(named:"radio_checked"), for: .normal)
        btnLipsing.setImage(UIImage.init(named:"radio_unchecked"), for: .normal)
        self.popupView.isHidden = false
        
    }
    
    @IBAction func  popupCancelClicked(){
        self.popupView.isHidden = true
    }
    
    @IBAction func  popupStartClicked(){
        let wrapper = self.dataArray[self.selectedIndex]
        self.popupView.isHidden = true
        if(self.optionSelected == 1){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "karaokeDetail") as! KaraokeDetailVC
            vc.lrcFilePath = wrapper.lrcFileURL!
            vc.mp3FilePath = wrapper.fileURL!
            vc.songName = wrapper.title ?? ""
            vc.EmojiCount = self.EmojiCount
            vc.VoiceFilterCount = self.VoicesCount
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "KaraokeLipsingVC") as! KaraokeLipsingVC
            vc.mp3FilePath = wrapper.original_file_url!
            vc.songName = wrapper.title ?? ""
            vc.EmojiCount = self.EmojiCount
            vc.VoiceFilterCount = self.VoicesCount
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func KarokeOptionClicked(_ sender: UIButton){
        if(sender.tag == 1){
            btnKaraoke.setImage(UIImage.init(named:"radio_checked"), for: .normal)
            btnLipsing.setImage(UIImage.init(named:"radio_unchecked"), for: .normal)
            self.optionSelected = 1
        }
        else{
            btnLipsing.setImage(UIImage.init(named:"radio_checked"), for: .normal)
            btnKaraoke.setImage(UIImage.init(named:"radio_unchecked"), for: .normal)
            self.optionSelected = 2
            
        }
    }
    
    @IBAction func KarokePlayClicked(_ sender: UIButton){
        let wrapper = self.dataArray[self.selectedIndex]
        let urldata = URL(string: wrapper.original_file_url ?? "")
        let player = AVPlayer(url:urldata!)
        playerController = AVPlayerViewController()
        playerController.player = player
        playerController.allowsPictureInPicturePlayback = true
        playerController.delegate = self
        playerController.player?.play()
        
        playerController.contentOverlayView?.backgroundColor = UIColor(displayP3Red: 180/255, green: 151/255, blue: 57/255, alpha: 1.0)
        
        let theImageView = UIImageView()
        theImageView.image = UIImage(named: "child")
        theImageView.frame = CGRect(x: self.view.bounds.width/2, y: self.view.bounds.height/2, width: 150, height: 150)
        theImageView.contentMode = .center
        playerController.contentOverlayView?.addSubview(theImageView)
        self.present(playerController,animated:true,completion:nil)
        
    }
    
    @objc func PlayBtnClicked(_ sender: UIButton){
        
        let wrapper = self.dataArray[sender.tag]
        let urldata = URL(string: wrapper.original_file_url ?? "")
        let player = AVPlayer(url:urldata!)
        playerController = AVPlayerViewController()
        playerController.player = player
        playerController.allowsPictureInPicturePlayback = true
        playerController.delegate = self
        playerController.player?.play()
        
        playerController.contentOverlayView?.backgroundColor = UIColor(displayP3Red: 180/255, green: 151/255, blue: 57/255, alpha: 1.0)
        
        let theImageView = UIImageView()
        theImageView.image = UIImage(named: "child")
        theImageView.frame = CGRect(x: self.view.bounds.width/2, y: self.view.bounds.height/2, width: 150, height: 150)
        theImageView.contentMode = .center
        playerController.contentOverlayView?.addSubview(theImageView)
        self.present(playerController,animated:true,completion:nil)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if self.dataArray.count-1 == indexPath.row{
            self.getMorekaraoke(page: page)
        }
    }
    
    func getMorekaraoke(page:Int){
        
        if(self.dataArray.count < self.totalRecords){
            self.page = self.page + 1
            self.getKaraokeData(page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
    }
    
    @IBAction func btncrossClicked(_ sender: Any){
        self.txtSearchField.text = ""
        self.searchStr = ""
        self.btnCancel.isHidden = true
        self.page = 1
        self.dataArray.removeAll()
        self.tableView.reloadData()
        self.getKaraokeData(page_no: page, search: self.searchStr, sort_by: self.sort)
    }
    
    @IBAction func SortingControl(_ sender: Any){
        let alert = UIAlertController(title: "Sort By", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        alert.addAction(UIAlertAction(title: "Name (A-Z)", style: .default , handler:{ (UIAlertAction)in
            self.order = "asc"
            self.sort = "1"
            self.dataArray.removeAll()
            self.getKaraokeData(page_no: self.page, search: self.searchStr, sort_by: self.sort)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Name (Z-A)", style: .default , handler:{ (UIAlertAction)in
            self.order = "desc"
            self.sort = "1"
            self.dataArray.removeAll()
            self.getKaraokeData(page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }))
        
        alert.addAction(UIAlertAction(title: "Date (Ascending)", style: .default , handler:{ (UIAlertAction)in
            self.sort = "2"
            self.order = "asc"
            self.dataArray.removeAll()
            self.getKaraokeData(page_no: self.page, search: self.searchStr, sort_by: self.sort)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Date (Descending)", style: .default , handler:{ (UIAlertAction)in
            self.sort = "2"
            self.order = "desc"
            self.dataArray.removeAll()
            self.getKaraokeData(page_no: self.page, search: self.searchStr, sort_by: self.sort)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    func getKaraokeData(page_no: Int, search: String, sort_by:String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        var parameter = [String:Any]()
        parameter["category_ids"] = appDelegate.KaraokeId
        parameter["pageNumber"] = page_no
        parameter["search"] = search
        parameter["sort_by"] = sort_by
        parameter ["sort_order"] = self.order
        showLoader()
        postRequest(strUrl: "karaoke/list", param: parameter, success: { (json) in
            if json["success"].stringValue == "true"{
                self.refreshControl.endRefreshing()
                
                if json["message"].stringValue == "No Records Found"{
                    self.ShowAlert(message: "No Record Found.")
                }
                else{
                    let arrData = json["data"].dictionaryValue["records"]!.arrayValue
                    self.totalRecords = json["data"]["totalRecords"].intValue
                    if(arrData.count > 0){
                        for ind in 0...arrData.count - 1{
                            let wrapper = KaraokeListModal.init(
                                id: arrData[ind]["id"].intValue,
                                tags: arrData[ind]["tags"].stringValue,
                                fileURL: arrData[ind]["file_url"].stringValue,
                                category: arrData[ind]["category"].stringValue,
                                lrcFile: arrData[ind]["lrc_file"].stringValue,
                                title: arrData[ind]["title"].stringValue,
                                file: arrData[ind]["file"].stringValue,
                                createdDateTime: arrData[ind]["created_date_time"].stringValue,
                                filteredContent: arrData[ind][""].stringValue,
                                fileName: arrData[ind]["file_name"].stringValue,
                                content: arrData[ind]["content"].stringValue,
                                lrcFileURL: arrData[ind]["lrc_file_url"].stringValue,
                                lrcFileName: arrData[ind]["lrc_file_name"].stringValue,original_file_url: arrData[ind]["original_file_url"].stringValue)
                            
                            self.dataArray.append(wrapper)
                        }
                        self.tableView.reloadData()
                    }
                }
            }
            self.hideLoader()
        })
        {
            (msg) in
            self.ShowAlert(message: msg)
        }
    }    
}

class KaraokeCell: UITableViewCell {
    @IBOutlet weak var imgSongView: UIImageView!
    @IBOutlet weak var lblKaraokeTitle: UILabel!
    @IBOutlet weak var lblKaraokeDesc: UILabel!
    @IBOutlet weak var btnKaraokePlay: UIButton!
    @IBOutlet weak var btnKaraokeStart: UIButton!
}
