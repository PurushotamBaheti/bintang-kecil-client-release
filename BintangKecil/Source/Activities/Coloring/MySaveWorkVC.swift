//
//  MySaveWorkVC.swift
//  BintangKecil
//
//  Created by Devesh on 02/10/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SDWebImage


class MySaveWorkVC: BaseViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var savedArr = [savedRecord]()
    
    let margin: CGFloat = 10


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = 20
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)

    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.tabBarController?.tabBar.isHidden = true
        self.savedArr.removeAll()
        self.collectionView.reloadData()
        self.fetchSavedList()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.savedArr.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SaveWorkCollectionCell", for: indexPath) as! SaveWorkCollectionCell

        let wrapper = self.savedArr[indexPath.item]
        cell.lblSavedWork.text = wrapper.userName
        cell.imgSavedWork.sd_setImage(with: URL(string: wrapper.file), placeholderImage: UIImage(named: "child"))
        cell.btnDelete.tag = wrapper.id
        cell.btnDelete.addTarget(self, action: #selector(deletebtnSaved), for: .touchUpInside)
        cell.dropShadow(UIColor.lightGray)
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size + 30)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let wrapper = self.savedArr[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CanvasVC") as! CanvasVC
        vc.pageTitle =  "Activity"

        vc.colorImg = wrapper.file
        vc.musicFile = ""
        let sketch = SketchData(identifier: UUID().uuidString, lastModifiedDate: Date())
        sketch.parentFolderName = ""
        vc.selectedSketch = sketch


        self.navigationController?.pushViewController(vc, animated: false)

    }

    func fetchSavedList() {
        self.collectionView.backgroundView = nil

        var parameter = [String:Any]()
        let isChild = UserDefaults.standard.getChild()
        if isChild{

            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            parameter["child_id"] = decodedData.id

        }
        showLoader()
        postRequest(strUrl: "user-coloring-image/list", param: parameter, success: { (json) in
            if json["success"].stringValue == "true" {
                if json["message"].stringValue == "No Records Found" {
                    self.CreateNoRecordsView()
                }
                else
                {
                    let arrData = json["data"].dictionaryValue["records"]!.arrayValue
                    if(arrData.count > 0) {
                        for ind in 0...arrData.count - 1 {
                            let wrapper = savedRecord.init(createdAt: arrData[ind]["created_at"].stringValue,
                                                           id: arrData[ind]["id"].intValue,
                                                           userName: arrData[ind]["user_name"].stringValue,
                                                           file: arrData[ind]["file"].stringValue,
                                                           posterImage: arrData[ind]["poster_image"].stringValue,
                                                           childID: arrData[ind]["child_id"].stringValue,
                                                           userID: arrData[ind]["user_id"].intValue)

                            self.savedArr.append(wrapper)
                        }

                        self.collectionView.reloadData()
                    }
                }
            }
            self.hideLoader()
        })
        {
            (msg) in

            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func CreateNoRecordsView() {
        let popupview = UIView()
        popupview.backgroundColor = UIColor.clear
        self.collectionView.backgroundView = popupview

        let ycor = (ScreenHeight - self.collectionView.frame.origin.y)/2

        let subview = UIView()
        subview.frame = CGRect.init(x:30, y: ycor - 160, width: popupview.frame.size.width - 60, height: 160)
        subview.backgroundColor = UIColor.clear
        popupview.addSubview(subview)

        let logo = UIImageView.init()
        logo.frame = CGRect.init(x:(subview.frame.size.width - 80)/2, y:20, width: 80, height: 80)
        logo.image = UIImage.init(named:"logo")
        logo.contentMode = .scaleAspectFit
        subview.addSubview(logo)

        let messageLbl = UILabel()
        messageLbl.frame = CGRect.init(x: 5, y: 90, width: subview.frame.size.width - 10, height: 50)
        messageLbl.backgroundColor = UIColor.clear
        messageLbl.text = "No Saved work yet."
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.init(name: "Metropolis-ExtraLight", size: 13.0)
        messageLbl.textColor = UIColor.black
        messageLbl.numberOfLines = 3
        subview.addSubview(messageLbl)

    }
    
    @objc func deletebtnSaved(sender:UIButton) {

        let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to Delete this?", preferredStyle: .alert)

        // Create OK button with action handler
        let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in

            var parameter = [String:Any]()
            parameter["record_id"] = sender.tag

            self.showLoader()
            postRequest(strUrl: "user-coloring-image/remove", param: parameter, success: { (json) in
                self.hideLoader()
                if json["status"].stringValue == "200" {
                    self.savedArr.removeAll()
                    self.collectionView.reloadData()
                    self.fetchSavedList()
                }
                else {
                    let message = json["message"].stringValue
                    self.showToast(message: message)
                }

            }) { (msg) in
                self.hideLoader()
                self.ShowAlert(message: msg)
            }

        })

        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
        }

        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)

        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)

    }
    
}

class SaveWorkCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblSavedWork: UILabel!
    @IBOutlet weak var imgSavedWork: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
}

class savedRecord {
    let createdAt: String
    let id: Int
    let userName: String
    let file, posterImage: String
    let childID: String
    let userID: Int

    init(createdAt: String, id: Int, userName: String, file: String, posterImage: String, childID: String, userID: Int) {
        self.createdAt = createdAt
        self.id = id
        self.userName = userName
        self.file = file
        self.posterImage = posterImage
        self.childID = childID
        self.userID = userID
    }
}
