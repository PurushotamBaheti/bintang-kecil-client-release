//
//  ColoringVC.swift
//  BintangKecil
//
//  Created by Purushotam on 26/08/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SDWebImage


class ColoringVC: BaseViewController,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITextFieldDelegate,ColoringCategoriesDelegate {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataArray = [ColoringModel]()
    var pageTitle = ""
    var page = 1
    var sort = "2"
    var order = "asc"
    var categoryId = ""
    var searchStr = ""
    private let refreshControl = UIRefreshControl()
    var isForColoring:Bool = false
    let margin: CGFloat = 10
    @IBOutlet weak var txtSearchField: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    
    var totalRecords = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseUI()
        if(self.isForColoring == true) {
            self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
        else {
            self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            
        }
        txtSearchField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        btnCancel.isHidden = true
    }
    
    @IBAction func SavedWorkClicked(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MySaveWorkVC") as! MySaveWorkVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func doneButtonClicked(_ sender: Any) {
        //your code when clicked on done
        self.txtSearchField.endEditing(true)
        self.searchStr = self.txtSearchField.text!
        
        if(self.isForColoring == true) {
            self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
        else {
            self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            
        }
    }
    
    @IBAction func FilterAction(sender: AnyObject) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColoringCategoriesVC") as! ColoringCategoriesVC
        
        if(self.isForColoring == true) {
            vc.ApiUrl = "coloring-image-category-list"
        }
        else {
            vc.ApiUrl = "tracing-image-category-list"
            
        }
        vc.selectedCategories = self.categoryId
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ColoringCategoriesSelectedSuccessfully(categories: String) {
        
        self.categoryId = categories
        if(self.categoryId.isEmpty){
            btnFilter.setImage(UIImage.init(named:"filter"), for: .normal)
        }
        else {
            btnFilter.setImage(UIImage.init(named:"filter_selected"), for: .normal)
        }
        
        self.page = 1
        if(self.isForColoring == true) {
            self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
        else{
            self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
    }
    
    @IBAction func SortingControl(_ sender: Any){
        let alert = UIAlertController(title: "Sort By", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: "Name (A-Z)", style: .default , handler:{ (UIAlertAction)in
            self.order = "asc"
            self.sort = "1"
            self.page = 1
            
            if(self.isForColoring == true){
                self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: self.categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            }
            else{
                self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: self.categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Name (Z-A)", style: .default , handler:{ (UIAlertAction)in
            self.order = "desc"
            self.sort = "1"
            self.page = 1
            if(self.isForColoring == true){
                self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: self.categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            }
            else{
                self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: self.categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Date (Ascending)", style: .default , handler:{ (UIAlertAction)in
            self.sort = "2"
            self.order = "asc"
            self.page = 1
            if(self.isForColoring == true){
                self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: self.categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            }
            else{
                self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: self.categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
                
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Date (Descending)", style: .default , handler:{ (UIAlertAction)in
            self.sort = "2"
            self.order = "desc"
            self.page = 1
            if(self.isForColoring == true){
                self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: self.categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            }
            else{
                self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: self.categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
    }
    @IBAction func btncrossClicked(_ sender: Any){
        self.txtSearchField.text = ""
        self.searchStr = ""
        self.btnCancel.isHidden = true
        self.page = 1
        if(self.isForColoring == true){
            self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
        else{
            self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.text!.count > 0){
            btnCancel.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if let textFieldString = textField.text, let swtRange = Range(range, in: textFieldString) {
            let fullString = textFieldString.replacingCharacters(in: swtRange, with: string)
            let trimmed = fullString.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if(!trimmed.isEmpty){
                btnCancel.isHidden = false
            }
            else{
                btnCancel.isHidden = true
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField.text!.count > 0){
            self.txtSearchField.endEditing(true)
            self.searchStr = txtSearchField.text!
            
            if(self.isForColoring == true){
                self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            }
            else{
                self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            }
        }
        return true
    }
    
    func customiseUI(){
        self.titleLbl.text = self.pageTitle
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = 20
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        collectionView.alwaysBounceVertical = true
        collectionView.refreshControl = refreshControl // iOS 10+
    }
    
    
    @objc
    private func didPullToRefresh(_ sender: Any) {
        self.page = 1
        dataArray.removeAll()
        self.collectionView.reloadData()
        
        if(self.isForColoring == true){
            self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
        else{
            self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func fetchPictureList(apiUrl:String, category_id: String, page_no: Int, search: String, sort_by:String ){
        self.collectionView.backgroundView = nil
        if(self.page == 1){
            self.dataArray.removeAll()
            self.collectionView.reloadData()
        }
        
        var parameter = [String:Any]()
        parameter["category_ids"] = category_id
        parameter["pageNumber"] = page_no
        parameter["search"] = search
        parameter["sort_by"] = sort_by
        parameter ["sort_order"] = self.order
        
        showLoader()
        postRequest(strUrl: apiUrl, param: parameter, success: { (json) in
            if json["success"].stringValue == "true"{
                self.refreshControl.endRefreshing()
                
                if json["message"].stringValue == "No Records Found"{
                    self.CreateNoFiltersView()
                }else{
                    let arrData = json["data"].dictionaryValue["records"]!.arrayValue
                    self.totalRecords = json["data"]["totalRecords"].intValue
                    if(arrData.count > 0){
                        for ind in 0...arrData.count - 1{
                            let wrapper = ColoringModel.init(fromJson: arrData[ind])
                            self.dataArray.append(wrapper)
                        }
                        self.collectionView.reloadData()
                    }
                }
            }
            self.hideLoader()
        })
        {
            (msg) in
            self.ShowAlert(message: msg)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.dataArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColoringCollectionViewCell", for: indexPath) as! ColoringCollectionViewCell
        
        cell.contentView.layer.cornerRadius = 5
        cell.contentView.clipsToBounds = true
        
        let wrapper = self.dataArray[indexPath.row]
        cell.lblItem.text = wrapper.title
        cell.lblCategory.text = wrapper.category
        cell.lblImage.contentMode = .scaleAspectFit
        cell.lblImage.sd_setImage(with: URL(string: wrapper.file_url!), placeholderImage: UIImage(named: "child"))
        cell.dropShadow(UIColor.lightGray)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size , height: size)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        if self.dataArray.count-1 == indexPath.row && self.dataArray.count < self.totalRecords {
            getMoreData(page: page)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let wrapper = self.dataArray[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CanvasVC") as! CanvasVC
        
        if(self.isForColoring == true) {
            vc.pageTitle = "Coloring"
        }
        else {
            vc.pageTitle = "Tracing"
            
        }
        vc.colorImg = wrapper.file_url
        vc.musicFile = wrapper.backgroundMusic!
        let sketch = SketchData(identifier: UUID().uuidString, lastModifiedDate: Date())
        sketch.parentFolderName = ""
        vc.selectedSketch = sketch
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func CreateNoFiltersView() {
        let popupview = UIView()
        popupview.backgroundColor = UIColor.clear
        self.collectionView.backgroundView = popupview
        
        let ycor = (ScreenHeight - self.collectionView.frame.origin.y)/2
        
        let subview = UIView()
        subview.frame = CGRect.init(x:30, y: ycor - 160, width: popupview.frame.size.width - 60, height: 160)
        subview.backgroundColor = UIColor.clear
        popupview.addSubview(subview)
        
        let logo = UIImageView.init()
        logo.frame = CGRect.init(x:(subview.frame.size.width - 80)/2, y:20, width: 80, height: 80)
        logo.image = UIImage.init(named:"logo")
        logo.contentMode = .scaleAspectFit
        subview.addSubview(logo)
        
        let messageLbl = UILabel()
        messageLbl.frame = CGRect.init(x: 5, y: 90, width: subview.frame.size.width - 10, height: 50)
        messageLbl.backgroundColor = UIColor.clear
        if(!self.categoryId.isEmpty){
            messageLbl.text = "No records found for the given criteria.Please try with different filters."
        }
        else{
            messageLbl.text = "No records found."
            
        }
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.init(name: "Metropolis-ExtraLight", size: 13.0)
        messageLbl.textColor = UIColor.white
        messageLbl.numberOfLines = 3
        subview.addSubview(messageLbl)
        
    }
    
    
    func getMoreData(page:Int){
        self.page = self.page + 1
        if(self.isForColoring == true){
            self.fetchPictureList(apiUrl: "coloring-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
        else{
            self.fetchPictureList(apiUrl: "tracing-image/list" , category_id: categoryId, page_no: self.page, search: self.searchStr, sort_by: self.sort)
        }
    }
}

class ColoringCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblImage: UIImageView!
    
}
