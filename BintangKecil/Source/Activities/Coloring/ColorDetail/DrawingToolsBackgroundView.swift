//
//  DrawingToolsBackgroundView.swift
//  Croquipad
//
//  Created by Pankaj Dhiman on 12/18/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

class DrawingToolsBackgroundView: UIView {
    var bgColot =  UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUpGraphicsView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.setUpGraphicsView()
    }
    
    func setUpGraphicsView() {
        let bgView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - 10, height: self.frame.size.height))
        bgView.translatesAutoresizingMaskIntoConstraints = true
        bgView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bgView.backgroundColor = bgColot
        self.insertSubview(bgView, at: 0)

        let graphicView = UIView(frame: CGRect(x: 10, y: 0, width: self.frame.size.width - 10, height: self.frame.size.height))
        graphicView.translatesAutoresizingMaskIntoConstraints = true
        graphicView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        graphicView.backgroundColor = bgColot
        graphicView.layer.cornerRadius = 15.0
        graphicView.clipsToBounds = true
        self.insertSubview(graphicView, at: 1)
    }
}
