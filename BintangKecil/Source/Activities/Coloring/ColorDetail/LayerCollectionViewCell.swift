//
//  layerCollectionViewCell.swift
//  Croquipad
//
//  Created by Dhruv Khatri on 18/11/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

class LayerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewLayer: UIView!
    @IBOutlet weak var imgLayer: UIImageView!
    @IBOutlet weak var longGesture: UILongPressGestureRecognizer!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var showHideBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        viewLayer.layer.cornerRadius = 3.0
        viewLayer.layer.borderWidth = 1.0
        viewLayer.layer.borderColor = UIColor.brown.cgColor
        viewLayer.clipsToBounds = true
        btnDelete.isHidden = true

        self.btnDelete.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
    }
    
    func setEditable(_ editable:Bool) {
        if editable {
            UIView.animate(withDuration: 0.3) { () -> Void in
                self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
            UIView.animate(withDuration: 0.3) { () -> Void in
                self.btnDelete.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }
            btnDelete.isHidden = false
        }
        else {
            
            viewLayer.frame = CGRect(x: 10, y: 2, width: self.frame.size.width - 20 , height: self.frame.size.height - 4)
            btnDelete.isHidden = true

        }
    }
}
