//
//  DrawingToolBackgroundView.swift
//  Croquipad
//
//  Created by Pankaj Dhiman on 12/18/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

class DrawingToolBackgroundView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
        
    }
    func setupView(){
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
        self.layer.borderWidth = 0.9
        self.layer.borderColor = UIColor.init(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 0.9).cgColor
    }
}
