//
//  layerTableViewCell.swift
//  Croquipad
//
//  Created by dhruv Khatri on 08/12/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

class layerTableViewCell: UITableViewCell {

    @IBOutlet weak var lblLayerTitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
