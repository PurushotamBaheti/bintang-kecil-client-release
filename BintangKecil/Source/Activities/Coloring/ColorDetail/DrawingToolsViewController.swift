//
//  DrawingToolsViewController.swift
//  Croquipad
//
//  Created by Pankaj Dhiman on 04/10/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

protocol DrawingToolsViewControllerDelegate : class {
    func drawingToosInExpanded(mode:Bool)
    func didChangeDrawing(data:DrawingData)
}

enum DrawingTool {
    case brush
    case size
    case opacity
    case color
    case settings
}
enum BrushType {
    case pencil
    case crayon
    case highlighter
    case painbrush
    case eraser
}

class DrawingData {
    var brushType : BrushType = BrushType.pencil
    var Size : Float = 2.0
    var opacity : Float = 1.0
    var color : UIColor = UIColor.black
    var settings : String = ""
    
    init() {
        
    }
}

class DrawingToolsViewController: UIViewController{

    @IBOutlet weak var mainToolsStackView: UIStackView!
    @IBOutlet weak var brushStackView: UIStackView!
    @IBOutlet weak var sizeStackView: UIStackView!
    @IBOutlet weak var opacityStackView: UIStackView!
    @IBOutlet weak var colorStackView: UIStackView!
    @IBOutlet weak var settingsStackView: UIStackView!
    @IBOutlet weak var btnBrush: UIButton!
    
    @IBOutlet weak var colorMixtureView: UIView!
    @IBOutlet weak var pantonView: UIView!

    @IBOutlet weak var opacityView: AlphaPickerView!
    @IBOutlet weak var sizeImagewidth: NSLayoutConstraint!

    weak var delegate : DrawingToolsViewControllerDelegate?
    var lastSelected : DrawingTool? = nil
    var drawingData = DrawingData()

    static var brushselectedIndex = 0
    static var brushSelectedColor = UIColor.black
    static var brushSelectedWidth = 1.0
    static var brushSelectedOpacity = 1.0

    @IBOutlet var brushselectorList: Array<UIImageView>?
    @IBOutlet var toolselectorList: Array<UIImageView>?
    @IBOutlet weak var backgroundView: UIView!
    
    var dictionary = [Float: [Float]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sizeImagewidth.constant = 4.0
        dictionary[0] = [1 , 4]
        dictionary[1] =  [5 , 10]
        dictionary[2] = [10 , 15]
        dictionary[3] = [10 , 30]
        dictionary[4] = [10 , 30]

        // Do any additional setup after loading the view.
        setDefaultContents()
        
        pantonView.isHidden = true
        
        // initnialise color
        opacityView.displayedColor = drawingData.color
        opacityView.addTarget(self, action: #selector(opacityChanged(_:)), for: .valueChanged)
        opacityView.updateMarkerPosition()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        DrawingToolsViewController.brushselectedIndex = 0
        DrawingToolsViewController.brushSelectedColor = UIColor.black
        self.setBrushselectedview(selindex: 0)
        
        let selIndex : Float = Float(DrawingToolsViewController.brushselectedIndex)
        let tempdict = self.dictionary[selIndex]!
        let lowerval = tempdict[0]
        drawingData.Size = lowerval
        sizeImagewidth.constant = CGFloat(4.0) + CGFloat(lowerval)
        self.setBrushselectedview(selindex: DrawingToolsViewController.brushselectedIndex)
        self.perform(#selector(updateViewwithDelay), with: nil, afterDelay: 1.0)

    }

    @objc func updateViewwithDelay(){
        delegate?.didChangeDrawing(data:drawingData)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func colorFromPantoneSelector(_ sender: Any) {
        
        let tag = (sender as! UIButton).tag
        if tag == 1000 { // 252,211,82
            drawingData.color = UIColor(red: 252/255.0, green: 211/255.0, blue: 82/255.0, alpha: 1.0)
        }
        else if tag == 1001 { // 243, 205, 196
            drawingData.color = UIColor(red: 243/255.0, green: 205/255.0, blue: 196/255.0, alpha: 1.0)
        }
        else if tag == 1002 { // 213, 174, 146
            drawingData.color = UIColor(red: 213/255.0, green: 174/255.0, blue: 146/255.0, alpha: 1.0)
        }
        else if tag == 1003 { // 142, 214, 255
            drawingData.color = UIColor(red: 142/255.0, green: 214/255.0, blue: 255/255.0, alpha: 1.0)
        }
        else if tag == 1004 { // 135, 180, 74
            drawingData.color = UIColor(red: 135/255.0, green: 180/255.0, blue: 74/255.0, alpha: 1.0)
        }
        else if tag == 1005 {  // 249, 77, 26
            drawingData.color = UIColor(red: 249/255.0, green: 77/255.0, blue: 26/255.0, alpha: 1.0)
        }
        
        DrawingToolsViewController.brushSelectedColor = drawingData.color
        opacityView.displayedColor = drawingData.color
        delegate?.didChangeDrawing(data:drawingData)
        
        if let last = lastSelected {
            show(drawingTool: last)
        }
    }
    
    @IBAction func colorSwitchSelector(_ sender: Any) {

        let tag = (sender as! UIButton).tag
        if tag == 1000 {  // color mixture
            colorMixtureView.isHidden = false
            pantonView.isHidden = true
        }
        else {
            colorMixtureView.isHidden = true
            pantonView.isHidden = false
        }
    }
    
    @IBAction func drawingToolSelector(_ sender: Any) {

        let tag = (sender as! UIButton).tag
        if tag == 1000 {  // brush
            show(drawingTool: DrawingTool.brush)
        }
        else if tag == 1001 { // size
            show(drawingTool: DrawingTool.size)
        }
        else if tag == 1002 { // opacity
            show(drawingTool: DrawingTool.opacity)
        }
        else if tag == 1003 { // color
            show(drawingTool: DrawingTool.color)
        }
        else if tag == 1004 { // settings
            show(drawingTool: DrawingTool.settings)
        }
    }
    
    @IBAction func brushSelector(_ sender: Any) {
        
        let tag = (sender as! UIButton).tag
        if tag == 1000 {  // pencil
            drawingData.brushType = BrushType.pencil
            DrawingToolsViewController.brushselectedIndex = 0
            self.setBrushselectedview(selindex: DrawingToolsViewController.brushselectedIndex)
        }
        else if tag == 1001 { // crayon
            drawingData.brushType = BrushType.crayon
            DrawingToolsViewController.brushselectedIndex = 1
            self.setBrushselectedview(selindex: DrawingToolsViewController.brushselectedIndex)
        }
        else if tag == 1002 { // highlighter
            drawingData.brushType = .highlighter
            DrawingToolsViewController.brushselectedIndex = 2
            self.setBrushselectedview(selindex: DrawingToolsViewController.brushselectedIndex)
        }
        else if tag == 1003 { // paint brush
            drawingData.brushType = BrushType.painbrush
            DrawingToolsViewController.brushselectedIndex = 3
            self.setBrushselectedview(selindex: DrawingToolsViewController.brushselectedIndex)
        }
        else if tag == 1004 { // eraser
            drawingData.brushType = .eraser
            DrawingToolsViewController.brushselectedIndex = 4
            self.setBrushselectedview(selindex: DrawingToolsViewController.brushselectedIndex)
        }

        let selIndex : Float = Float(DrawingToolsViewController.brushselectedIndex)
        
        let tempdict = self.dictionary[selIndex]!
        let lowerval = tempdict[0]
        drawingData.Size = lowerval
        sizeImagewidth.constant = CGFloat(4.0) + CGFloat(lowerval)
        
        delegate?.didChangeDrawing(data:drawingData)
        brushStackView.isHidden = true
        self.resettoolselectedview()
        delegate?.drawingToosInExpanded(mode: false)
        self.resettoolselectedview()
        lastSelected = nil
    }
    
    @IBAction func sizeSelector(_ sender: Any) {

        let selIndex : Float = Float(DrawingToolsViewController.brushselectedIndex)
        let tempdict = self.dictionary[selIndex]!
        let lowerval = tempdict[0]
        let upperval = tempdict[1]
        
        let tag = (sender as! UIButton).tag
        if tag == 1000 {  // Add
            if(drawingData.Size < upperval){
                drawingData.Size = drawingData.Size + 1
                sizeImagewidth.constant = CGFloat(4.0) + CGFloat(drawingData.Size - lowerval) + CGFloat(lowerval)
            }
        }

        else if tag == 1001 { // substract

            if(drawingData.Size > lowerval) {
                drawingData.Size = drawingData.Size - 1
                sizeImagewidth.constant = CGFloat(4.0) + CGFloat(drawingData.Size - lowerval) + CGFloat(lowerval)
            }
        }
        
        delegate?.didChangeDrawing(data:drawingData)
    }
    
    func show(drawingTool:DrawingTool) {
        
        // hide last selected item
        if let lastOne = lastSelected {
            
            switch lastOne {
            case DrawingTool.brush:
                brushStackView.isHidden = true
                self.resettoolselectedview()
                break
            case DrawingTool.size:
                sizeStackView.isHidden = true
                self.resettoolselectedview()
                break
            case DrawingTool.opacity:
                opacityStackView.isHidden = true
                self.resettoolselectedview()
                break
            case DrawingTool.color:
                colorStackView.isHidden = true
                self.resettoolselectedview()
                break
            case DrawingTool.settings:
                settingsStackView.isHidden = true
                self.resettoolselectedview()
                break
            }
            
            // if click on second times on same button
            if lastOne == drawingTool {
                delegate?.drawingToosInExpanded(mode: false)
                self.resettoolselectedview()
                lastSelected = nil
                self.view.layer.borderColor = UIColor.clear.cgColor
                return
            }
        }
        
        self.resettoolselectedview()
        
        // visible selected item
        lastSelected = drawingTool
        switch drawingTool {
        case DrawingTool.brush:
            brushStackView.isHidden = false
            self.setBrushselectedview(selindex:DrawingToolsViewController.brushselectedIndex)
            toolselectorList![0].backgroundColor = UIColor.init(red: 237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            break
        case DrawingTool.size:
            sizeStackView.isHidden = false
            toolselectorList![1].backgroundColor = UIColor.init(red: 237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            break
        case DrawingTool.opacity:
            opacityStackView.isHidden = false
            toolselectorList![2].backgroundColor = UIColor.init(red: 237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            break
        case DrawingTool.color:
            colorStackView.isHidden = false
            toolselectorList![3].backgroundColor = UIColor.init(red: 237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            break
        case DrawingTool.settings:
            settingsStackView.isHidden = false
            toolselectorList![4].backgroundColor = UIColor.init(red: 237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            break
            
        }
        
        self.view.layer.borderColor = UIColor.lightGray.cgColor
        delegate?.drawingToosInExpanded(mode: true)
    }
    
    func setDefaultContents () {
        mainToolsStackView.isHidden = false
        brushStackView.isHidden = true
        sizeStackView.isHidden = true
        opacityStackView.isHidden = true
        colorStackView.isHidden = true
        settingsStackView.isHidden = true
    }

    // MARK : Call back from color picker control
    
    @IBAction func colorChanged(_ sender: ColorPickerView) {
        self.drawingData.color = sender.color
        opacityView.displayedColor = drawingData.color

        let yourColor = sender.color
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        yourColor.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        DrawingToolsViewController.brushSelectedColor = UIColor(red: r, green: g, blue: b, alpha: 1.0)
        self.drawingData.color = UIColor(red: r, green: g, blue: b, alpha: 1.0)
        delegate?.didChangeDrawing(data:drawingData)
    }
    @objc func opacityChanged(_ sender: AlphaPickerView) {
        self.drawingData.opacity = Float(sender.pickedAlpha)
        DrawingToolsViewController.brushSelectedOpacity = Double(sender.pickedAlpha)
        delegate?.didChangeDrawing(data:drawingData)
    }
    
    
    func setBrushselectedview(selindex :Int) {
        for index in 0...4 {
            brushselectorList![index].backgroundColor = UIColor.white
        }
        brushselectorList![selindex].backgroundColor = UIColor.black
    }
    
    func resettoolselectedview() {
        for index in 0...4 {
            toolselectorList![index].backgroundColor = UIColor.clear
        }
    }
    
    open func currentBrush() -> DrawingData {
        return drawingData
    }

}

extension DrawingTool {
    
}
