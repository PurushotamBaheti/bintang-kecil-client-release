//
//  DailogViewController.swift
//  Croquipad
//
//  Created by Pawan Sharma on 19/09/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

protocol ValidationProtocol {
    func vlidate(title:String) -> Bool
}

class DialogViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtFolderName: UITextField!

    fileprivate let reuseIdentifier = "DialogCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 3, left: 4, bottom: 3, right: 4)

    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFolderNameHeight: NSLayoutConstraint!

    weak var folerDelegate : FoldersViewController?
    weak var sketchDelegate : SketchesViewController?

    var textureArray = NSMutableArray()
    var texturelargeArray = NSMutableArray()

    var textureName: String = ""
    var isFromView: String = ""
    
    var selectedIndex = -1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewWidth.constant = (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad ? 0.65 : 0.86) * screenWidth

        txtFolderName.attributedPlaceholder = NSAttributedString(string: txtFolderName.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : UIColor.white.withAlphaComponent(0.7)])
       
        self.view.layoutIfNeeded()
        
        txtFolderName.setBottomBorder(borderColor: UIColor.white)
        
        if isFromView == "FolderSection"
        {
            textureArray = ["stained-paper-1.jpg","stained-paper-2.jpg","stained-paper-3.jpg","stained-paper-4.jpg","stained-paper-5.jpg","stained-paper-6.jpg","stained-paper-7.jpg","stained-paper-8.jpg","stained-paper-9.jpg","stained-paper-10.jpg","ricepaper-1","ricepaper-2","ricepaper-3","ricepaper-4","ricepaper-5","ricepaper-6","ricepaper-7","ricepaper-8","ricepaper-9","ricepaper-10","ricepaper-11","ricepaper-12"]
            
            collectionView.reloadData()
        }
        else if isFromView == "SketchSection"
        {
            self.txtFolderNameHeight.constant = 0
            viewHeight.constant = viewHeight.constant - 30
            textureArray = ["croquis1.png", "croquis2.png", "croquis2.png"]
            texturelargeArray = ["croquis1_thumb.png", "croquis2_thumb.png", "croquis3_thumb.png"]

            collectionView.reloadData()
        }
        
    }

    
    // MARK: - IBAction
    @IBAction func btnClosePressed(_ sender: UIButton)
    {
        self.dismiss(animated: false) {
        
        }
    }
    
    @IBAction func btnSubmitPressed(_ sender: UIButton) {
        
        if isFromView == "FolderSection" {
          
            let folderName : String = self.txtFolderName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if folderName.characters.count == 0 {
                SwAlert.showNoActionAlert(title: "Croquipad", message: "Folder name is required.", buttonTitle: "ok")
                return
            }
            
            if self.folerDelegate?.vlidate(title: folderName) == true {
                SwAlert.showNoActionAlert(title: "Croquipad", message: "Folder is exists..", buttonTitle: "ok")
                return
            }
            
            if textureName.characters.count == 0 {
                SwAlert.showNoActionAlert(title: "Croquipad", message: "Please select texture.", buttonTitle: "ok")
                return
            }
            

            // send notification to create folder
            let userInfos : [String : String] = ["folderName":folderName,"textureName":self.textureName]
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"CreateFolder"),object: nil,userInfo: userInfos)
            
            self.dismiss(animated: false) {
               
            }
        }
        else
        {
            
            if textureName.characters.count == 0 {
                SwAlert.showNoActionAlert(title: "Croquipad", message: "Please select sketch.", buttonTitle: "ok")
                return
            }
            
            // send notification to create folder
            let userInfos : [String : String] = ["sketchName":"sketchName","textureName":self.textureName]
            let nc = NotificationCenter.default
            nc.post(name:Notification.Name(rawValue:"CreateSketch"),object: nil,userInfo: userInfos)
            
            self.dismiss(animated: true) {
                
            }
        }
    }

    
    @IBAction func btnCancelPressed(_ sender: UIButton)
    {
        self.dismiss(animated: false) {
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DialogViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return textureArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        aCell.contentView.backgroundColor = UIColor.white
        
        let imageView  = aCell.viewWithTag(1001) as! UIImageView!
        imageView?.contentMode = .scaleAspectFit
        imageView?.image = UIImage(named:textureArray.object(at: indexPath.row) as! String)
        
        if(self.selectedIndex > -1) {
            
            if(self.selectedIndex == indexPath.row) {
                imageView?.layer.borderWidth = 1.0
                imageView?.layer.borderColor = UIColor.darkGray.cgColor
            }
            else {
                imageView?.layer.borderWidth = 1.0
                imageView?.layer.borderColor = UIColor.white.cgColor
            }
        }
        else {
           
            imageView?.layer.borderWidth = 1.0
            imageView?.layer.borderColor = UIColor.white.cgColor
        }
        
        return aCell
        
    }
}

extension DialogViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
//        let availableWidth = view.frame.width - paddingSpace
//        let widthPerItem = availableWidth / itemsPerRow
//        
//        return CGSize(width: widthPerItem, height: widthPerItem + ((widthPerItem*30)/100))
        if isFromView == "FolderSection"
        {
            return CGSize(width:82,
                          height:82)
        }
        else
        {
            return CGSize(width:110,
                          height:170)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

extension DialogViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        textureName = self.textureArray.object(at: indexPath.row) as! String
         if isFromView == "SketchSection"
         {
            textureName = self.texturelargeArray.object(at: indexPath.row) as! String
         }
        
        selectedIndex = indexPath.row
        self.collectionView.reloadData()
        
    }
}
