//
//  LayersView.swift
//  Croquipad
//
//  Created by Pankaj Dhiman on 12/14/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

class LayersView: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var layers = [LayerData]()
    fileprivate var selectAction: ((Int) -> Void)?
    fileprivate var hideAction: ((Int) -> Void)?
    fileprivate var deleteAction: ((Int) -> Void)?
    fileprivate var isEditable = false

    internal var isViewHidden = true
    var width : CGFloat = 160.0

    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    convenience init(view:UIView, selectedAtIndex: @escaping (_ selectedAtIndex:Int)->(), hiddenAtIndex: @escaping (_ hiddenAtIndex:Int)->(), deleteAtIndex: @escaping (_ deleteAtIndex:Int)->() ) {
        
        let frame = CGRect(x: ScreenWidth, y: 0 , width: 160.0, height: ScreenHeight)

        self.init(frame:frame)
        
        selectAction = selectedAtIndex
        hideAction = hiddenAtIndex
        deleteAction = deleteAtIndex
        
        self.backgroundColor = UIColor.clear
        self.collectionView.backgroundColor = UIColor.clear
        view.addSubview(self)
        
        self.translatesAutoresizingMaskIntoConstraints = true
        self.autoresizingMask = [.flexibleHeight,.flexibleLeftMargin]
        
        let cellSize = CGSize(width:width - 20 , height:width + 10.0)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 10, bottom: 1, right: 10)
        layout.minimumLineSpacing = 1.0
        layout.minimumInteritemSpacing = 1.0
        collectionView.setCollectionViewLayout(layout, animated: true)

        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressGestureHandler))
        lpgr.minimumPressDuration = 1.5
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(lpgr)
        
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    private func nibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        view.backgroundColor = UIColor.clear
        addSubview(view)
        collectionView.register(UINib(nibName: "LayerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LayerCollectionViewCell")

    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func layersViewHidden(_ hidden:Bool) {
        var frame = self.frame
        if hidden == true {
            if isViewHidden == false {
                //   self.transform = CGAffineTransform(translationX: -320, y: 0)
                frame.origin.x = frame.minX + width
            }
        } else {
            
            if isViewHidden == true {
                //  self.transform = CGAffineTransform(translationX: 0, y: 0)
                frame.origin.x = frame.minX - width
                self.collectionView.reloadData()
            }
        }
        self.frame = frame
        isViewHidden = hidden
    }

    @objc func deleteLayer(sender : UIButton) {
        
        if let action = deleteAction {
            action(sender.tag)
            layers.remove(at: sender.tag)
            collectionView.deleteItems(at: [IndexPath(item: sender.tag, section: 0)])
        }
    }
    
    @objc func showHideLayer(sender : UIButton) {
        
        if isEditable == true {
            return
        }
        
        if let action = hideAction {
            action(sender.tag)
            layers[sender.tag].isHidden = !layers[sender.tag].isHidden
            collectionView.reloadItems(at: [IndexPath(item: sender.tag, section: 0)])
        }
    }
    
    func reload(_ data:[LayerData]) {
        
        isEditable = false
        
        layers.removeAll()
        layers.append(contentsOf: data)
        collectionView.reloadData()
    }

}

extension LayersView : UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return layers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LayerCollectionViewCell", for: indexPath) as! LayerCollectionViewCell
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(deleteLayer(sender:)), for: .touchUpInside)
        cell.showHideBtn.tag = indexPath.row
        cell.showHideBtn.addTarget(self, action: #selector(showHideLayer(sender:)), for: .touchUpInside)
        
        let layer = layers[indexPath.row]
        cell.showHideBtn.alpha = layer.isHidden ? 0.6 : 1.0
        cell.alpha = layer.isHidden ? 0.6 : 1.0
        cell.setEditable(isEditable)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let layer = layers[indexPath.row]
        if layer.isHidden {
            return
        }
        
        if let action = selectAction {
            action(indexPath.row)
        }
    }
}

extension LayersView : UIGestureRecognizerDelegate {
    
    @objc  func longPressGestureHandler(gestureRecognizer : UILongPressGestureRecognizer){
        if (gestureRecognizer.state == UIGestureRecognizer.State.began){

            let p = gestureRecognizer.location(in: self.collectionView)
            if let _  = (self.collectionView?.indexPathForItem(at: p)) as NSIndexPath?{
                isEditable = !isEditable
                self.collectionView.reloadData()
            }

        }
        else if (gestureRecognizer.state == UIGestureRecognizer.State.changed){}
        else if (gestureRecognizer.state != UIGestureRecognizer.State.ended){}
    }
}
