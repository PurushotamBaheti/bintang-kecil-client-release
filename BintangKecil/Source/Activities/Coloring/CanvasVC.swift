//
//  CanvasVC.swift
//  BintangKecil
//
//  Created by Purushotam on 31/08/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import DrawKit
import MobileCoreServices
import AVFoundation
import Photos

class CanvasVC: BaseViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    var pageTitle = ""
    var musicFile = ""
    var colorImg = ""
    var musicFilePath = ""
    var colorImgPath = ""
    
    @IBOutlet weak var containerView: UIView! // drawing tools container
    @IBOutlet weak var widthContraints: NSLayoutConstraint! // drawing tools container width constraints
    @IBOutlet weak var heightContraints: NSLayoutConstraint! // drawing tools container height constraints
    var isLayersViewHidden = true
    var layersView : LayersView?
    var selectedSketch: SketchData?
    var canvasView: Canvas!
    var toolBar: ToolBar?
    var canvasContainerView: CanvasContainerView!
    var scrollView: UIScrollView!
    var currentDrawing = UIView()
    var alllayers : [LayerData]!
    var currentLayer = 0
    var drawingData = DrawingData()
    static var selectedSketchData: SketchData?
    var sketchImage : UIImage?
    
    var activeEvent = ""
    var audioPlayer: AVAudioPlayer?
    
    @IBOutlet weak var toolBarHideBtn: UIButton!
    var isShowingHidden : Bool = false
    
    var isSomeActivityDone : Bool = false
    
    var zoomScale : Float = 1.0
    @IBOutlet  var zoomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let loadmessage  = "Please wait!. Preparing Canvas for you.\nTunggu sebentar!. Canvasmu sedang disiapkan ya Sobat."
        self.showLoaderwithMessage(message: loadmessage)
        self.setupView()
        self.InitialiseCanvas()
        self.titleLbl.text = self.pageTitle
        
    }
    
    @IBAction func SaveBtnClicked(){
        self.activeEvent = "save"
        self.canvasView.save()
    }
    
    @IBAction func ShareBtnClicked(){
        self.activeEvent = "share"
        self.canvasView.save()
    }
    
    @IBAction func ToolBarBtnClicked(){
        if(isShowingHidden == false){
            isShowingHidden = true
            toolBarHideBtn.setImage(UIImage.init(named: "password_hide_1"), for: .normal)
            containerView.isHidden = true
        }
        else{
            isShowingHidden = false
            toolBarHideBtn.setImage(UIImage.init(named: "password_visible_1"), for: .normal)
            containerView.isHidden = false
        }
    }
    
    func setupView(){
        widthContraints.constant = 70
        heightContraints.constant = 280
    }
    
    func InitialiseCanvas(){
        loadFileAsync(url: URL.init(string: self.colorImg)!) { (path, error) in
            self.colorImgPath = path!
            if(self.musicFile.isEmpty){
                DispatchQueue.main.async {
                    self.hideLoader()
                    self.setupCanvasView()
                }
            }
            else{
                loadFileAsync(url: URL.init(string: self.musicFile)!) { (path, error) in
                    self.musicFilePath = path!
                    DispatchQueue.main.async {
                        self.hideLoader()
                        self.setupCanvasView()
                        self.playBackgroundMusic()
                    }
                }
            }
        }
    }
    
    func playBackgroundMusic() {
        do {
            let alertSound = NSURL(fileURLWithPath: self.musicFilePath)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            try audioPlayer = AVAudioPlayer(contentsOf: alertSound as URL)
            audioPlayer!.numberOfLoops = -1
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
            
            
        } catch {
            print(error)
        }
    }
    
    func setupCanvasView(){
        CanvasVC.selectedSketchData = self.selectedSketch
        if let image = UIImage.init(contentsOfFile: self.colorImgPath) {
            self.sketchImage = image
        }
        
        let documentsUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let sketchPath = documentsUrl.appendingPathComponent((selectedSketch?.identifier)!)
        
        // load layers
        let filePath = sketchPath.appendingPathComponent("\((selectedSketch?.identifier)!).layers")
        if let layers = self.loadLayerData(withFile: filePath.absoluteString.removingPercentEncoding!){
            alllayers = setupLayersDatasource(layersViews: layers)
        }
        
        var ycoordinate : CGFloat = 64
        if(ScreenHeight > 811){
            ycoordinate = 84
        }
        
        let scrollView1 = UIScrollView(frame: CGRect.init(x: 0, y: ycoordinate, width:ScreenWidth , height: ScreenHeight - ycoordinate))
        scrollView1.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(scrollView1)
        self.scrollView = scrollView1
        
        let canvasFrame = CGRect(origin: .zero, size: CGSize(width: scrollView.frame.size.width, height: scrollView.frame.size.height))
        if let sketchImage = sketchImage {
            
            var loadedLayers : [UIView]? = nil
            if alllayers != nil {
                loadedLayers = alllayers.map{ $0.view }
            }
            
            let canvas = Canvas(canvasId: "", backgroundImage: UIImage.init(named:"btn_bg")!, sketchImage: sketchImage, layers: loadedLayers)
            canvas.delegate = self
            canvas.translatesAutoresizingMaskIntoConstraints = true
            canvas.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            canvas.frame = canvasFrame
            self.canvasView = canvas
        }
        
        // canvas container
        let canvasContainerView = CanvasContainerView(canvasSize: canvasView.frame.size)
        canvasContainerView.documentView = canvasView
        canvasContainerView.translatesAutoresizingMaskIntoConstraints = true
        canvasContainerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.canvasContainerView = canvasContainerView
        scrollView.addSubview(canvasContainerView)
        
        scrollView.contentSize = canvasView.frame.size
        scrollView.contentOffset = CGPoint(x: (canvasView.frame.width - scrollView.bounds.width) / 2.0,
                                           y: (canvasView.frame.height - scrollView.bounds.height) / 2.0)
        scrollView.maximumZoomScale = 3.0
        scrollView.minimumZoomScale = 1.0
        scrollView.panGestureRecognizer.allowedTouchTypes = [UITouch.TouchType.direct.rawValue as NSNumber]
        scrollView.pinchGestureRecognizer?.allowedTouchTypes = [UITouch.TouchType.direct.rawValue as NSNumber]
        scrollView.delegate = self
        scrollView.delaysContentTouches = false
        scrollView.isScrollEnabled = false
        
        currentDrawing = canvasView.returnCurrent()
        alllayers = setupLayersDatasource(layersViews: canvasView.returnData())
        
        self.view.bringSubviewToFront(containerView)
        
        // add layers view
        layersView = LayersView.init(view: self.view, selectedAtIndex: { index in
            // PDH : Bring layer in front
            self.currentDrawing = self.canvasView.setCurrentLayerAtIndex(index)
            
        }, hiddenAtIndex: { index in
            // PDH : hide Layer
            self.currentDrawing = self.canvasView.layerHiddenAtIndex(index)
            
            
        }, deleteAtIndex: { index in
            // PDH : delete layer
            self.currentDrawing = self.canvasView.deleteLayerAtIndex(index)
        })
        
        self.view.bringSubviewToFront(zoomView)
        
    }
    
    @IBAction func ZoomInClicked(){
        if(self.zoomScale < 2.0){
            self.zoomScale = self.zoomScale + 0.20
            self.scrollView.setZoomScale(CGFloat(self.zoomScale), animated: true)
        }
    }
    
    @IBAction func ZoomOutClicked(){
        if(self.zoomScale > 1.0){
            self.zoomScale = self.zoomScale - 0.20
            self.scrollView.setZoomScale(CGFloat(self.zoomScale), animated: true)
        }
    }
    
    var containerViewController: DrawingToolsViewController?
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "drawingToosSegue" {
            let connectContainerViewController = segue.destination as! DrawingToolsViewController
            containerViewController = connectContainerViewController
            containerViewController?.delegate = self
        }
    }
    
    fileprivate func saveLayerData(items: [UIView], toFile:String) {
        //4 - NSKeyedArchiver is going to look in every shopping list class and look for encode function and is going to encode our data and save it to our file path.  This does everything for encoding and decoding.
        //5 - archive root object saves our array of shopping items (our data) to our filepath url
        NSKeyedArchiver.archiveRootObject(items, toFile: toFile)
    }
    
    fileprivate func loadLayerData(withFile:String) -> [UIView]? {
        //6 - if we can get back our data from our archives (load our data), get our data along our file path and cast it as an array of ShoppingItems
        if let items = NSKeyedUnarchiver.unarchiveObject(withFile: withFile) as? [UIView] {
            return items
        }
        return nil
    }
    
    fileprivate func setupLayersDatasource(layersViews:[UIView]) -> [LayerData] {
        
        var arr = [LayerData]()
        for i in 0..<layersViews.count {
            let view = layersViews[i]
            arr.append(LayerData(name: "", view: view, isHidden:view.isHidden))
        }
        return arr
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.containerView.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        if(!self.musicFile.isEmpty){
            audioPlayer!.stop()
        }
    }
    
    override func backAction(sender: AnyObject) {
        if(self.isSomeActivityDone == true){
            let dialogMessage = UIAlertController(title: "Confirm", message: "You have some unsaved activity in this page.Once you exit from this page, all your activities on this page will be lost. Do you want to save your work ?", preferredStyle: .alert)
            
            // Create OK button with action handler
            let ok = UIAlertAction(title: "Save", style: .default, handler: { (action) -> Void in
                self.activeEvent = "save"
                self.canvasView.save()
                
            })
            
            // Create Cancel button with action handlder
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                self.navigationController?.popViewController(animated: true)
            }
            
            //Add OK and Cancel button to dialog message
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)
            
            // Present dialog message to user
            self.present(dialogMessage, animated: true, completion: nil)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: - CanvasDelegate
extension CanvasVC: CanvasDelegate {
    
    func brush() -> Brush? {
        return nil
    }
    
    func canvas(_ canvas: Canvas, willBegunDrawing drawing: Drawing) {
        
    }
    
    func canvas(_ canvas: Canvas, WillEndDrawing drawing: Drawing) {
        // PDH : save data after that
        currentDrawing = canvasView.returnCurrent()
        self.isSomeActivityDone = true
    }
    
    func canvas(_ canvas: Canvas, didUpdateDrawing drawing: Drawing, mergedImage image: UIImage?) {
        
    }
    
    func canvas(_ canvas: Canvas, didSaveDrawing drawing: Drawing, mergedImage image: UIImage?) {
        
        if(self.activeEvent == "save"){
            self.saveDrawing(canvas: canvas, image: image!)
        }
        else{
            self.shareDrawing(canvas: canvas, image: image!)
        }
        
    }
    
    private func shareDrawing(canvas : Canvas, image: UIImage?) {
        
        // set up activity view controller
        let activityViewController = UIActivityViewController(activityItems: [image!], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.addToReadingList, .assignToContact,.airDrop,.copyToPasteboard,.message,.openInIBooks,.markupAsPDF,.mail]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func redoSelector(_ sender: UIButton) {
        canvasView.redo()
    }
    
    @IBAction func undoSelector(_ sender: UIButton) {
        canvasView.undo()
    }
    
    @IBAction func clearSelector(_ sender: UIButton) {
        
        let dialogMessage = UIAlertController(title: "Confirm", message: "This option will clear all your work. Do you want to proceed? ", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.canvasView.clear()
            self.isSomeActivityDone = false
            
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    
    private func saveDrawing(canvas : Canvas, image: UIImage?) {
        //  UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        self.isSomeActivityDone = false
        self.CallSaveImage(img: image!)
    }
    
    func CallSaveImage(img:UIImage){
        var parameter = [String:Any]()
        let isChild = UserDefaults.standard.getChild()
        if isChild{
            
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            parameter["child_id"] = decodedData.id
            
        }
        var TotalArray = [[String : Any]]()
        let dict = ["img":img as Any,"key":"file"]
        TotalArray.append(dict)
        showLoader()
        
        uploadColorData(strUrl: "user-coloring-image/upload", arrImage: TotalArray, param: parameter, success: { (json) in
            if json["status"].stringValue == "200"{
                self.showToast(message: "Your activity (drawing) saved successfully as template in My Saved Work.")
                
            }
            self.hideLoader()
            
        }, failure: {
            (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg as! String)
        })
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            
            let status = PHPhotoLibrary.authorizationStatus()
            if(status == .notDetermined || status == .denied || status == .restricted){
                let ac = UIAlertController(title: "Permission error", message: "Please grant permission to access Photo library from App settings of device.", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                present(ac, animated: true)
            }
            else{
                let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                present(ac, animated: true)
            }
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your work is saved in Photos Gallery.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
}


// MARK: - PaletteDelegate
extension CanvasVC: PaletteDelegate {
    
    func colorWithTag(_ tag: NSInteger) -> UIColor? {
        if tag == 4 {
            // if you return clearColor, it will be eraser
            return UIColor.clear
        }
        if tag == 3 {
            let brush = Brush()
            brush.isPaintBrush = true
        }
        else
        {
            let brush = Brush()
            brush.isPaintBrush = false
        }
        
        return nil
    }
    
}

extension CanvasVC: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension CanvasVC: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.canvasContainerView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        var desiredScale = self.traitCollection.displayScale
        let existingScale = self.canvasView.contentScaleFactor
        
        if scale >= 2.0 {
            desiredScale *= 2.0
        }
        if abs(desiredScale - existingScale) > 0.00001 {
            self.canvasView.contentScaleFactor = desiredScale
            self.canvasView.setNeedsDisplay()
        }
    }
}


extension CanvasVC : DrawingToolsViewControllerDelegate {
    
    func drawingToosInExpanded(mode:Bool) {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.widthContraints.constant = mode == true ? 200.0 : 100.0
        } else if UIDevice.current.userInterfaceIdiom == .phone {
            self.widthContraints.constant = mode == true ? 120.0 : 70.0
            //phone
        }
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func didChangeDrawing(data:DrawingData) {
        
        if let canvas = self.canvasView {
            if(data.brushType == BrushType.painbrush){
                canvas.updateDrawing(brushSize: data.Size, color: data.color, opacity: 0.3, isEraser: data.brushType == BrushType.eraser ? true : false, isPaint: true, brushType: DrawingToolsViewController.brushselectedIndex)
            }
            else{
                canvas.updateDrawing(brushSize: data.Size, color: data.color, opacity: data.opacity, isEraser: data.brushType == BrushType.eraser ? true : false, isPaint: false,  brushType: DrawingToolsViewController.brushselectedIndex)
            }
        }
    }
}

extension UIView {
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}

extension UIView {
    func takeScreenshot() -> UIImage {
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        // Draw view in that context
        let _ = drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
