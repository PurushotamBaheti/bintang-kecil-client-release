//
//  AddForumVC.swift
//  BintangKecil
//
//  Created by Devesh on 11/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

protocol AddForumDelegate{
    func forumAdddedSuccessfully()
}

class AddForumVC: BaseViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtdropdownTitle: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var addImgLbl: UILabel!
    @IBOutlet weak var activeTextField   : UITextField!

    fileprivate let pickerView1 = ToolbarPickerView()
    fileprivate var titles1 = [""]
    fileprivate var titles2 = [""]

    var textfieldID = ""
    var ForumImage : UIImage? = nil
    var dataArray = [ArticleListModal]()
    var imagePicker = UIImagePickerController()
    var delegate : AddForumDelegate?
    var isFromArticlePage : Bool = false
    var ArticleId = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.getArticleData()

        txtViewDesc.delegate = self
        txtViewDesc.textColor = UIColor.black
        txtViewDesc.placeholder = "Forum Description"
        txtTitle.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        txtdropdownTitle.placeholder = "Select                                              "
        txtdropdownTitle.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))

        imagePicker.delegate = self
        
        self.txtdropdownTitle.inputView = self.pickerView1
        self.txtdropdownTitle.inputAccessoryView = self.pickerView1.toolbar

        self.pickerView1.dataSource = self
        self.pickerView1.delegate = self
        self.pickerView1.toolbarDelegate = self
        self.pickerView1.reloadAllComponents()
    }

    func ValidateTextFields() -> Bool{
        if((txtTitle.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_FORUM_TITLE)
            return false
        }
        else if((txtViewDesc.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_FORUM_DESCRIPTION)
            return false
        }
        else if(txtViewDesc.text == "Forum Description"){
            self.showToast(message: MessageString.EMPTY_FORUM_DESCRIPTION)
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtViewDesc.textColor == UIColor.lightGray {
            txtViewDesc.text = ""
            txtViewDesc.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewDesc.text == "" {
            txtViewDesc.text = "Forum Description"
            txtViewDesc.textColor = UIColor.lightGray
        }
    }


    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    @IBAction func doneBtnClicked(sender: AnyObject){
        self.view.endEditing(true)
        if(self.ValidateTextFields() == true){
            self.CallAddForumApi()
        }
    }

    @IBAction func ArticleBtnClicked(sender: AnyObject){}

    func CallAddForumApi(){
        var parameter = [String:Any]()
        parameter["title"] = txtTitle.text
        parameter["content"] = txtViewDesc.text
        parameter["article_id"] = textfieldID

        var TotalArray = [[String : Any]]()
        if(self.ForumImage != nil){
            let dict = ["img":self.ForumImage as Any,"key":"image"]
            TotalArray.append(dict)
        }
        showLoader()
        uploadData(strUrl: "forums", arrImage: TotalArray, param: parameter, success: { (json) in
            if json["status"].stringValue == "200"{
                self.showToast(message: "Forum created successfully!")
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0){
                    // do stuff 03 seconds later
                    if(self.isFromArticlePage == true){
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForumVC") as! ForumVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else{
                        self.delegate?.forumAdddedSuccessfully()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            else{
                self.ShowAlert(message: "Discussion on this Article is already open.Please select some other article.")
            }
            self.hideLoader()

        }, failure: {
            (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg as! String)
        })
    }

    @IBAction func ImageBtnClicked(sender: AnyObject){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }

        self.present(alert, animated: true, completion: nil)
    }

    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    //MARK:- ***************  UIImagePickerController delegate Methods ****************
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){

        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        // Set photoImageView to display the selected image.
        imgView.image = selectedImage
        ForumImage = selectedImage
        addImgLbl.isHidden = true

        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    func getArticleData(){
        var parameter = [String:Any]()
        parameter["search"] = ""
        parameter["pageNumber"] = ""

        showLoader()
        postRequest(strUrl: "articles/list", param: parameter, success: { (json) in
            if json["success"].stringValue == "true"{
                if json["message"].stringValue == "No Records Found"{
                   // self.ShowAlert(message: "No Record Found.")
                }
                else{
                    let arrData = json["data"].dictionaryValue["records"]!.arrayValue
                    if(arrData.count > 0){
                        for ind in 0...arrData.count - 1{
                            let wrapper = ArticleListModal.init(fromJson: arrData[ind])
                            self.dataArray.append(wrapper)
                            self.titles1.append(wrapper.title)
                            self.titles2.append(String(wrapper.id))
                            if(self.isFromArticlePage == true && wrapper.id == self.ArticleId){
                                self.txtdropdownTitle.text = wrapper.title
                                self.textfieldID = "\(wrapper.id!)"
                            }
                        }
                        self.pickerView1.reloadAllComponents()
                    }
                }
            }
            self.hideLoader()
        })
        {
            (msg) in
            self.ShowAlert(message: msg)
        }
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        self.activeTextField = textField
    }
}

extension AddForumVC: ToolbarPickerViewDelegate {

    func didTapDone() {
        if self.activeTextField == self.txtdropdownTitle{
            let row = self.pickerView1.selectedRow(inComponent: 0)
            self.pickerView1.selectRow(row, inComponent: 0, animated: false)
            if self.titles1.count > 0{
                self.txtdropdownTitle.text = self.titles1[row]
                self.textfieldID = self.titles2[row]
            }
            self.txtdropdownTitle.resignFirstResponder()
        }
    }

    func didTapCancel() {
        self.txtdropdownTitle.resignFirstResponder()
    }
}

extension AddForumVC: UIPickerViewDataSource, UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.titles1.count
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return self.titles1[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {}
}
