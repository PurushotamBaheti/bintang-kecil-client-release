//
//  ForumVC.swift
//  BintangKecil
//
//  Created by Devesh on 09/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class ForumVC: BaseViewController, UITableViewDelegate, UITableViewDataSource,AddForumDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var dataArray = [ForumModel]()
    var totalRecords : Int = 0
    
    var page = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getForumData(page_no: 1, search: "")
    }

    @IBAction func addForumAction(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddForumVC") as! AddForumVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }

    func forumAdddedSuccessfully() {
        self.dataArray.removeAll()
        self.tableView.reloadData()
        self.getForumData(page_no: 1, search: "")
    }

    
    override func viewWillAppear(_ animated: Bool) {

        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {

        self.tabBarController?.tabBar.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{

        let cell = tableView.dequeueReusableCell(withIdentifier: "ForumCell", for: indexPath as IndexPath) as! ForumCell
        let wrapper = self.dataArray[indexPath.row]
        if(wrapper.file_thumbnail_url!.isEmpty){
            cell.imgArticleView.image = UIImage.init(named: "noImage")
        }
        else{
            cell.imgArticleView.sd_setImage(with: URL(string: wrapper.file_thumbnail_url!), placeholderImage: UIImage(named: "noImage"))
        }
        cell.lblArticleTile.text = wrapper.title
        cell.lblArticleDate.text = self.convertDateFormatter(date:  wrapper.created_at)
        cell.lblArticleDesc.text = wrapper.content.htmlToString
        cell.lblUserName.text = wrapper.user_name
        cell.selectionStyle = .none
        cell.dropShadow(UIColor.lightGray)
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
        let wrapper = self.dataArray[indexPath.row]
        vc.forumTagId = "\(wrapper.id ?? 0)"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if self.dataArray.count-1 == indexPath.row{
            if(self.dataArray.count < self.totalRecords){
                self.page = self.page + 1
                self.getForumData(page_no: self.page, search: "")
            }
        }
    }
    
    func getForumData(page_no: Int, search: String){
        var parameter = [String:Any]()
        parameter["pageNumber"] = page_no
        parameter["search"] = search

        showLoader()
        postRequest(strUrl: "forums/list", param: parameter, success: { (json) in
            if json["success"].stringValue == "true"{
                if json["message"].stringValue == "No Records Found"{
                    self.CreateNoFiltersView()
                }
                else{
                    let arrData = json["data"].dictionaryValue["records"]!.arrayValue
                    self.totalRecords =  json["data"]["totalRecords"].intValue
                    if(arrData.count > 0){
                        for ind in 0...arrData.count - 1{
                            let wrapper = ForumModel.init(fromJson: arrData[ind])
                            self.dataArray.append(wrapper)
                        }
                        self.tableView.reloadData()
                    }
                }
            }
            
            self.hideLoader()
        })
        {
            (msg) in

            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func CreateNoFiltersView() {
        let popupview = UIView()
        popupview.backgroundColor = UIColor.clear
        self.tableView.backgroundView = popupview

        let ycor = (ScreenHeight - self.tableView.frame.origin.y)/2

        let subview = UIView()
        subview.frame = CGRect.init(x:30, y: ycor - 160, width: popupview.frame.size.width - 60, height: 160)
        subview.backgroundColor = UIColor.clear
        popupview.addSubview(subview)

        let logo = UIImageView.init()
        logo.frame = CGRect.init(x:(subview.frame.size.width - 80)/2, y:20, width: 80, height: 80)
        logo.image = UIImage.init(named:"logo")
        logo.contentMode = .scaleAspectFit
        subview.addSubview(logo)

        let messageLbl = UILabel()
        messageLbl.frame = CGRect.init(x: 5, y: 90, width: subview.frame.size.width - 10, height: 50)
        messageLbl.backgroundColor = UIColor.clear
        messageLbl.text = "No records found."
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.init(name: "Metropolis-ExtraLight", size: 13.0)
        messageLbl.textColor = UIColor.black
        messageLbl.numberOfLines = 3
        subview.addSubview(messageLbl)
    }


    func convertDateFormatter(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: NSLocale.current.identifier)
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"//this your string date format
        let convertedDate = dateFormatter.date(from: date)

        guard dateFormatter.date(from: date) != nil else {
            // assert(false, "no date from string")
            return ""
        }
        dateFormatter.dateFormat = "dd MMM yyyy"///this is what you want to convert format
        let timeStamp = dateFormatter.string(from: convertedDate!)

        return timeStamp
    }
}

class ForumCell: UITableViewCell {
    @IBOutlet weak var imgArticleView: UIImageView!
    @IBOutlet weak var lblArticleTile: UILabel!
    @IBOutlet weak var lblArticleDesc: UILabel!
    @IBOutlet weak var lblArticleDate: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
}
