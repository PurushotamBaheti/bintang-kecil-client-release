//
//  ForumDetailVC.swift
//  BintangKecil
//
//  Created by Devesh on 13/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class ForumDetailVC: BaseViewController, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtCommentField: UITextField!
    @IBOutlet weak var pageTitleLbl: UILabel!
    
    var Forum_name = ""
    var Forum_image = ""
    var Forum_Desc = ""
    var Forum_Total = 0
    var dataArray = [Forum_Comment]()
    var dataModel = ForumModel.init()
    var page = 1
    var pageTitle = ""
    var isForumOwner : Bool = false
    var totalRecords : Int = 0
    var forumTagId = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.getForumData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0{
            if Forum_image == ""{
                return 2
            }
            else{
                return 3
            }
        }
        else{
            if(self.dataArray.count == 0){
                return 1
            }
            return self.dataArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if Forum_image == ""{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FourmDescription", for: indexPath as IndexPath) as! FourmDescription
                    cell.lblFourmTitle.text = Forum_name
                    cell.lblFourm.text = Forum_Desc
                    return cell
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FourmLocal", for: indexPath as IndexPath) as! FourmLocal
                    cell.lblCommentCount.text = String(Forum_Total)
                    return cell
                }
            }
            else{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FourmPicture", for: indexPath as IndexPath) as! FourmPicture
                    cell.imgFourm.sd_setImage(with: URL(string: Forum_image), placeholderImage: UIImage(named: "child"))
                    return cell
                }
                else if indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FourmDescription", for: indexPath as IndexPath) as! FourmDescription
                    cell.lblFourmTitle.text = Forum_name
                    cell.lblFourm.text = Forum_Desc
                    return cell
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FourmLocal", for: indexPath as IndexPath) as! FourmLocal
                    cell.lblCommentCount.text = String(Forum_Total)
                    return cell
                }
            }
        }
        else{
            if(self.dataArray.count > 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "FourmComment", for: indexPath as IndexPath) as! FourmComment
                let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
                let wrapper = self.dataArray[indexPath.row]

                cell.lblUserName.text = wrapper.userName

                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: NSLocale.current.identifier)
                dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"//this your string date format
                let convertedDate = dateFormatter.date(from: wrapper.createdAt)
                dateFormatter.dateFormat = "dd MMM YYYY"///this is what you want to convert format
                let timeStamp = dateFormatter.string(from: convertedDate!)

                cell.lblDate.text = timeStamp

                cell.lblComment.text = wrapper.comment
                if(wrapper.userImage == ""){
                    cell.imgUser.image = UIImage.init(named: "child")
                }
                else{
                    cell.imgUser.sd_setImage(with: URL(string: wrapper.userImage), placeholderImage: UIImage(named: "child"))
                }
                if(isForumOwner == true){
                    cell.btnDelete.tag = wrapper.id
                    cell.btnDelete.addTarget(self,action:#selector(buttonClicked),
                                             for:.touchUpInside)
                    cell.btnDeleteConstraint.constant = 20
                    cell.btnDelete.isHidden = false
                    cell.decriptionBottomConstraint.constant = 28
                }
                else if(decodedData.id == wrapper.userID){
                    cell.btnDelete.tag = wrapper.id
                    cell.btnDelete.addTarget(self,action:#selector(buttonClicked),
                                             for:.touchUpInside)
                    cell.btnDeleteConstraint.constant = 20
                    cell.btnDelete.isHidden = false
                    cell.decriptionBottomConstraint.constant = 28
                }
                else{
                    cell.btnDeleteConstraint.constant = 0
                    cell.btnDelete.isHidden = true
                    cell.decriptionBottomConstraint.constant = 8
                }

                cell.selectionStyle = .none
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
                return cell

            }
        }
    }
    
    @objc func buttonClicked(sender:UIButton){
        var parameter = [String:Any]()
        parameter["comment_id"] = sender.tag

        showLoader()
        postRequest(strUrl: "forums/comments/delete", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                //mobilenumb = mobilenumb.replacingOccurrences(of:"-", with: "")
                self.page = 1
                self.dataArray.removeAll()
                self.tableView.reloadData()
                self.getCommentData(page_no:self.page, forum_id: self.forumTagId)
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    @IBAction func btnSendAction(sender: AnyObject){
        self.txtCommentField.resignFirstResponder()

        if txtCommentField.text?.isEmpty ?? true{
            print("textField is empty")
        }
        else{
            var parameter = [String:Any]()
            parameter["forum_id"] = forumTagId
            parameter["comment"] = txtCommentField.text

            showLoader()
            postRequest(strUrl: "forums/comment", param: parameter, success: { (json) in
                self.hideLoader()
                if json["status"].stringValue == "200"{
                    //mobilenumb = mobilenumb.replacingOccurrences(of:"-", with: "")
                    self.page = 1
                    self.dataArray.removeAll()
                    self.tableView.reloadData()
                    self.txtCommentField.text = ""
                    self.getCommentData(page_no:self.page, forum_id: self.forumTagId)

                }
                else{
                    let message = json["message"].stringValue
                    self.showToast(message: message)
                }

            }) { (msg) in
                self.hideLoader()
                self.ShowAlert(message: msg)
            }
        }
    }

    func getForumData(){
        var parameter = [String:Any]()
        parameter["forum_id"] = forumTagId

        showLoader()
        postRequest(strUrl: "forum/detail", param: parameter, success: { (json) in
            if json["status"].stringValue == "200"{
                self.dataModel = ForumModel.init(fromJson: json["data"])
                let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
                if(decodedData.id == self.dataModel.user_id){
                    self.isForumOwner = true
                }
                self.Forum_name = self.dataModel.title
                self.Forum_image = self.dataModel.file_url
                self.Forum_Desc = self.dataModel.filtered_content
                self.pageTitle = self.dataModel.title

                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
                self.getCommentData(page_no: self.page, forum_id: self.forumTagId)
            }
            else{
                self.Forum_Total = 0
                self.tableView.reloadData()
            }
            self.hideLoader()
        })
        {
            (msg) in
            self.ShowAlert(message: msg)
        }
    }

    func getCommentData(page_no: Int, forum_id: String){
        var parameter = [String:Any]()
        parameter["pageNumber"] = page_no
        parameter["forum_id"] = forum_id
        
        showLoader()
        postRequest(strUrl: "forums/comments/list", param: parameter, success: { (json) in
            if json["status"].stringValue == "200"{
                let arrData = json["data"].dictionaryValue["records"]!.arrayValue
                self.totalRecords = json["data"]["totalRecords"].intValue
                if(arrData.count > 0){
                    for ind in 0...arrData.count - 1{
                        let wrapper = Forum_Comment.init(
                            userName: arrData[ind]["user_name"].stringValue,
                            id: arrData[ind]["id"].intValue,
                            createdAt: arrData[ind]["created_at"].stringValue,
                            userID: arrData[ind]["user_id"].intValue,
                            userImage: arrData[ind]["user_image"].stringValue,
                            comment: arrData[ind]["comment"].stringValue,
                            userThumbnailImage: arrData[ind]["user_thumbnail_image"].stringValue)
                        
                        self.dataArray.append(wrapper)
                    }
                    self.Forum_Total = json["data"].dictionaryValue["totalRecords"]!.intValue
                    self.tableView.reloadData()
                }
            }
            else{
                self.Forum_Total = 0
                self.tableView.reloadData()
            }
            self.hideLoader()
        })
        {
            (msg) in
            self.ShowAlert(message: msg)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if self.dataArray.count-1 == indexPath.row {
            getMoreComments(page: page)
        }
    }
    
    func getMoreComments(page:Int){
        //hit api
        if(self.dataArray.count < self.totalRecords){
            self.page = self.page + 1
            self.dataArray.removeAll()
            self.getCommentData(page_no: self.page, forum_id: self.forumTagId)
        }
    }
}

class FourmPicture : UITableViewCell{
    @IBOutlet weak var imgFourm: UIImageView!
}

class FourmDescription : UITableViewCell{
    @IBOutlet weak var lblFourmTitle: UILabel!
    @IBOutlet weak var lblFourm: UILabel!
}

class FourmLocal : UITableViewCell{
    @IBOutlet weak var lblCommentCount: UILabel!
}

class FourmComment : UITableViewCell{
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var btnDeleteConstraint : NSLayoutConstraint!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var decriptionBottomConstraint : NSLayoutConstraint!
}

class Forum_Comment {
    let userName: String
    let id: Int
    let createdAt: String
    let userID: Int
    let userImage: String
    let comment: String
    let userThumbnailImage: String

    init(userName: String, id: Int, createdAt: String, userID: Int, userImage: String, comment: String, userThumbnailImage: String) {
        self.userName = userName
        self.id = id
        self.createdAt = createdAt
        self.userID = userID
        self.userImage = userImage
        self.comment = comment
        self.userThumbnailImage = userThumbnailImage
    }
}
