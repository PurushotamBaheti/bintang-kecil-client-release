//
//  ArticlesVC.swift
//  BintangKecil
//
//  Created by Devesh on 09/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SDWebImage

class ArticlesVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var page = 1
    var dataArray = [ArticleListModal]()
    var totalRecords : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.dataArray.removeAll()
        self.tableView.reloadData()
        self.getArticleData(page_no: 1, search: "")

    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath as IndexPath) as! ArticleCell
        let wrapper = self.dataArray[indexPath.row]

        if(wrapper.file_thumbnail_url!.isEmpty){
            cell.imgArticleView.image = UIImage.init(named: "child")
        }
        else{
            cell.imgArticleView.sd_setImage(with: URL(string: wrapper.file_thumbnail_url!), placeholderImage: UIImage(named: "child"))
        }

        cell.lblArticleTile.text = wrapper.title
        if(!wrapper.formatted_publish_date.isEmpty){
            cell.lblArticleDate.text = self.convertDateFormatter(date: wrapper.formatted_publish_date)
        }
        else{
            cell.lblArticleDate.text = "N/A"
        }
        if(!wrapper.filtered_content.isEmpty){
            cell.lblArticleDesc.text = wrapper.filtered_content.htmlToString
        }
        else{
            cell.lblArticleDesc.text = "N/A"
        }
        cell.selectionStyle = .none
        cell.dropShadow(UIColor.lightGray)
        return cell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let wrapper = self.dataArray[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "articledetail") as! ArticlesDetailVC
        vc.HtmlString = wrapper.content
        vc.titleStr = wrapper.title
        vc.articleId = wrapper.id
        vc.Imageurl = wrapper.file_url
        vc.isForumLinked = wrapper.isForumLinked
        if(wrapper.isForumLinked == true){
            vc.forumModel = wrapper.forum!
        }
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if self.dataArray.count-1 == indexPath.row{
            if(self.dataArray.count < self.totalRecords){
                self.page = self.page + 1
                self.getArticleData(page_no: self.page, search: "")
            }
        }

    }

    func getArticleData(page_no: Int, search: String){
        self.tableView.backgroundView = nil
        var parameter = [String:Any]()
        parameter["search"] = search
        parameter["pageNumber"] = page_no

        showLoader()
        postRequest(strUrl: "articles/list", param: parameter, success: { (json) in
            if json["success"].stringValue == "true"{
                if json["message"].stringValue == "No Records Found"{
                   // self.ShowAlert(message: "No Record Found.")
                    self.CreateNoFiltersView()
                }
                else{
                    let arrData = json["data"].dictionaryValue["records"]!.arrayValue
                    print(arrData)
                    self.totalRecords =  json["data"]["totalRecords"].intValue
                    if(arrData.count > 0){
                        for ind in 0...arrData.count - 1{
                            let wrapper = ArticleListModal.init(fromJson: arrData[ind])
                            self.dataArray.append(wrapper)
                        }
                        self.tableView.reloadData()
                    }
                }
            }
            
            self.hideLoader()
        })
        {
            (msg) in
            self.ShowAlert(message: msg)
        }
    }
    func CreateNoFiltersView() {
        let popupview = UIView()
        popupview.backgroundColor = UIColor.clear
        self.tableView.backgroundView = popupview

        let ycor = (ScreenHeight - self.tableView.frame.origin.y)/2

        let subview = UIView()
        subview.frame = CGRect.init(x:30, y: ycor - 160, width: popupview.frame.size.width - 60, height: 160)
        subview.backgroundColor = UIColor.clear
        popupview.addSubview(subview)

        let logo = UIImageView.init()
        logo.frame = CGRect.init(x:(subview.frame.size.width - 80)/2, y:20, width: 80, height: 80)
        logo.image = UIImage.init(named:"logo")
        logo.contentMode = .scaleAspectFit
        subview.addSubview(logo)

        let messageLbl = UILabel()
        messageLbl.frame = CGRect.init(x: 5, y: 90, width: subview.frame.size.width - 10, height: 50)
        messageLbl.backgroundColor = UIColor.clear
        messageLbl.text = "No records found."
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.init(name: "Metropolis-ExtraLight", size: 13.0)
        messageLbl.textColor = UIColor.black
        messageLbl.numberOfLines = 3
        subview.addSubview(messageLbl)
    }

    func convertDateFormatter(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"//this your string date format
        dateFormatter.locale = Locale(identifier: NSLocale.current.identifier)
        let convertedDate = dateFormatter.date(from: date)

        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }
        dateFormatter.dateFormat = "dd MMM yyyy"///this is what you want to convert format
        let timeStamp = dateFormatter.string(from: convertedDate!)

        return timeStamp
    }
}

class ArticleCell: UITableViewCell {
    
    @IBOutlet weak var imgArticleView: UIImageView!
    @IBOutlet weak var lblArticleTile: UILabel!
    @IBOutlet weak var lblArticleDesc: UILabel!
    @IBOutlet weak var lblArticleDate: UILabel!
    
}
