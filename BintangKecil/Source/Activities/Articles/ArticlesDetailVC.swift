//
//  ArticlesDetailVC.swift
//  BintangKecil
//
//  Created by Purushotam on 13/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import WebKit
import AVKit
import AVFoundation
import SDWebImage

class ArticlesDetailVC: UIViewController,WKNavigationDelegate, WKUIDelegate,UIDocumentInteractionControllerDelegate {
    
    @IBOutlet weak var navHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var articleImg: UIImageView!
    @IBOutlet weak var ArticleTitle: UILabel!
    @IBOutlet weak var ForumBtn: UIButton!
    @IBOutlet weak var ImgHeight: NSLayoutConstraint!
    @IBOutlet weak var ForumLbl: UILabel!
    
    var HtmlString = ""
    var titleStr = ""
    var articleId = 0
    var Imageurl = ""
    var forumModel = ArticleForumLinkedModal.init()
    var isForumLinked : Bool = false
    var webView : WKWebView!
    let documentInteractionController = UIDocumentInteractionController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseUI()
    }
    
    func customiseUI(){
        var ycoordinate :CGFloat = 64
        if(ScreenHeight > 811){
            navHeight.constant = 84
            ycoordinate = 84
        }
        
        if(!Imageurl.isEmpty){
            articleImg.isHidden = false
            articleImg.sd_setImage(with: URL(string: Imageurl), placeholderImage: UIImage(named: "child"))
            ycoordinate = ycoordinate + 140
            ImgHeight.constant = 140
        }
        else{
            articleImg.isHidden = true
            ImgHeight.constant = 0
        }
        
        self.ArticleTitle.text = self.titleStr
        if(self.isForumLinked == true){
            
            ForumBtn.setTitle("Discuss Now", for: .normal)
        }
        else{
            
            ForumBtn.setTitle("Open A Discussion", for: .normal)
        }
        ycoordinate = ycoordinate + self.ArticleTitle.frame.size.height + 20
        
        let font = "<font face='Metropolis-Light' size='6' color= 'black'>%@"
        let html = String(format: font, HtmlString)
        self.documentInteractionController.delegate = self
        
        webView = WKWebView(frame: CGRect(x: 0, y: ycoordinate, width: ScreenWidth, height: ScreenHeight - ycoordinate - 70))
        self.view.addSubview(webView)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.loadHTMLString(html, baseURL: nil)
        self.view.bringSubviewToFront(webView)
    }
    
    @IBAction func forumBtnClicked(){
        if(self.isForumLinked == true){
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
            vc.forumTagId = "\(self.forumModel.id ?? 0)"
            vc.Forum_name = self.forumModel.title
            vc.Forum_image = self.forumModel.file_url!
            vc.Forum_Desc = self.forumModel.content
            vc.pageTitle = self.forumModel.title
            if(decodedData.id == self.forumModel.user_id){
                vc.isForumOwner = true
            }
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddForumVC") as! AddForumVC
            vc.isFromArticlePage = true
            vc.ArticleId = self.articleId
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func backAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SaveBtnClicked(){
        self.callSaveApi()
    }
    
    func callSaveApi(){
        var parameter = [String:Any]()
        parameter["article_id"] = articleId
        
        showLoader()
        postRequest(strUrl: "articles/download", param: parameter, success: { (json) in
            if json["success"].stringValue == "true"{
                let path =  json["data"]["path"].stringValue
                self.downloadFile(fileurl: path)
            }
            else{
                self.hideLoader()
            }
            
        })
        {
            (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
    
    func downloadFile(fileurl : String){
        let fileArr = fileurl.components(separatedBy: "/")
        let filename = fileArr[fileArr.count - 1]
        guard let url = URL(string: fileurl) else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data, error == nil else { return }
            let tmpURL = FileManager.default.temporaryDirectory
                
                .appendingPathComponent(filename)
            
            do {
                try data.write(to: tmpURL)
                
            } catch {
                
                print(error)
                
            }
            
            DispatchQueue.main.async {
                /// STOP YOUR ACTIVITY INDICATOR HERE
                self.hideLoader()
                // self.showToast(message: "Zip File successfully downloaded")
                self.share(url: tmpURL)
            }
            
        }.resume()
    }
    
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.presentPreview(animated: true)
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        
        return self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
}
