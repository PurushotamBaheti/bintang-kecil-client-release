//
//  FunnyEmojiVC.swift
//  quickstart-ios-swift
//
//  Created by Lara Vertlberg on 09/12/2019.
//  Copyright © 2019 Lara Vertlberg. All rights reserved.
//

import UIKit
import DeepAR
import AVKit
import AVFoundation
import AssetsLibrary
import Photos

enum Mode: String {
    case masks
    case effects
    case filters
}

enum RecordingMode : String {
    case photo
    case video
    case lowQualityVideo
}
enum Masks: String, CaseIterable {
    case none
    case alien
    case aviators
    case beard
    case bigmouth
    case dalmatian
    case fatify
    case flowers
    case grumpycat
    case koala
    case lion
    case mudMask
    case obama
    case pug
    case slash
    case smallface
    case teddycigar
    case topology
    case tripleface
}
enum Effects: String, CaseIterable {
    case none
    case fire
    case heart
    case blizzard
    case rain
}
enum Filters: String, CaseIterable {
    case none
    case tv80
    case drawingmanga
    case sepia
    case bleachbypass
    case realvhs
    case filmcolorperfection
}

class FunnyEmojiVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var masksButton: UIButton!
    @IBOutlet weak var effectsButton: UIButton!
    @IBOutlet weak var filtersButton: UIButton!
    
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var recordActionButton: UIButton!
    
   // @IBOutlet weak var lowQVideoButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var arViewContainer: UIView!

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    private var deepAR: DeepAR!
    private var arView: ARView!

    var EmojiCount = 0
    @IBOutlet weak var recordButton: UIButton!


    private var AllfilterArray = ["None", "Alien","Aviators","Beard","Big Mouth","Dalmatian","Fatify","Flowers","Grumpy cat","Koala","Lion","Mud Mask","Obama","Pug","Slash","Small face","Teddy Cigar","Topology","Triple face"]
    var filterArray = [String]()

    // This class handles camera interaction. Start/stop feed, check permissions etc. You can use it or you
    // can provide your own implementation
    private var cameraController: CameraController!
    @IBOutlet weak var collectionView: UICollectionView!
    let margin: CGFloat = 4
    var videoURL : URL!
    
    // MARK: - Private properties -
    
    private var maskIndex: Int = 0
    private var maskPaths: [String?] {
        return Masks.allCases.map { $0.rawValue.path }
    }
    
    private var effectIndex: Int = 0
    private var effectPaths: [String?] {
        return Effects.allCases.map { $0.rawValue.path }
    }
    
    private var filterIndex: Int = 0
    private var filterPaths: [String?] {
        return Filters.allCases.map { $0.rawValue.path }
    }
    
    private var buttonModePairs: [(UIButton, Mode)] = []
    private var currentMode: Mode! {
        didSet {
            updateModeAppearance()
        }
    }
    
    private var buttonRecordingModePairs: [(UIButton, RecordingMode)] = []
    private var currentRecordingMode: RecordingMode! {
        didSet {
            updateRecordingModeAppearance()
        }
    }
    
    private var isRecordingInProcess: Bool = false

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.saveButton.isHidden = true
        self.shareButton.isHidden = true
        self.setupFilters()
        setupDeepARAndCamera()
        addTargets()

        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)

        collectionView.reloadData()
        collectionView.selectItem(at: NSIndexPath(item: 0, section: 0) as IndexPath, animated: true, scrollPosition:[])

        
        buttonRecordingModePairs = [ (photoButton, RecordingMode.photo), (videoButton, RecordingMode.video)]
        currentMode = .masks
        currentRecordingMode = .photo    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)

    }

    func setupFilters(){
        if(self.EmojiCount == 0){
            self.filterArray.append(contentsOf: self.AllfilterArray)
        }
        else{
            for ind in 0...self.EmojiCount{
                self.filterArray.append(self.AllfilterArray[ind])
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        // sometimes UIDeviceOrientationDidChangeNotification will be delayed, so we call orientationChanged in 0.5 seconds anyway
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.orientationDidChange()
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filterArray.count
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FunnyEmojiFilterCell", for: indexPath) as! FunnyEmojiFilterCell
        cell.lblItem.text = self.filterArray[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 100, height:80)

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var path: String?
        maskIndex = indexPath.row
        path = maskPaths[maskIndex]
        switchMode(path)
    }

    @IBAction func CrossBtnClicked() {
        cameraController.stopCamera()
        cameraController.stopAudio()
        cameraController.deepAR = nil
        self.arView.removeFromSuperview()
        self.deepAR.shutdown()
        self.deepAR = nil

        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func SaveBtnClicked() {
        DispatchQueue.main.async {
            self.showLoader()
        }

        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.videoURL as URL)
        }) { saved, error in
            if saved {

                DispatchQueue.main.async {
                    self.hideLoader()
                }

                let alertController = UIAlertController(title: "Your Funny video is successfully saved in your device gallery.", message: nil, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)

            }
            else {
                DispatchQueue.main.async {
                    self.hideLoader()
                }
            }
        }
    }

    @IBAction func ShareBtnClicked() {
        DispatchQueue.main.async {
            self.showLoader()
        }
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath="\(documentsPath)/funny.mp4"
        do{

            try FileManager.default.removeItem(atPath: filePath)

        }catch{

        }

        DispatchQueue.global(qos: .background).async {
            if let urlData = NSData(contentsOf: self.videoURL as URL){

                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                }
                DispatchQueue.main.async {

                    //Hide activity indicator
                    let activityVC = UIActivityViewController(activityItems: [NSURL(fileURLWithPath: filePath)], applicationActivities: nil)
                    activityVC.excludedActivityTypes = [.addToReadingList, .assignToContact,.airDrop,.copyToPasteboard,.message,.openInIBooks,.markupAsPDF,.mail]
                    self.present(activityVC, animated: true, completion: {

                        DispatchQueue.main.async {
                            self.hideLoader()
                        }
                    })
                }
            }
        }
    }
    
    // MARK: - Private methods -
    
    private func setupDeepARAndCamera() {
        self.deepAR = DeepAR()
        self.deepAR.delegate = self
        self.deepAR.setLicenseKey(LICENSE_KEY)

        cameraController = CameraController()
        cameraController.deepAR = self.deepAR
        cameraController.deepAR.initialize()

        let frame = CGRect.init(x: 0.0, y: 0.0, width: ScreenWidth, height: ScreenHeight)
        
        self.arView = self.deepAR.createARView(withFrame: frame) as? ARView
        self.arViewContainer.addSubview(self.arView)
        cameraController.startCamera()
    }
    
    private func addTargets() {
        switchCameraButton.addTarget(self, action: #selector(didTapSwitchCameraButton), for: .touchUpInside)
        recordActionButton.addTarget(self, action: #selector(didTapRecordActionButton), for: .touchUpInside)
        previousButton.addTarget(self, action: #selector(didTapPreviousButton), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(didTapNextButton), for: .touchUpInside)

        photoButton.addTarget(self, action: #selector(didTapPhotoButton), for: .touchUpInside)
        videoButton.addTarget(self, action: #selector(didTapVideoButton), for: .touchUpInside)
     //  lowQVideoButton.addTarget(self, action: #selector(didTapLowQVideoButton), for: .touchUpInside)
    }
    
    private func updateModeAppearance() {
        buttonModePairs.forEach { (button, mode) in
            button.isSelected = mode == currentMode
        }
    }
    
    private func updateRecordingModeAppearance() {
        buttonRecordingModePairs.forEach { (button, recordingMode) in
            button.isSelected = recordingMode == currentRecordingMode
        }
    }
    
    private func switchMode(_ path: String?) {
        deepAR.switchEffect(withSlot: currentMode.rawValue, path: path)
    }
    
    @objc
    private func orientationDidChange() {
        if #available(iOS 13.0, *) {
            guard let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation else { return }

            switch orientation {
            case .landscapeLeft:
                cameraController.videoOrientation = .landscapeLeft
                break
            case .landscapeRight:
                cameraController.videoOrientation = .landscapeRight
                break
            case .portrait:
                cameraController.videoOrientation = .portrait
                break
            case .portraitUpsideDown:
                cameraController.videoOrientation = .portraitUpsideDown
            default:
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc
    private func didTapSwitchCameraButton() {
        cameraController.position = cameraController.position == .back ? .front : .back
    }
    
    @objc
    private func didTapRecordActionButton() {
        self.saveButton.isHidden = true
        self.shareButton.isHidden = true
        
        if (currentRecordingMode == RecordingMode.photo) {
            deepAR.takeScreenshot()
            return
        }
        if (isRecordingInProcess) {
            deepAR.finishVideoRecording()
            self.recordButton.setImage(UIImage.init(named:"start"), for: .normal)

            isRecordingInProcess = false
            self.saveButton.isHidden = false
            self.shareButton.isHidden = false
            return
        }
        
        let width: Int32 = Int32(deepAR.renderingResolution.width)
        let height: Int32 =  Int32(deepAR.renderingResolution.height)
        
        if (currentRecordingMode == RecordingMode.video) {

            self.recordButton.setImage(UIImage.init(named:"stop_new"), for: .normal)
            deepAR.startVideoRecording(withOutputWidth: width, outputHeight: height)
            isRecordingInProcess = true
            return
        }
        
        if (currentRecordingMode == RecordingMode.lowQualityVideo) {


            /*
            let videoQuality = 0.6
            let bitrate =  1250000
            let videoSettings:[AnyHashable : AnyObject] = [
                AVVideoQualityKey : (videoQuality as AnyObject),
            ]
            
            let frame = CGRect(x: 0, y: 0, width: 200, height: 200)
            
            deepAR.startVideoRecording(withOutputWidth: width, outputHeight: height, subframe: frame, videoCompressionProperties: videoSettings, recordAudio: true)
            self.recordButton.setImage(UIImage.init(named:"stop"), for: .normal)
            isRecordingInProcess = true
 */

            self.recordButton.setImage(UIImage.init(named:"stop_new"), for: .normal)
            deepAR.startVideoRecording(withOutputWidth: width, outputHeight: height)
            isRecordingInProcess = true
            return
            

        }
    }
    
    @objc
    private func didTapPreviousButton() {
        var path: String?
        switch currentMode! {
        case .effects:
            effectIndex = (effectIndex - 1 < 0) ? (effectPaths.count - 1) : (effectIndex - 1)
            path = effectPaths[effectIndex]
        case .masks:
            maskIndex = (maskIndex - 1 < 0) ? (self.filterArray.count - 1) : (maskIndex - 1)
            collectionView.selectItem(at: NSIndexPath(item: maskIndex, section: 0) as IndexPath, animated: false, scrollPosition:.right)
            path = maskPaths[maskIndex]
        case .filters:
            filterIndex = (filterIndex - 1 < 0) ? (filterPaths.count - 1) : (filterIndex - 1)
            path = filterPaths[filterIndex]
        }
        switchMode(path)
    }
    
    @objc
    private func didTapNextButton() {
        var path: String?
        
        switch currentMode! {
        case .effects:
            effectIndex = (effectIndex + 1 > effectPaths.count - 1) ? 0 : (effectIndex + 1)
            path = effectPaths[effectIndex]
        case .masks:
            maskIndex = (maskIndex + 1 > self.filterArray.count - 1) ? 0 : (maskIndex + 1)
            self.collectionView.scrollToItem(at:  NSIndexPath(item: maskIndex, section: 0) as IndexPath, at: .left, animated: true)
            collectionView.selectItem(at: NSIndexPath(item: maskIndex, section: 0) as IndexPath, animated: true, scrollPosition:[])
            path = maskPaths[maskIndex]
        case .filters:
            filterIndex = (filterIndex + 1 > filterPaths.count - 1) ? 0 : (filterIndex + 1)
            path = filterPaths[filterIndex]
        }
        switchMode(path)
    }
    
    @objc
    private func didTapMasksButton() {
        currentMode = .masks
    }
    
    @objc
    private func didTapEffectsButton() {
        currentMode = .effects
    }
    
    @objc
    private func didTapFiltersButton() {
        currentMode = .filters
    }
    
    @objc
    private func didTapPhotoButton() {
        self.shareButton.isHidden = true
        self.saveButton.isHidden = true
        currentRecordingMode = .photo
        self.recordButton.setImage(UIImage.init(named:"start"), for: .normal)
    }
    
    @objc
    private func didTapVideoButton() {
        currentRecordingMode = .video
        self.recordButton.setImage(UIImage.init(named:"start"), for: .normal)
    }
    

}

// MARK: - ARViewDelegate -

extension FunnyEmojiVC: DeepARDelegate {
    func didFinishPreparingForVideoRecording() { }
    
    func didStartVideoRecording() { }
    
    func didFinishVideoRecording(_ videoFilePath: String!) {

        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let components = videoFilePath.components(separatedBy: "/")
        guard let last = components.last else { return }
        let destination = URL(fileURLWithPath: String(format: "%@/%@", documentsDirectory, last))

        self.videoURL = destination

        let playerController = AVPlayerViewController()
        let player = AVPlayer(url: destination)
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    func recordingFailedWithError(_ error: Error!) {

        print(error!)
    }
    
    func didTakeScreenshot(_ screenshot: UIImage!) {
        UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
        
        let imageView = UIImageView(image: screenshot)
        imageView.frame = view.frame
        view.insertSubview(imageView, aboveSubview: arView)
        
        let flashView = UIView(frame: view.frame)
        flashView.alpha = 0
        flashView.backgroundColor = .black
        view.insertSubview(flashView, aboveSubview: imageView)
        
        UIView.animate(withDuration: 0.1, animations: {
            flashView.alpha = 1
        }) { _ in
            flashView.removeFromSuperview()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                imageView.removeFromSuperview()
                self.showAlert("Bintang", message: "Image saved successfully in your device photo gallery.")
            }
        }
    }
    
    func didInitialize() {}
    func faceVisiblityDidChange(_ faceVisible: Bool) {}
}

extension String {
    var path: String? {
        return Bundle.main.path(forResource: self, ofType: nil)
    }
}
