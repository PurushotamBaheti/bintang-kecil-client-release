//
//  FunnyEmojiFilterCell.swift
//  BintangKecil
//
//  Created by Purushotam on 02/10/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class FunnyEmojiFilterCell: UICollectionViewCell {

    @IBOutlet weak var lblItem: UILabel!
    override func awakeFromNib() {

        super.awakeFromNib()
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.white.cgColor
    }
    override var isSelected: Bool{
        didSet{
            if self.isSelected {

                self.layer.borderWidth = 5.0
                self.layer.borderColor = UIColor.white.cgColor
            }
            else {
                self.layer.borderWidth = 1.0
                self.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
}
