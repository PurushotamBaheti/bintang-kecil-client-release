//
//  FunnyVoiceVC.swift
//  BintangKecil
//
//  Created by Purushotam on 17/09/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import AVFoundation

protocol AudioRecorder {
    var audioRecorder: AVAudioRecorder! { get }
}

class FunnyVoiceVC: BaseViewController,AVAudioRecorderDelegate, AudioRecorder {

    @IBOutlet weak var BottomToolView: UIView!
    @IBOutlet weak var stopBtn: UIButton!
    var scrollView = UIScrollView()
    var selectedIndex : Int  = 0
    var allButtons = [UIButton]()
    var AlltoolArray = ["Original", "Slow", "Fast","High Pitch","Low Pitch","Echo","Reverb"]
    // Instances
    var audioRecorder: AVAudioRecorder!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet  var TapMsgLbl: UILabel!
    var recordedAudioURL:URL!
    var audioFile:AVAudioFile!
    var audioEngine:AVAudioEngine!
    var audioPlayerNode: AVAudioPlayerNode!
    var stopTimer: Timer!

    var outputUrl : URL!
    var controller = UIDocumentInteractionController()
    var filterCount = 0

    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    var toolArray = [String]()


    override func viewDidLoad() {
        self.setupToolBarOptions()
        super.viewDidLoad()
        self.setupBottomToolBar()
        self.BottomToolView.isHidden = true
        self.saveBtn.isHidden = true
        self.shareBtn.isHidden = true
        self.TapMsgLbl.text =  "Tap to Record"

    }

    func setupToolBarOptions(){

        if(self.filterCount == 0){
            self.toolArray.append(contentsOf: self.AlltoolArray)
        }
        else{
            for ind in 0...self.filterCount{
                self.toolArray.append(self.AlltoolArray[ind])
            }
        }
    }

    func setupBottomToolBar() {
        scrollView.frame = CGRect.init(x:0, y: 0, width:ScreenWidth, height: 80)
        scrollView.backgroundColor = UIColor.clear
        BottomToolView.addSubview(scrollView)

        var xcoordinate : CGFloat = 0
        let width : CGFloat = 80

        for ind in 0...self.toolArray.count - 1 {
            let btn = UIButton()
            btn.frame = CGRect.init(x:xcoordinate + 10, y: 0, width: width - 20, height: 60.0)
            btn.tag = ind
            btn.setImage(UIImage.init(named:self.toolArray[ind]), for: .normal)
            btn.imageEdgeInsets = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
            btn.backgroundColor = UIColor.init(red: 237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            if(ind == 0) {
                btn.layer.borderWidth = 3
                btn.layer.borderColor = UIColor.init(red: 196.0/255.0, green: 196.0/255.0, blue: 196.0/255.0, alpha: 1.0).cgColor
            }
            else {
                btn.layer.borderWidth = 0
                btn.layer.borderColor = UIColor.clear.cgColor
            }
            btn.addTarget(self, action:#selector(self.toolbarOptionClicked(_:)), for: .touchUpInside)

            scrollView.addSubview(btn)
            allButtons.append(btn)

            let nameLbl = UILabel()
            nameLbl.frame = CGRect.init(x:xcoordinate, y: 60, width: width, height: 20)
            nameLbl.text = self.toolArray[ind]
            nameLbl.textColor = UIColor.black
            nameLbl.font = UIFont.init(name:"Noteworthy-Bold", size: 12.0)
            nameLbl.backgroundColor = UIColor.clear
            nameLbl.textAlignment = .center
            scrollView.addSubview(nameLbl)
            xcoordinate = xcoordinate + width

        }
        self.scrollView.contentSize = CGSize.init(width:xcoordinate, height: 80)

    }

    func recordSession() {
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0] as String
        let recordingName = "recordedVoice.wav"
        let pathArray = [dirPath, recordingName]
        let filePath = URL(string: pathArray.joined(separator: "/"))
        let session = AVAudioSession.sharedInstance()
        session.requestRecordPermission() { allowed in
            DispatchQueue.main.async {
                if allowed {
                    do {
                        try session.setCategory(.playAndRecord)

                        try self.audioRecorder = AVAudioRecorder(url: filePath!, settings: [:])
                        self.audioRecorder.delegate = self
                        self.audioRecorder.isMeteringEnabled = true
                        self.audioRecorder.prepareToRecord()
                        self.audioRecorder.record()
                    } catch {
                        self.showAlert(Alerts.AudioSessionError, message: String(describing: Error.self))
                    }
                } else {
                    self.showAlert(Alerts.RecordingDisabledTitle, message: Alerts.RecordingDisabledMessage)
                }
            }
        }
    }

    @objc func toolbarOptionClicked(_ sender: UIButton) {

        self.selectedIndex = sender.tag
        self.setToolBarSelectedValue()

        switch(sender.tag) {

        case 0:
            playSound(rate: 1.0)
        case 1:
            playSound(rate: 0.5)
        case 2:
            playSound(rate: 1.5)
        case 3:
            playSound(pitch: 1000)
        case 4:
            playSound(rate: 0.95, pitch: -368, echo: false, reverb: true, vader: true)
        case 5:
            playSound(echo: true)
        case 6:
            playSound(reverb: true)
        default:
            playSound(rate: 1.0)
        }

    }

    func setToolBarSelectedValue() {
        for i in 0...self.allButtons.count - 1 {
            if(i == self.selectedIndex) {
                self.allButtons[i].layer.borderWidth = 3
                self.allButtons[i].layer.borderColor = UIColor.init(red: 196.0/255.0, green: 196.0/255.0, blue: 196.0/255.0, alpha: 1.0).cgColor
            }
            else {
                self.allButtons[i].layer.borderWidth = 0
                self.allButtons[i].layer.borderColor = UIColor.clear.cgColor
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    @IBAction func TapBtnClicked() {
        if(audioPlayerNode != nil) {
            if(audioPlayerNode.isPlaying == true) {
                self.stopAudio()
            }
        }
        self.TapMsgLbl.text =  "Speak now: We are listening...\nWhen done recording,click the Stop Button"
        self.saveBtn.isHidden = true
        self.shareBtn.isHidden = true
        self.selectedIndex = 0
        self.setToolBarSelectedValue()
        self.BottomToolView.isHidden = true
        self.stopBtn.isEnabled = true
        self.stopBtn.alpha = 1.0
        self.recordButton.isEnabled = false
        self.recordButton.alpha = 0.7
        self.recordSession()
    }

    @IBAction func StopBtnClicked() {
        self.TapMsgLbl.text =  "Tap to Record"
        self.saveBtn.isHidden = false
        self.shareBtn.isHidden = false
        self.stopBtn.isEnabled = false
        self.stopBtn.alpha = 0.7
        self.recordButton.isEnabled = true
        self.recordButton.alpha = 1.0

        audioRecorder.stop()
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(false)
        } catch {
            showAlert(Alerts.RecordingFailedTitle, message: Alerts.RecordingDisabledMessage)
        }
    }

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            self.setupAudio()
            self.BottomToolView.isHidden = false

        } else {
            showAlert(Alerts.AudioSessionError, message: String(describing: Error.self))
        }
    }

    @IBAction func ShareBtnClicked()
    {
        switch(self.selectedIndex) {
        case 0:
            shareSound(rate: 1.0)
        case 1:
            shareSound(rate: 0.5)
        case 2:
            shareSound(rate: 1.5)
        case 3:
            shareSound(pitch: 1000)
        case 4:
            shareSound(rate: 0.95, pitch: -368, echo: false, reverb: true, vader: true)
        case 5:
            shareSound(echo: true)
        case 6:
            shareSound(reverb: true)
        default:
            shareSound(rate: 1.0)
        }
    }
}

extension FunnyVoiceVC: AVAudioPlayerDelegate {
    func setupAudio() {
        // initialize (recording) audio file
        do {
            audioFile = try AVAudioFile(forReading: self.audioRecorder.url as URL)
        } catch {
            showAlert(Alerts.AudioFileError, message: String(describing: error))
        }
    }

    // Mark: Play Sound

    func playSound(rate: Float? = nil, pitch: Float? = nil, echo: Bool = false, reverb: Bool = false, vader: Bool = false) {

        // initialize audio engine components
        audioEngine = AVAudioEngine()
        // node for playing audio
        audioPlayerNode = AVAudioPlayerNode()
        audioEngine.attach(audioPlayerNode)

        // node for adjusting rate/pitch
        let changeRatePitchNode = AVAudioUnitTimePitch()
        if let pitch = pitch {
            changeRatePitchNode.pitch = pitch
        }
        if let rate = rate {
            changeRatePitchNode.rate = rate
        }
        audioEngine.attach(changeRatePitchNode)

        // node for echo
        let echoNode = AVAudioUnitDistortion()
        echoNode.loadFactoryPreset(.multiEcho1)
        audioEngine.attach(echoNode)

        let reverbNode = AVAudioUnitReverb()
        if vader {
            // node for reverb
            reverbNode.loadFactoryPreset(.mediumHall)
            reverbNode.wetDryMix = 16
            audioEngine.attach(reverbNode)
        } else {
            // node for reverb
            reverbNode.loadFactoryPreset(.cathedral)
            reverbNode.wetDryMix = 50
            audioEngine.attach(reverbNode)
        }


        // connect nodes
        if echo == true && reverb == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, reverbNode, audioEngine.outputNode)
        } else if echo == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, audioEngine.outputNode)
        } else if reverb == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, reverbNode, audioEngine.outputNode)
        } else {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, audioEngine.outputNode)
        }

        // schedule to play and start the engine!
        audioPlayerNode.stop()
        audioPlayerNode.scheduleFile(audioFile, at: nil) {

            var delayInSeconds: Double = 0

            if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {

                if let rate = rate {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                } else {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                }
            }

            // schedule a stop timer for when audio finishes playing
            self.stopTimer = Timer(timeInterval: delayInSeconds, target: self, selector: #selector(FunnyVoiceVC.stopAudio), userInfo: nil, repeats: false)
            RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
        }

        do {
            try audioEngine.start()
        } catch {
            showAlert(Alerts.AudioEngineError, message: String(describing: error))
            return
        }

        // play the recording!
        audioPlayerNode.play()
    }

    @objc func stopAudio() {
        if let audioPlayerNode = audioPlayerNode {
            audioPlayerNode.stop()
        }
        if let stopTimer = stopTimer {
            stopTimer.invalidate()
        }
        if let audioEngine = audioEngine {
            audioEngine.stop()
            audioEngine.reset()
        }
    }

    // MARK: Connect List of Audio Nodes

    func connectAudioNodes(_ nodes: AVAudioNode...) {
        for x in 0..<nodes.count-1 {
            audioEngine.connect(nodes[x], to: nodes[x+1], format: audioFile.processingFormat)
        }
    }


    func shareSound(rate: Float? = nil, pitch: Float? = nil, echo: Bool = false, reverb: Bool = false, vader: Bool = false) {

        let loadmessage  = "Please wait!. This may take a while.\nTunggu sebentar!. Ini mungkin memakan waktu cukup lama."
        self.showLoaderwithMessage(message: loadmessage)
        // initialize audio engine components
        audioEngine = AVAudioEngine()

        // node for playing audio
        audioPlayerNode = AVAudioPlayerNode()
        audioEngine.attach(audioPlayerNode)

        // node for adjusting rate/pitch
        let changeRatePitchNode = AVAudioUnitTimePitch()
        if let pitch = pitch {
            changeRatePitchNode.pitch = pitch
        }
        if let rate = rate {
            changeRatePitchNode.rate = rate
        }
        audioEngine.attach(changeRatePitchNode)

        // node for echo
        let echoNode = AVAudioUnitDistortion()
        echoNode.loadFactoryPreset(.multiEcho1)
        audioEngine.attach(echoNode)

        let reverbNode = AVAudioUnitReverb()
        if vader {
            // node for reverb
            reverbNode.loadFactoryPreset(.mediumHall)
            reverbNode.wetDryMix = 16
            audioEngine.attach(reverbNode)
        } else {
            // node for reverb
            reverbNode.loadFactoryPreset(.cathedral)
            reverbNode.wetDryMix = 50
            audioEngine.attach(reverbNode)
        }


        // connect nodes
        if echo == true && reverb == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, reverbNode, audioEngine.outputNode)
        } else if echo == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, echoNode, audioEngine.outputNode)
        } else if reverb == true {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, reverbNode, audioEngine.outputNode)
        } else {
            connectAudioNodes(audioPlayerNode, changeRatePitchNode, audioEngine.outputNode)
        }

        // schedule to play and start the engine!
        var delayInSeconds: Double = 0
        audioPlayerNode.stop()
        audioPlayerNode.scheduleFile(audioFile, at: nil) {

            if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {

                if let rate = rate {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate) / Double(rate)
                } else {
                    delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
                }
            }

            // schedule a stop timer for when audio finishes playing
            self.stopTimer = Timer(timeInterval: delayInSeconds, target: self, selector: #selector(FunnyVoiceVC.stopAudio), userInfo: nil, repeats: false)
            RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
        }
        do {
            let maxNumberOfFrames: AVAudioFrameCount = 4096 // maximum number of frames the engine will be asked to render in any single render call
            try audioEngine.enableManualRenderingMode(.offline, format: audioFile.processingFormat, maximumFrameCount: maxNumberOfFrames)
        } catch {
            fatalError("could not enable manual rendering mode, \(error)")
        }

        do {
            try audioEngine.start()
            audioPlayerNode.play()
        } catch {
            showAlert(Alerts.AudioEngineError, message: String(describing: error))
            return
        }
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0] as String
        let recordingName = "effectedSound2.caf"
        let pathArray = [dirPaths, recordingName]
        let filePath = URL(string: pathArray.joined(separator: "/"))!
        let filePathString: String = pathArray.joined(separator: "/")
        self.outputUrl = filePath

        let outputFile: AVAudioFile
        do {
            let outputURL = filePath
            outputFile = try AVAudioFile(forWriting: outputURL, settings: audioFile.fileFormat.settings)
        } catch {
            fatalError("could not open output audio file, \(error)")
        }

        // buffer to which the engine will render the processed data
        let buffer: AVAudioPCMBuffer = AVAudioPCMBuffer(pcmFormat: audioEngine.manualRenderingFormat, frameCapacity: audioEngine.manualRenderingMaximumFrameCount)!
        //: ### Render loop
        //: Pull the engine for desired number of frames, write the output to the destination file
        while audioEngine.manualRenderingSampleTime < audioFile.length {
            do {
                let framesToRender = min(buffer.frameCapacity, AVAudioFrameCount(audioFile.length - audioEngine.manualRenderingSampleTime))
                let status = try audioEngine.renderOffline(framesToRender, to: buffer)
                switch status {
                case .success:
                    // data rendered successfully
                    try outputFile.write(from: buffer)
                case .insufficientDataFromInputNode:
                    // applicable only if using the input node as one of the sources
                    break
                case .cannotDoInCurrentContext:
                    // engine could not render in the current render call, retry in next iteration
                    break
                case .error:
                    // error occurred while rendering
                    fatalError("render failed")
                }
            } catch {
                fatalError("render failed, \(error)")
            }
        }

        // code
        audioPlayerNode.stop()
        audioEngine.stop()
        audioEngine.reset()

        DispatchQueue.main.async {
            //Hide activity indicator
            let activityVC = UIActivityViewController(activityItems: [NSURL(fileURLWithPath: filePathString)], applicationActivities: nil)
            activityVC.excludedActivityTypes = [.addToReadingList, .assignToContact,.airDrop,.copyToPasteboard,.message,.openInIBooks,.markupAsPDF,.mail]
            self.present(activityVC, animated: true, completion: {

                DispatchQueue.main.async {
                    self.hideLoader()
                }
            })
        }
    }
}
