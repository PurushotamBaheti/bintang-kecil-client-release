//
//  PurchasePlanVC.swift
//  BintangKecil
//
//  Created by Devesh on 24/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class PurchasePlanVC: BaseViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate,paymentDelegate {
    
    @IBOutlet weak var tableView: UITableView!

    var localDataModel : PaymentPlanModel!
    var FeaturesArray = [PayFeatureModel]()
    var Price : Double = 0.0
    var discount : Double  = 0.0
    var isPromoApplied : Bool = false
    var promoCode = ""
    var isFromAddChild : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.IntialiseData()
    }

    func IntialiseData(){
        self.FeaturesArray.append(contentsOf: localDataModel.features)
        self.Price = Double.init(localDataModel.amount)!
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(section == 0){
            return 1
        }
        else if(section == 1){
            return self.localDataModel.features.count
        }
        else if(section == 2){
            return 1
        }
        else if(section == 2){
            return 1
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanHeaderCell", for: indexPath as IndexPath) as! PlanHeaderCell

            cell.planNameLbl.text = localDataModel.name
            cell.planPriceLbl.text = localDataModel.currency_code + " " + localDataModel.amount
            cell.planDescLbl.text = localDataModel.desc?.htmlToString
            if(!localDataModel.desc!.isEmpty){
                cell.bottomHeight.constant = 4
            }
            cell.planBgView.clipsToBounds = true
            cell.planBgView.layer.cornerRadius = 10
            cell.planBgView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]

            DispatchQueue.main.async {
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
            }
            cell.selectionStyle = .none
            return cell
        }
        else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanCell", for: indexPath) as! PlanCell
            let wrapper = self.FeaturesArray[indexPath.row]
            cell.planLbl.text = wrapper.name
            cell.planImg.sd_setImage(with: NSURL.init(string: wrapper.icon) as URL?, placeholderImage:UIImage.init(named:"child"), options: .highPriority, context: .init())
            cell.selectionStyle = .none
            if(indexPath.row == self.FeaturesArray.count - 1){
                cell.planBgView.clipsToBounds = true
                cell.planBgView.layer.cornerRadius = 10
                cell.planBgView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            }
            return cell
        }
        else if(indexPath.section == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath)
            return cell
        }
        else if(indexPath.section == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanPromoCodeCell", for: indexPath) as! PlanPromoCodeCell
            cell.promoTextField.delegate = self
            if(self.isPromoApplied == false){
                cell.deleteBtn.isHidden = true
                cell.promoTextField.isEnabled = true
                cell.promoTextField.alpha = 1.0
                cell.applyBtn.isEnabled = true
                cell.applyBtn.alpha = 1.0
                cell.promoTextField.text = self.promoCode
            }
            else{
                cell.deleteBtn.isHidden = false
                cell.promoTextField.isEnabled = false
                cell.promoTextField.alpha = 0.4
                cell.applyBtn.isEnabled = false
                cell.applyBtn.alpha = 0.4
                cell.promoTextField.text = self.promoCode

            }
            cell.promoTextField.tag = 101
            cell.deleteBtn.addTarget(self, action: #selector(RemovePromoCodeBtnClicked(_:)), for: .touchUpInside)
            cell.applyBtn.addTarget(self, action: #selector(ApplyPromoCodeBtnClicked(_:)), for: .touchUpInside)
            cell.planPriceLbl.text = self.localDataModel.currency_code + " " + "\(self.Price)"
            cell.planDiscountbl.text = self.localDataModel.currency_code + " " + "\(self.discount)"
            let payment = self.Price - self.discount
            cell.planPayablePriceLbl.text = self.localDataModel.currency_code + " " + "\(payment)"
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanCheckOutCell", for: indexPath) as! PlanCheckOutCell
            let payment = self.Price - self.discount
            let str  = "Proceed to Pay " + self.localDataModel.currency_code + " " + "\(payment)"
            cell.checkOutBtn.setTitle(str, for: .normal)
            cell.checkOutBtn.addTarget(self, action: #selector(CheckOutBtnClicked(_:)), for: .touchUpInside)

            return cell
        }
    }

    @objc func RemovePromoCodeBtnClicked(_ sender: UIButton){
        self.promoCode = ""
        self.discount = 0.0
        self.isPromoApplied = false
        self.tableView.reloadData()

    }

    @objc func ApplyPromoCodeBtnClicked(_ sender: UIButton){
        self.view.endEditing(true)
        if(self.promoCode.isEmpty){
            self.showToast(message: MessageString.EMPTY_PROMOCODE)
        }
        else{
            self.callApplyPromoCodeApi()
        }

    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.tag == 101){
            self.promoCode = textField.text!
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func callApplyPromoCodeApi(){
        var parameter = [String:Any]()
        parameter["code"] = self.promoCode
        parameter["plan_id"] = self.localDataModel.id

        showLoader()
        postRequest(strUrl: "promocode/apply", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                let dic = json["data"]["discount"].intValue
                if(Double(dic) < self.Price){
                    self.discount = Double(dic)
                    self.isPromoApplied = true
                    self.showToast(message: "Promo Code Applied.")
                    self.tableView.reloadData()
                }
            }
            else if(json["status"].stringValue == "201"){
                let message = json["message"].stringValue
                self.showToast(message: message)
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    @objc func CheckOutBtnClicked(_ sender: UIButton){
        self.callCheckOutApi()
    }

    func callCheckOutApi(){
        var parameter = [String:Any]()
        parameter["code"] = self.promoCode
        parameter["plan_id"] = self.localDataModel.id
        parameter["payment"] = "xendit"

        showLoader()
        postRequest(strUrl: "purchase-subscription", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                let fullUrl = json["data"]["full_url"].stringValue
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
                vc.pageTitle = "Subscription Purchase"
                vc.pageUrl = fullUrl
                vc.isFromAddChild = self.isFromAddChild
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)

            }
            else if(json["status"].stringValue == "201"){
                let message = json["message"].stringValue
                self.showToast(message: message)
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
    func paymentFailed() {

        let message = "Transaction declined!. Do you want to continue payment with some other options ?"
        let dialogMessage = UIAlertController(title: "Bintang Kecil", message: message, preferredStyle: .alert)

        // Create OK button with action handler
        let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.callCheckOutApi()
        })

        let cancel = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in

        })
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)

        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)

    }

    func callActivateSubscriptionApi(orderId:String){
        var parameter = [String:Any]()
        parameter["order_id"] = orderId

        showLoader()
        postRequest(strUrl: "user-activate-plan", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                let message = "Subscription purchased successfully."
                self.showToast(message: message)

                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0){
                    if(self.isFromAddChild == true){
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: AddChildVC.self) {
                                _ =  self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                    else{
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
            else if(json["status"].stringValue == "201"){
                let message = json["message"].stringValue
                self.showToast(message: message)
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
}

class PlanHeaderCell: UITableViewCell {

    @IBOutlet weak var planPriceLbl: UILabel!
    @IBOutlet weak var planNameLbl: UILabel!
    @IBOutlet weak var planDescLbl: UILabel!
    @IBOutlet weak var planBgView: UIView!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }

}

class PlanPromoCodeCell: UITableViewCell {

    @IBOutlet weak var planPriceLbl: UILabel!
    @IBOutlet weak var planDiscountbl: UILabel!
    @IBOutlet weak var planPayablePriceLbl: UILabel!
    @IBOutlet weak var promoTextField: UITextField!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }
}

class PlanCheckOutCell: UITableViewCell {

    @IBOutlet weak var checkOutBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }
}

