//
//  PaymentPlanVC.swift
//  BintangKecil
//
//  Created by Devesh on 24/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import ScalingCarousel


class PaymentPlanVC: BaseViewController {

    @IBOutlet weak var carouselHeight: NSLayoutConstraint!
    var dataArray = [PaymentPlanModel]()
    @IBOutlet weak var carousel: ScalingCarouselView!
    var isFromAddChild : Bool = false
    @IBOutlet weak var helpView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.fetchPaymentPlanList()
        let HelpShown = UserDefaults.standard.value(forKey:"helpshown")
        if(HelpShown == nil){
            helpView.frame = self.view.bounds
            self.view.addSubview(helpView)
            UserDefaults.standard.set("yes", forKey: "helpshown")
            UserDefaults.standard.synchronize()
        }

        // Do any additional setup after loading the view.
    }

    @IBAction func removeHelpAction(sender: AnyObject) {
        helpView.removeFromSuperview()
    }


    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {

    }

    @IBAction func PayAction(sender: AnyObject) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let subscriptionvc = storyBoard.instantiateViewController(withIdentifier: "PurchasePlanVC")
        self.navigationController?.pushViewController(subscriptionvc, animated: true)
        
    }
    func fetchPaymentPlanList(){
        let parameter = [String:Any]()

        showLoader()
        postRequest(strUrl: "subscription-plan/list", param: parameter, success: { (json) in
            self.hideLoader()
            print(json)
            if json["status"].stringValue == "200"{
                let arrData = json["data"].arrayValue
                if(arrData.count > 0){
                    for ind in 0...arrData.count - 1{
                        let wrapper = PaymentPlanModel.init(fromJson: arrData[ind])
                        self.dataArray.append(wrapper)
                    }

                    if(ScreenHeight <= 568.0){
                        let carouselHeightval  = 0.75 * ScreenHeight
                        self.carouselHeight.constant = carouselHeightval
                    }
                    else{
                        let carouselHeightval  = 0.80 * ScreenHeight
                        self.carouselHeight.constant = carouselHeightval
                    }
                    self.carousel.reloadData()
                }
            }
            else if(json["status"].stringValue == "201"){
                let message = json["message"].stringValue
                self.showToast(message: message)
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
}

typealias CarouselDatasource = PaymentPlanVC
extension CarouselDatasource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Cell
        let wrapper = self.dataArray[indexPath.row]
        cell.lblPlanTitleLbl.text = wrapper.name + " Features"
        cell.DataArray = wrapper.features
        cell.desc = wrapper.desc!
        cell.FeaturesTable.reloadData()
        cell.PurchaseBtn.tag = indexPath.row
        cell.PurchaseBtn.addTarget(self, action: #selector(PurchaseBtnClicked(_:)), for: .touchUpInside)

        DispatchQueue.main.async {
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
        }

        cell.dropShadow(UIColor.lightGray)

        return cell
    }

    @objc func PurchaseBtnClicked(_ sender: UIButton){
        let wrapper = self.dataArray[sender.tag]
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let subscriptionvc = storyBoard.instantiateViewController(withIdentifier: "PurchasePlanVC") as! PurchasePlanVC
        subscriptionvc.localDataModel = wrapper
        subscriptionvc.isFromAddChild = self.isFromAddChild
        
        self.navigationController?.pushViewController(subscriptionvc, animated: true)
        
    }
}

typealias CarouselDelegate = PaymentPlanVC
extension PaymentPlanVC: UICollectionViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        carousel.didScroll()
        guard let _ = carousel.currentCenterCellIndex?.row else { return }
    }
}

private typealias ScalingCarouselFlowDelegate = PaymentPlanVC
extension ScalingCarouselFlowDelegate: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

import SDWebImage
class Cell: ScalingCarouselCell,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var lblPlanTitleLbl: UILabel!
    @IBOutlet weak var PurchaseBtn: UIButton!
    @IBOutlet weak var FeaturesTable: UITableView!
    var  DataArray = [PayFeatureModel]()
    var desc = ""

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(!self.desc.isEmpty){
            return self.DataArray.count + 1
        }
        return self.DataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == self.DataArray.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanDescCell", for: indexPath as IndexPath) as! PlanDescCell
            cell.planDescLbl.text = self.desc.htmlToString

            return cell

        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanCell", for: indexPath as IndexPath) as! PlanCell
            let wrapper = self.DataArray[indexPath.row]
            cell.planLbl.text = wrapper.name
            cell.planImg.sd_setImage(with: NSURL.init(string: wrapper.icon) as URL?, placeholderImage:UIImage.init(named:"child"), options: .refreshCached, context: .init())
            cell.selectionStyle = .none
            return cell
        }
    }
}


class PlanCell: UITableViewCell {

    @IBOutlet weak var planLbl: UILabel!
    @IBOutlet weak var planImg: UIImageView!
    @IBOutlet weak var planBgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }
}

class PlanDescCell: UITableViewCell {

    @IBOutlet weak var planDescLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func draw(_ rect: CGRect) {

    }
}
