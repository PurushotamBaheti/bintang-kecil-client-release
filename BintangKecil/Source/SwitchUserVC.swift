//
//  SwitchUserVC.swift
//  BintangKecil
//
//  Created by Purushotam on 24/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class SwitchUserVC: BaseViewController,AddChildDelegate {
    
    @IBOutlet weak var addChildBtn: UIButton!
    @IBOutlet weak var parentImg: UIImageView!
    @IBOutlet weak var parentInnerCircle: UIView!
    @IBOutlet weak var parentLbl: UILabel!
    @IBOutlet weak var parentOuterCircle: UIView!
    @IBOutlet weak var childImg: UIImageView!
    @IBOutlet weak var childInnerCircle: UIView!
    @IBOutlet weak var childLbl: UILabel!
    @IBOutlet weak var childOuterCircle: UIView!
    
    var dataArray = [ChildModel]()
    var childScroll = UIScrollView()
    var isParentSelected : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.getChildList()
        
    }
    
    func setUI()
    {
        let isChild = UserDefaults.standard.getChild()
        if isChild{
            
            parentImg.image = UIImage.init(named: "parent_black")
            parentInnerCircle.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            parentLbl.textColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            parentOuterCircle.backgroundColor = UIColor.clear
            
            childImg.image = UIImage.init(named: "childIcon")
            childInnerCircle.backgroundColor = UIColor.white
            childLbl.textColor = UIColor.black
            childOuterCircle.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            
            addChildBtn.isHidden = false
            isParentSelected = false
        }
        else{
            
            parentImg.image = UIImage.init(named: "parent")
            parentInnerCircle.backgroundColor = UIColor.white
            parentLbl.textColor = UIColor.black
            parentOuterCircle.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            
            childImg.image = UIImage.init(named: "childIcon_black")
            childInnerCircle.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            childLbl.textColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            childOuterCircle.backgroundColor = UIColor.clear
            
            addChildBtn.isHidden = true
            isParentSelected = true
        }
        
        createChildView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func ParentSelected() {
        self.parentImg.image = UIImage.init(named: "parent")
        self.parentInnerCircle.backgroundColor = UIColor.white
        self.parentLbl.textColor = UIColor.black
        self.parentOuterCircle.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
        
        self.childImg.image = UIImage.init(named: "childIcon_black")
        self.childInnerCircle.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
        self.childLbl.textColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
        self.childOuterCircle.backgroundColor = UIColor.clear
        
        self.addChildBtn.isHidden = true
        self.isParentSelected = true
        let subViews = self.childScroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        UserDefaults.standard.setChild(false)
        UserDefaults.standard.synchronize()
        
    }
    
    @IBAction func ChildSelected(){
        self.parentImg.image = UIImage.init(named: "parent_black")
        self.parentInnerCircle.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
        self.parentLbl.textColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
        self.parentOuterCircle.backgroundColor = UIColor.clear
        
        self.childImg.image = UIImage.init(named: "childIcon")
        self.childInnerCircle.backgroundColor = UIColor.white
        self.childLbl.textColor = UIColor.black
        self.childOuterCircle.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
        
        self.addChildBtn.isHidden = false
        self.isParentSelected = false
        if(self.dataArray.count > 0){
            self.addChildinUI()
        }
        
    }
    
    func getChildList(){
        showLoader()
        getRequest(strUrl: "children/list", success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                let tempArray = json["data"].arrayValue
                if(tempArray.count > 0){
                    for ind in 0...tempArray.count - 1{
                        let localArr = tempArray[ind]
                        let wrapper = ChildModel.init(id: localArr["id"].intValue, name:  localArr["name"].stringValue,image: localArr["image_url"].stringValue)
                        self.dataArray.append(wrapper)
                    }
                    if(self.isParentSelected == false){
                        self.addChildinUI()
                    }
                }
            }
            else{
                
            }
            
        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
    
    func createChildView(){
        var ycoordinate : CGFloat = 250
        if(ScreenHeight > 811){
            ycoordinate = 270
        }
        childScroll.frame = CGRect.init(x:20, y: ycoordinate, width: ScreenWidth-40, height: ScreenHeight - 420)
        childScroll.backgroundColor = UIColor.clear
        self.view.addSubview(childScroll)
    }
    
    func addChildinUI(){
        let subViews = self.childScroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        var xcoordinate : CGFloat = 0
        var yCoordinate: CGFloat = 0
        let gap : CGFloat = 24
        
        for ind in 0...self.dataArray.count - 1{
            let wrapper = self.dataArray[ind]
            let size = wrapper.name.size(withAttributes:[.font: UIFont.init(name:"Noteworthy-Bold", size: 15.0)!])
            let textsize = size.width  + 18 + 40
            if((xcoordinate + textsize + gap) > self.childScroll.frame.width){
                yCoordinate = yCoordinate + 40 + 14
                xcoordinate = 0
            }
            
            let boxView = UIView()
            boxView.frame = CGRect.init(x:xcoordinate, y: yCoordinate, width: textsize, height: 40.0)
            boxView.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            boxView.layer.cornerRadius = 20
            self.childScroll.addSubview(boxView)
            
            // inner box
            let innerImgView = UIImageView()
            innerImgView.frame = CGRect.init(x:5, y: 5, width: 30, height: 30)
            innerImgView.image = UIImage.init(named:"childIcon_black")
            innerImgView.contentMode = .scaleAspectFill
            innerImgView.clipsToBounds = true
            innerImgView.layer.cornerRadius = 15
            boxView.addSubview(innerImgView)
            
            let nameLbl = UILabel()
            nameLbl.frame = CGRect.init(x:44, y: 5, width: textsize - 44 - 9, height: 30)
            nameLbl.text = wrapper.name!
            nameLbl.textColor = UIColor.black
            
            if let _ = UserDefaults.standard.object(forKey: "childdata"){
                let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
                if(decodedData.id  == wrapper .id){
                    nameLbl.textColor = UIColor.white
                }
            }
            nameLbl.font = UIFont.init(name:"Noteworthy-Bold", size: 15.0)
            nameLbl.textAlignment = .center
            boxView.addSubview(nameLbl)
            
            let btn = UIButton()
            btn.frame = CGRect.init(x:0, y: 0, width: textsize, height: 40.0)
            btn.backgroundColor = UIColor.clear
            btn.tag = ind
            btn.addTarget(self, action: #selector(SelectChildBtnAction(_:)), for: .touchUpInside)
            boxView.addSubview(btn)
            
            xcoordinate = xcoordinate + textsize + gap
            
        }
    }
    
    
    @IBAction func AddChildClicked(){
        //addchild
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "addchild") as! AddChildVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func childAdddedSuccessfully() {
        self.dataArray.removeAll()
        self.getChildList()
    }
    
    @objc func SelectChildBtnAction(_ sender: UIButton){
        
        let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you switch to this user ?", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.isChildAccountSwitched = true
            
            let wrapper = self.dataArray[sender.tag]
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: wrapper)
            UserDefaults.standard.set(encodedData, forKey: "childdata")
            UserDefaults.standard.setChild(true)
            UserDefaults.standard.synchronize()
            self.navigationController?.popViewController(animated: true)
            
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
        
    }
    
}
