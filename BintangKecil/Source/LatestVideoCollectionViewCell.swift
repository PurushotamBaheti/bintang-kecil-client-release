//
//  StoryboardCollectionViewCell.swift
//  CenteredCollectionView_Example
//
//  Created by Benjamin Emdon on 2018-04-12.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

class LatestVideoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var posterImg : UIImageView!
    @IBOutlet weak var downloadView: UIView!
    @IBOutlet weak var downloadBtn: UIButton!
    @IBOutlet weak var downloadImgView: UIImageView!
    
}
