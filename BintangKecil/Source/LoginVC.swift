//
//  LoginVC.swift
//  BintangKecil
//
//  Created by Owebest on 04/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON
import GoogleSignIn
import FirebaseCrashlytics
import TwitterKit
import TwitterCore
import SKCountryPicker
import AuthenticationServices

class LoginVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var countryCodeTxtField: UITextField!
    @IBOutlet weak var showPasswordBtn: UIButton!

    var isShowPassword : Bool = false
    var validation = Validation()
    var socialId = ""
    var socialEmail = ""
    var socialName = ""
    var loginType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.PageName = "Login"
        
        self.navigationController?.navigationBar.isHidden = true
        self.customiseUI()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userDidSignInGoogle(_:)),
                                               name: .signInGoogleCompleted,
                                               object: nil)
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func customiseUI() {

        userNameTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        passwordTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        mobileTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        
    }

    @IBAction func CountryBtnClicked(){
      presentCountryPickerScene()

    }
    
    @IBAction func SignUpClicked() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "signup") as! SignupVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func ForgotPasswordClicked() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "forgotpassword") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func LoginClicked() {
        loginType = 0
        self.view.endEditing(true)
        if(self.ValidateTextFields() == true) {
            self.callLoginApi()
        }
    }
    
    func ValidateTextFields() -> Bool {
        if((userNameTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! && (mobileTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_EMAIL_MOBILE)
            return false
        }

        else if(!(userNameTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! && validation.validateEmailId(emailID: self.userNameTxtField.text!) == false) {
            self.showToast(message: MessageString.INVALID_EMAIL)
            return false
        }
        else if(!(mobileTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! && validation.validaPhoneNumber(phoneNumber: self.mobileTxtField.text!) == false) {
                self.showToast(message: MessageString.INVALID_MOBILE)
                return false
        }
        else if((passwordTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_PASSWORD)
            return false
        }
        else if(validation.validatePassword(password:self.passwordTxtField.text!) == false) {
            self.showToast(message: MessageString.INVALID_PASSWORD)
            return false
        }
        
        return true
    }
    
    func callLoginApi() {
        var parameter = [String:Any]()
        parameter["type"] = loginType
        if(!(mobileTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            parameter["email"] = ""
            var mobilenum = countryCodeTxtField.text! + ((mobileTxtField.text?.trimmingCharacters(in: .whitespaces))!)
            mobilenum = mobilenum.replacingOccurrences(of:"+", with: "")
            parameter["mobile"] = mobilenum
        }
        else {
            parameter["mobile"] = ""
            parameter["email"] = (userNameTxtField.text?.trimmingCharacters(in: .whitespaces))!
        }
        
        var token = ""
        if let tokenstr = UserDefaults.standard.value(forKey: "fcmId") {
            token = tokenstr as! String
        }
        parameter["password"] = passwordTxtField.text
        parameter["osType"] = "ios"
        parameter["macId"] = DEVICE_ID
        parameter["tokenId"] = token
        parameter["social_id"] = socialId
        
        showLoader()
        postRequest(strUrl: "login", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {
                
                let userWrapper = UserModal.init(id: json["data"]["id"].intValue, image: json["data"]["image"].stringValue, email: json["data"]["email"].stringValue, image_thumbnail: json["data"]["image_thumbnail"].stringValue, accessToken: json["data"]["accessToken"].stringValue, user_code: json["data"]["user_code"].stringValue, name: json["data"]["name"].stringValue, mobile: json["data"]["mobile"].stringValue)
                
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userWrapper)
                UserDefaults.standard.set(encodedData, forKey: "userdata")
                UserDefaults.standard.setLogin(true)
                UserDefaults.standard.synchronize()
                self.view.endEditing(true)
                let window = UIApplication.shared.keyWindow
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "tabbar")
                window?.rootViewController = dashboard
            }
            else {
                let message = json["message"].stringValue
                self.showToast(message: message)
            }
            
        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
    
    func callSocialLoginApi() {
        var token = ""
        if let tokenstr = UserDefaults.standard.value(forKey: "fcmId") {
            token = tokenstr as! String
        }
        
        var parameter = [String:Any]()
        parameter["type"] = loginType
        parameter["password"] = ""
        parameter["osType"] = "ios"
        parameter["macId"] = DEVICE_ID
        parameter["tokenId"] = token
        parameter["social_id"] = socialId
        parameter["email"] = socialEmail
        parameter["mobile"] = ""
        print(parameter)
        
        showLoader()
        postRequest(strUrl: "login", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {
                let userWrapper = UserModal.init(id: json["data"]["id"].intValue, image: json["data"]["image"].stringValue, email: json["data"]["email"].stringValue, image_thumbnail: json["data"]["image_thumbnail"].stringValue, accessToken: json["data"]["accessToken"].stringValue, user_code: json["data"]["user_code"].stringValue, name: json["data"]["name"].stringValue, mobile: json["data"]["mobile"].stringValue)
                
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userWrapper)
                UserDefaults.standard.set(encodedData, forKey: "userdata")
                
                UserDefaults.standard.setLogin(true)
                UserDefaults.standard.synchronize()
                self.view.endEditing(true)
                let window = UIApplication.shared.keyWindow
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "tabbar")
                window?.rootViewController = dashboard
            }
            else if(json["status"].stringValue == "201") {
                
                if(self.loginType > 0){
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "social") as! SocialSignupVC
                    vc.socialId = self.socialId
                    vc.userName = self.socialName
                    vc.userEmail = self.socialEmail
                    vc.userMobile = ""
                    vc.type = self.loginType
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    let message = json["message"].stringValue
                    self.showToast(message: message)
                }
                
            }
            else {
                let message = json["message"].stringValue
                self.showToast(message: message)
            }
            
        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func callAppleLoginApi() {
        var token = ""
        if let tokenstr = UserDefaults.standard.value(forKey: "fcmId") {
            token = tokenstr as! String
        }

        var parameter = [String:Any]()
        parameter["type"] = loginType
        parameter["password"] = ""
        parameter["osType"] = "ios"
        parameter["macId"] = DEVICE_ID
        parameter["tokenId"] = token
        parameter["social_id"] = socialId
        parameter["email"] = socialEmail
        parameter["mobile"] = ""

        showLoader()
        postRequest(strUrl: "ios-social-email-login", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {
                let userWrapper = UserModal.init(id: json["data"]["id"].intValue, image: json["data"]["image"].stringValue, email: json["data"]["email"].stringValue, image_thumbnail: json["data"]["image_thumbnail"].stringValue, accessToken: json["data"]["accessToken"].stringValue, user_code: json["data"]["user_code"].stringValue, name: json["data"]["name"].stringValue, mobile: json["data"]["mobile"].stringValue)

                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userWrapper)
                UserDefaults.standard.set(encodedData, forKey: "userdata")

                UserDefaults.standard.setLogin(true)
                UserDefaults.standard.synchronize()
                self.view.endEditing(true)
                let window = UIApplication.shared.keyWindow
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "tabbar")
                window?.rootViewController = dashboard
            }
            else if(json["status"].stringValue == "201") {

                if(self.loginType > 0){
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "social") as! SocialSignupVC
                    vc.socialId = self.socialId
                    vc.userName = self.socialName
                    vc.userEmail = self.socialEmail
                    vc.userMobile = ""
                    vc.type = self.loginType
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    let message = json["message"].stringValue
                    self.showToast(message: message)
                }

            }
            else {
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
    
    
    
    @IBAction func ShowPasswordClicked() {
        if(isShowPassword == false) {
            isShowPassword = true
            self.passwordTxtField.isSecureTextEntry = false
            showPasswordBtn.setImage(UIImage.init(named:"password_hide_black"), for: .normal)
        }
        else {
            isShowPassword = false
            self.passwordTxtField.isSecureTextEntry = true
            showPasswordBtn.setImage(UIImage.init(named:"password_visible_black"), for: .normal)
        }
    }
    
    @IBAction func FacebookBtnClicked() {
        
        let loginManager = LoginManager()
        
        if let _ = AccessToken.current {
            // Access token available -- user already logged in
            // Perform log out
            
            loginManager.logOut()
            
        } 
        // Access token not available -- user already logged out
        // Perform log in
        
        loginManager.logIn(permissions: ["public_profile","email"], from: self) { [weak self] (result, error) in
            
            // Check for error
            guard error == nil else {
                // Error occurred
                return
            }
            
            // Check for cancel
            guard let result = result, !result.isCancelled else {
                return
            }
            
            guard let accessToken = FBSDKLoginKit.AccessToken.current else { return }
            let graphRequest = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                          parameters: ["fields": "email, name"],
                                                          tokenString: accessToken.tokenString,
                                                          version: nil,
                                                          httpMethod: .get)
            graphRequest.start { (connection, result, error) -> Void in
                if error == nil {
                    
                    guard let Info = result as? [String: Any] else { return }
                    
                    var fbId = ""
                    var fbEmail = ""
                    var FbName = ""
                    if let userName = Info["name"] as? String{
                        FbName = userName
                    }
                    if let userId = Info["id"] as? String{
                        fbId = userId
                    }
                    if let userEmail = Info["email"] as? String{
                        fbEmail = userEmail
                    }
                    self?.socialId = fbId
                    self?.socialEmail = fbEmail
                    self?.socialName = FbName
                    self?.loginType = 1
                    self?.callSocialLoginApi()
                    
                    
                }
                else {
                    
                    print("error \(error)")
                }
            }
            
        }
        
    }
    
    @IBAction func GoogleBtnClicked() {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func TwitterBtnClicked() {
        TWTRTwitter.sharedInstance().logIn { [weak self] (session, error) in
            guard let self = self  else { return }
            if error != nil {
                
                //  self.ShowAlert(message: error!.localizedDescription)
                return
            }
            
            var TwitterEmail = ""
            
            let client = TWTRAPIClient.withCurrentUser()
            client.requestEmail { email, error in
                if (email != nil) {
                    let recivedEmailID = email ?? ""
                    TwitterEmail = recivedEmailID
                    self.loginType = 3
                    self.socialId = session!.userID
                    self.socialName = session!.userName
                    self.socialEmail = TwitterEmail
                    let store = TWTRTwitter.sharedInstance().sessionStore
                    store.logOutUserID(session!.userID)
                    
                    
                    self.callSocialLoginApi()
                    
                    
                }else {
                    print("error--: \(String(describing: error?.localizedDescription))");
                }
            }
            
        }
    }
    
    
    // MARK:- Notification
    @objc private func userDidSignInGoogle(_ notification: Notification) {
        // Update screen after user successfully signed in
        if let user = GIDSignIn.sharedInstance()?.currentUser {
            self.loginType = 2
            self.socialName = user.profile.name!
            self.socialEmail = user.profile.email!
            self.socialId = user.userID
            GIDSignIn.sharedInstance()?.signOut()
            self.callSocialLoginApi()
            
        }
    }

    
}


/// MARK: - Private Methods
private extension LoginVC {

    func presentCountryPickerScene(withSelectionControlEnabled selectionControlEnabled: Bool = true) {
        switch selectionControlEnabled {
        case true:
            // Present country picker with `Section Control` enabled
            _ = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
                guard let self = self else { return }
                self.countryCodeTxtField.text = country.dialingCode!
            }

        case false:
            _ = CountryPickerController.presentController(on: self) { [weak self] (country: Country) in
                guard let self = self else { return }
                self.countryCodeTxtField.text = country.dialingCode!
            }

        }
    }
}

extension LoginVC: ASAuthorizationControllerPresentationContextProviding,ASAuthorizationControllerDelegate {

    // For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }

    @IBAction func actionHandleAppleSignin() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }else{
            self.showToast(message: "Not supoorted.Require iOS version 13 or greater.")
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Get user data with Apple ID credentitial
            let userId = appleIDCredential.user
            let userFirstName = appleIDCredential.fullName?.givenName
            let userLastName = appleIDCredential.fullName?.familyName
            let userEmail = appleIDCredential.email
            if(userEmail != nil){
               self.socialEmail = userEmail!
            } else {
               self.socialEmail = ""
            }
            if(userFirstName == nil){
                 self.socialName = ""
            }else {
                 self.socialName = userFirstName! + " " + userLastName!
            }

            self.socialId = userId
            self.loginType = 4
            self.callAppleLoginApi()


            // Write your code here
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Get user data using an existing iCloud Keychain credential
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password

            self.socialEmail = ""
            self.socialName = ""
            self.socialId = appleUsername
            self.loginType = 4
            self.callAppleLoginApi()

            // Write your code here
        }
    }



}
