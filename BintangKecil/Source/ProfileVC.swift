//
//  ProfileVC.swift
//  BintangKecil
//
//  Created by Owebest on 13/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SDWebImage
import MobileCoreServices

extension UIImage {
    func upOrientationImage() -> UIImage? {
        switch imageOrientation {
        case .up:
            return self
        default:
            UIGraphicsBeginImageContextWithOptions(size, false, scale)
            draw(in: CGRect(origin: .zero, size: size))
            let result = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return result
        }
    }
}

class ProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,editProfileDelegate,verifyPasswordDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var bgHeight: NSLayoutConstraint!
    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var ChildImgEditBtn: UIButton!
    var userImage : UIImage? = nil
    var picker:UIImagePickerController?=UIImagePickerController()
    @IBOutlet weak var membershipLbl: UILabel!


    var dataArray = ["My Downloads", "Switch Account", "Settings","About Us","Our Terms","Privacy Policy","FAQ"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.customiseUI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.loadProfileView()
        self.fetchPlanInfo()
        self.tabBarController?.tabBar.isHidden = false

    }

    func fetchPlanInfo() {

        var parameter = [String:Any]()
        let isChild = UserDefaults.standard.getChild()
        if isChild{

            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            parameter["child_id"] = decodedData.id

        }
        showLoader()
        postRequest(strUrl: "user-plan", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {
                let planName = json["data"]["name"].stringValue
                let planDate = json["data"]["subscription_start_date"].stringValue

                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"//this your string date format
                dateFormatter.locale = Locale(identifier: NSLocale.current.identifier)


                let convertedDate = dateFormatter.date(from: planDate)

                dateFormatter.dateFormat = "dd MMM YYYY"///this is what you want to convert format
                let timeStamp = dateFormatter.string(from: convertedDate!)

                self.membershipLbl.text = planName + " Member " + "since " + timeStamp

            }
            else if(json["status"].stringValue == "202") {
                self.membershipLbl.text = ""
                let message = json["message"].stringValue
                self.showToast(message: message)
                if(message == "Your account may be deactivated from admin. please contact to admin support!") {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        UserDefaults.standard.removeObject(forKey:"childdata")
                        UserDefaults.standard.removeObject(forKey: "userdata")
                        UserDefaults.standard.setChild(false)
                        UserDefaults.standard.setLogin(false)
                        UserDefaults.standard.synchronize()
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                        appDelegate.window?.rootViewController = dashboard
                    }
                }
            }
            else {
                //  let message = json["message"].stringValue
                // self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            //  self.ShowAlert(message: msg)
        }
    }


    func loadProfileView() {
        let isChild = UserDefaults.standard.getChild()
        if isChild{

            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            self.userNameLbl.text = decodedData.name
            if(decodedData.image_url! == "") {
                userImgView.image = UIImage.init(named: "noImage")
            }
            else {
                userImgView.sd_setImage(with: URL(string: decodedData.image_url!), placeholderImage: UIImage(named: "noImage"))
            }
            self.editBtn.isHidden = true
            self.ChildImgEditBtn.isHidden = false
        }
        else {
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
            self.userNameLbl.text = decodedData.name
            if(decodedData.image.isEmpty) {
                userImgView.image = UIImage.init(named: "noImage")
            }
            else {
                userImgView.sd_setImage(with: URL(string: decodedData.image), placeholderImage: UIImage(named: "noImage"))
            }

            self.editBtn.isHidden = false
            self.ChildImgEditBtn.isHidden = true
        }
    }


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func customiseUI() {

        self.tabBarController?.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.tabBarController?.tabBar.layer.shadowRadius = 5
        self.tabBarController?.tabBar.layer.shadowOpacity = 0.65
        self.tabBarController?.tabBar.layer.masksToBounds = false

        self.tblView.estimatedRowHeight = 80
        self.tblView.rowHeight = UITableView.automaticDimension
    }

    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath as IndexPath) as! ProfileCell
        cell.titleLbl.text = self.dataArray[indexPath.row]
        cell.selectionStyle = .none
        return cell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if(indexPath.row == 0){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "download") as! MyDownloadVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else  if(indexPath.row == 2) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "settings") as! SettingsVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if(indexPath.row == 1) {
            let isChild = UserDefaults.standard.getChild()
            if isChild{

                self.CheckIfPasswordExists()
            }
            else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "switchuser") as! SwitchUserVC
                self.navigationController?.pushViewController(vc, animated: false)
            }

        }
        else if(indexPath.row == 3) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
            vc.pageTitle = "About Us"
            vc.pageUrl = ABOUT_US_URL
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else if(indexPath.row == 4) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
            vc.pageTitle = "Our Terms"
            vc.pageUrl = TERMS_URL
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else if(indexPath.row == 5) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
            vc.pageTitle = "Privacy Policy"
            vc.pageUrl = PRIVACY_URL
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else if(indexPath.row == 6){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
            vc.pageTitle = "FAQ"
            vc.pageUrl = FAQ_URL
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }

    @IBAction func editAction(sender: AnyObject) {

        let isChild = UserDefaults.standard.getChild()
        if isChild{

            self.showToast(message: "You are not authorized to access this feature. You must have parent account access for this.")

        }
        else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "editprofile") as! EditProfileVC
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: false)
        }

    }

    func profileUpdatedSuccessfully() {

        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
        self.userNameLbl.text = decodedData.name
        if(decodedData.image.isEmpty){
            userImgView.image = UIImage.init(named: "noImage")
        }

    }

    func CheckIfPasswordExists(){
        showLoader()
        getRequest(strUrl: "is-password-set", success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "mpin") as! VerifyPasswordVC
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)

            }
            else{
                self.showToast(message: "No Password set for the account. Please logout and set your password from forgot password section.")

            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
    func PasswordVerifiedSuccessfully() {

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "switchuser") as! SwitchUserVC
        self.navigationController?.pushViewController(vc, animated: false)

    }

    @IBAction func ImageBtnAction(){
        let alert:UIAlertController=UIAlertController(title: "Edit Image Options", message: nil, preferredStyle: UIAlertController.Style.actionSheet)

        let gallaryAction = UIAlertAction(title: "Open Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in self.openGallary()
        }
        let CameraAction = UIAlertAction(title: "Open Camera", style: UIAlertAction.Style.default){
            UIAlertAction in self.openCamera()
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in self.cancel()

        }
        alert.addAction(gallaryAction)
        alert.addAction(CameraAction)
        alert.addAction(cancelAction)

        self.present(alert, animated: true, completion: nil)
    }
    func openGallary() {
        picker?.delegate = self
        picker!.allowsEditing = true
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }

    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerController.SourceType.camera;
            imag.mediaTypes = [kUTTypeImage] as [String]
            imag.allowsEditing = true

            self.present(imag, animated: true, completion: nil)
        }
    }
    func cancel(){

    }

    private func imagePickerControllerDidCancel(picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        let image = info[.originalImage] as! UIImage
        userImage = image
        self.userImgView.image = image
        self.CallEditProfileApi()

    }

    func CallEditProfileApi(){

        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel

        var parameter = [String:Any]()
        parameter["child_id"] = decodedData.id

        var TotalArray = [[String : Any]]()
        if(self.userImage != nil) {
            let dict = ["img":self.userImage as Any,"key":"image"]
            TotalArray.append(dict)
        }

        showLoader()

        uploadData(strUrl: "children/image/upload", arrImage: TotalArray, param: parameter, success: { (json) in
            if json["status"].stringValue == "200" {

                let wrapper = ChildModel.init(id: json["data"]["id"].intValue, name: json["data"]["name"].stringValue, image: json["data"]["image"].stringValue)
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: wrapper)
                UserDefaults.standard.set(encodedData, forKey: "childdata")
                UserDefaults.standard.setChild(true)
                UserDefaults.standard.synchronize()

                self.showToast(message: "Profile Image updated successfully!")
                self.loadProfileView()

            }
            self.hideLoader()

        }, failure: {
            (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg as! String)
        })
    }

}

class ProfileCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }

}
