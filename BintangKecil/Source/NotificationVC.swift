//
//  NotificationVC.swift
//  BintangKecil
//
//  Created by Owebest on 13/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class NotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource,AVPlayerViewControllerDelegate {

    var dataArray = [NotificationModel]()
    @IBOutlet weak var tblView: UITableView!
    var playerController = AVPlayerViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblView.estimatedRowHeight = 140
        self.tblView.rowHeight = UITableView.automaticDimension

        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.tabBarController?.tabBar.layer.shadowRadius = 5
        self.tabBarController?.tabBar.layer.shadowOpacity = 0.65
        self.tabBarController?.tabBar.layer.masksToBounds = false
        
        // Do any additional setup after loading the view.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .default
    }

    override func viewWillAppear(_ animated: Bool) {
        self.dataArray.removeAll()
        self.tblView.reloadData()
        self.getNotificationList()
    }

    func getNotificationList(){
        self.tblView.backgroundView = nil
        showLoader()
        getRequest(strUrl: "user-notifications", success: { (json) in
        self.hideLoader()
        if json["status"].stringValue == "200"{

            let arrData = json["data"].arrayValue
            if(arrData.count > 0){
                for ind in 0...arrData.count - 1{
                    let wrapper = NotificationModel.init(fromJson: arrData[ind])
                    self.dataArray.append(wrapper)
                }
                self.tblView.reloadData()
             }
            else{
                self.CreateNoFiltersView()
            }
        }
        else if( json["status"].stringValue == "202"){
            let message = json["message"].stringValue
            self.showToast(message: message)
             if(message == "Your account may be deactivated from admin. please contact to admin support!"){
                   DispatchQueue.main.asyncAfter(deadline: .now() + 3.0){
                      UserDefaults.standard.removeObject(forKey:"childdata")
                      UserDefaults.standard.removeObject(forKey: "userdata")
                      UserDefaults.standard.setChild(false)
                      UserDefaults.standard.setLogin(false)
                      UserDefaults.standard.synchronize()
                      let appDelegate = UIApplication.shared.delegate as! AppDelegate
                      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                      let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                      appDelegate.window?.rootViewController = dashboard
                    }
                 }
        }
        else{
           let message  = json["message"].stringValue
           if(message == "No Records Found"){
             self.CreateNoFiltersView()
            }
            else{
            self.showToast(message: message)
            }
        }

        }) { (msg) in
            self.hideLoader()
        self.ShowAlert(message: msg)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
 
      let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath as IndexPath) as! notificationCell
      let wrapper = self.dataArray[indexPath.row]
      if(wrapper.type == 1){
          cell.notiImg.image = UIImage.init(named:"Articles_theme")
      }
      else if(wrapper.type == 2){
          cell.notiImg.image = UIImage.init(named:"home_theme")
      }
      else if(wrapper.type == 3 || wrapper.type == 7){
          cell.notiImg.image = UIImage.init(named:"Forum_theme")
      }
      else if(wrapper.type == 4){
          cell.notiImg.image = UIImage.init(named:"Coloring_theme")
      }
      else if(wrapper.type == 5){
          cell.notiImg.image = UIImage.init(named:"Tracing_theme")
      }
      else if(wrapper.type == 6 || wrapper.type == 8 || wrapper.type == 9){
         cell.notiImg.image = UIImage.init(named:"Karaoke_theme")
      }
      else{
        cell.notiImg.image = UIImage.init(named:"notifications_selected")
      }
      cell.notiImg.contentMode = .scaleAspectFit
      cell.titleLbl.text = wrapper.title
      cell.descLbl.text = wrapper.body
      cell.timeLbl.text = wrapper.time
      cell.selectionStyle = .none
      return cell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let wrapper = self.dataArray[indexPath.row]
        if(wrapper.type == 1){
          let vc = self.storyboard?.instantiateViewController(withIdentifier: "ArticlesVC") as! ArticlesVC
          self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(wrapper.type == 3){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
            vc.forumTagId = "\(wrapper.id ?? 0)"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if(wrapper.type == 4){
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColoringVC") as! ColoringVC
           vc.pageTitle = NSLocalizedString("Coloring", comment: "")
           vc.isForColoring = true
           self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(wrapper.type == 5){
          let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColoringVC") as! ColoringVC
          vc.pageTitle = NSLocalizedString("Tracing", comment: "")
          vc.isForColoring = false
          self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(wrapper.type == 6){
             let vc = self.storyboard?.instantiateViewController(withIdentifier: "KaraokeListVC") as! KaraokeListVC
             self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(wrapper.type == 8 || wrapper.type == 9){
             let urldata = URL(string: wrapper.url!)
             let player = AVPlayer(url:urldata!)
             playerController = AVPlayerViewController()
             playerController.player = player
             playerController.allowsPictureInPicturePlayback = true
             playerController.delegate = self
             playerController.player?.play()
             self.present(playerController,animated:true,completion:nil)
        }
        else if(wrapper.type == 2){
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.Categories = ""
            appDelegate.CategoriesNames = ""
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
            vc.sort = "2"
            vc.order = "asc"
            vc.searchStr = ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(wrapper.type == 7){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
            vc.forumTagId = "\(wrapper.id ?? 0)"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if(wrapper.type == 10){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPlanVC") as! PaymentPlanVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func CreateNoFiltersView(){
        let popupview = UIView()
        popupview.backgroundColor = UIColor.clear
        self.tblView.backgroundView = popupview

        let ycor = (ScreenHeight - self.tblView.frame.origin.y)/2

        let subview = UIView()
        subview.frame = CGRect.init(x:30, y: ycor - 160, width: popupview.frame.size.width - 60, height: 160)
        subview.backgroundColor = UIColor.clear
        popupview.addSubview(subview)

        let logo = UIImageView.init()
        logo.frame = CGRect.init(x:(subview.frame.size.width - 80)/2, y:20, width: 80, height: 80)
        logo.image = UIImage.init(named:"logo")
        logo.contentMode = .scaleAspectFit
        subview.addSubview(logo)

        let messageLbl = UILabel()
        messageLbl.frame = CGRect.init(x: 5, y: 90, width: subview.frame.size.width - 10, height: 50)
        messageLbl.backgroundColor = UIColor.clear
        messageLbl.text = "No Notifications yet."

        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.init(name: "Metropolis-ExtraLight", size: 13.0)
        messageLbl.textColor = UIColor.black
        messageLbl.numberOfLines = 3
        subview.addSubview(messageLbl)

    }

}


class notificationCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var notiImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }

}
