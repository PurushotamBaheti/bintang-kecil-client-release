//
//  ForgotPasswordVC.swift
//  BintangKecil
//
//  Created by Owebest on 13/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SKCountryPicker

class ForgotPasswordVC: BaseViewController {

    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var countryCodeTxtField: UITextField!
    
    var validation = Validation()

    override func viewDidLoad() {
        super.viewDidLoad()
        mobileTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))

    }

    @IBAction func CountryBtnClicked(){
        self.presentCountryPickerScene()

    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .default
    }

    @IBAction func SubmitBtnClicked(){
        self.view.endEditing(true)
        if(self.ValidateTextFields() == true) {
            self.callCredentialsExistApi()
        }
    }

    func ValidateTextFields() -> Bool {
        if((mobileTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_MOBILE)
            return false
        }
        else if(validation.validaPhoneNumber(phoneNumber: self.mobileTxtField.text!) == false) {
             self.showToast(message: MessageString.INVALID_MOBILE)
             return false
        }
        return true
    }

    func callCredentialsExistApi() {
          var parameter = [String:Any]()
          parameter["email"] = ""
          var mobilenum = self.countryCodeTxtField.text! + self.mobileTxtField.text!
          mobilenum = mobilenum.replacingOccurrences(of:"+", with: "")
          parameter["mobile"] = mobilenum

          showLoader()
          postRequest(strUrl: "check-email-mobile-exist", param: parameter, success: { (json) in
          self.hideLoader()
          if json["status"].stringValue == "200" {
            let mobilenumb = self.countryCodeTxtField.text! + self.mobileTxtField.text!

             let emailstr = json["data"]["email"].stringValue
             let vc = self.storyboard?.instantiateViewController(withIdentifier: "otp") as! OtpVC
             vc.isFromSignUp = false
             vc.mobile = mobilenumb
             vc.email = emailstr
             self.navigationController?.pushViewController(vc, animated: false)
          }
          else {
          let message = json["message"].stringValue
          self.showToast(message: message)
          }

          }) { (msg) in
            self.hideLoader()
          self.ShowAlert(message: msg)
          }
      }

}


/// MARK: - Private Methods
private extension ForgotPasswordVC {

    func presentCountryPickerScene(withSelectionControlEnabled selectionControlEnabled: Bool = true) {
        switch selectionControlEnabled {
        case true:
            // Present country picker with `Section Control` enabled
            _ = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
                guard let self = self else { return }
                self.countryCodeTxtField.text = country.dialingCode!
            }

        case false:
            _ = CountryPickerController.presentController(on: self) { [weak self] (country: Country) in
                guard let self = self else { return }
                self.countryCodeTxtField.text = country.dialingCode!
            }

        }
    }
}
