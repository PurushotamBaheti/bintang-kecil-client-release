//
//  AppDelegate.swift
//  NFC-Example
//
//  Created by Hans Knöchel on 08.06.17.
//  Copyright © 2017 Hans Knoechel. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import GoogleSignIn
import Firebase
import TwitterKit
import UserNotifications
import AVKit
import AVFoundation
import CallKit


extension UIWindow {
    
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        return nil
    }
    
    class func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        
        switch(vc){
        case is UINavigationController:
            let navigationController = vc as! UINavigationController
            return UIWindow.getVisibleViewControllerFrom( vc: navigationController.visibleViewController!)
            
        case is UITabBarController:
            let tabBarController = vc as! UITabBarController
            return UIWindow.getVisibleViewControllerFrom(vc: tabBarController.selectedViewController!)
            
        default:
            if let presentedViewController = vc.presentedViewController {
                if let presentedViewController2 = presentedViewController.presentedViewController {
                    return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController2)
                }
                else{
                    return vc;
                }
            }
            else{
                return vc;
            }
        }
        
    }
    
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate,AVPlayerViewControllerDelegate,CXCallObserverDelegate {
    
    var window: UIWindow?
    var PageName  = ""
    var Categories = ""
    var CategoriesNames = ""
    var KaraokeId = ""
    var KaraokeNames = ""
    var isChildAccountSwitched : Bool = false
    var isAppOpenedFromPush : Bool = false
    var PushType = ""
    var PushId = ""
    var PushUrl = ""
    var playerController = AVPlayerViewController()
    var callObserver = CXCallObserver()
    
    var isIncomingCall : Bool = false
    
    
    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions:
            launchOptions
        )
        
        if(launchOptions != nil)
        {
            isAppOpenedFromPush = true
        }
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        // Request permission to display alerts and play sounds.
        center.requestAuthorization(options: [.alert, .sound])
        { (granted, error) in
            // Enable or disable features based on authorization.
        }
        center.removeAllDeliveredNotifications()
        
        
        
        GIDSignIn.sharedInstance().clientID = GOOGLE_CLIENT_ID
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
     //   Messaging.messaging().shouldEstablishDirectChannel = true
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        application.registerForRemoteNotifications()
        
        
        TWTRTwitter.sharedInstance().start(withConsumerKey: TWITTER_CONSUMER_KEY, consumerSecret: TWITTER_CONSUMER_SECRET)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Noteworthy-Bold", size: 10)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Noteworthy-Bold", size: 10)!], for: .selected)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(red: 237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)], for: .selected)
        
        
        let login = UserDefaults.standard.getLogin()
        if login{
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let dashboard = storyBoard.instantiateViewController(withIdentifier: "tabbar")
            let frame = UIScreen.main.bounds
            window = UIWindow(frame: frame)
            self.window?.rootViewController = dashboard
            
        }
        else{
            //loginnav
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
            let frame = UIScreen.main.bounds
            window = UIWindow(frame: frame)
            self.window?.rootViewController = dashboard
            
        }
        
        callObserver.setDelegate(self, queue: nil) //Set delegate to self to call delegate method.
        self.window?.makeKeyAndVisible()
        
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        if(self.isIncomingCall == true){
            
            self.isIncomingCall = false
            if let topController = self.window?.visibleViewController(){
                
                if let rootVC = topController as? FunnyVoiceVC {
                    rootVC.navigationController?.popViewController(animated: true)
                    
                    if let topController2 = self.window?.visibleViewController() {
                        topController2.showToast(message: "This activity is abruptly closed due to incoming call.Please try again")
                    }
                    
                }
                
                if let rootVC1 = topController as? KaraokeLipsingVC {
                    rootVC1.navigationController?.popViewController(animated: true)
                    
                    if let topController2 = self.window?.visibleViewController() {
                        topController2.showToast(message: "This activity is abruptly closed due to incoming call.Please try again")
                    }
                }
                
                if let rootVC2 = topController as? KaraokeDetailVC {
                    rootVC2.navigationController?.popViewController(animated: true)
                    if let topController2 = self.window?.visibleViewController() {
                        topController2.showToast(message: "This activity is abruptly closed due to incoming call.Please try again")
                    }
                }
                
            }
        }
    }
    
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        
        self.isIncomingCall = true
        
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "fcmId")
        UserDefaults.standard.synchronize()
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                let strToken: String = result.token
                print("deviceTokenString---->>",strToken)
                UserDefaults.standard.set(strToken, forKey: "deviceID")
            }
        }
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if TWTRTwitter.sharedInstance().application(app, open: url, options: options) {
            return true
        }
        
        return ApplicationDelegate.shared.application(
            app,
            open: url,
            options: options
        )
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let _ = notification.request.content.userInfo
        completionHandler([.alert, .sound])
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        if let pushtype = userInfo["type"] {
            
            self.PushType = pushtype as! String
            if let pushId = userInfo["id"] {
                self.PushId = pushId as! String
            }
            
            if let pushurl = userInfo["url"] {
                self.PushUrl = pushurl as! String
            }
            
            if(self.isAppOpenedFromPush == true) {
                self.perform(#selector(navigatePageWithDelay), with: nil, afterDelay: 1.2)
            }
            else {
                self.navigatePageWithDelay()
            }
            
        }
        
        completionHandler()
        
    }
    
    @objc func navigatePageWithDelay() {
        
        self.isAppOpenedFromPush = false
        if let topController = self.window?.visibleViewController() {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            
            if(self.PushType == "1") {
                let vc = storyBoard.instantiateViewController(withIdentifier: "ArticlesVC") as! ArticlesVC
                topController.navigationController?.pushViewController(vc, animated: true)
                
            } else if(self.PushType == "2") {
                self.Categories = ""
                self.CategoriesNames = ""
                let vc = storyBoard.instantiateViewController(withIdentifier: "home") as! HomeVC
                vc.sort = "2"
                vc.order = "asc"
                vc.searchStr = ""
                topController.navigationController?.pushViewController(vc, animated: true)
            }
            else if(self.PushType == "3" || self.PushType == "7") {
                if(!self.PushId.isEmpty) {
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
                    vc.forumTagId = self.PushId
                    topController.navigationController?.pushViewController(vc, animated: true)
                    
                }
            }
            else if(self.PushType == "4") {
                let vc = storyBoard.instantiateViewController(withIdentifier: "ColoringVC") as! ColoringVC
                vc.pageTitle = NSLocalizedString("Coloring", comment: "")
                vc.isForColoring = true
                topController.navigationController?.pushViewController(vc, animated: true)
                
            }
            else if(self.PushType == "5") {
                let vc = storyBoard.instantiateViewController(withIdentifier: "ColoringVC") as! ColoringVC
                vc.pageTitle = NSLocalizedString("Tracing", comment: "")
                vc.isForColoring = false
                topController.navigationController?.pushViewController(vc, animated: true)
                
            }
            else if(self.PushType == "6") {
                let vc = storyBoard.instantiateViewController(withIdentifier: "KaraokeListVC") as! KaraokeListVC
                topController.navigationController?.pushViewController(vc, animated: true)
            }
            else if(self.PushType == "8" || self.PushType == "9") {
                if(!self.PushUrl.isEmpty) {
                    let urldata = URL(string: self.PushUrl)
                    let player = AVPlayer(url:urldata!)
                    playerController = AVPlayerViewController()
                    playerController.player = player
                    playerController.delegate = self
                    playerController.player?.play()
                    topController.present(playerController, animated: true, completion: nil)
                    
                }
            }
            else if(self.PushType == "10") {
                let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentPlanVC") as! PaymentPlanVC
                topController.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}

extension AppDelegate: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!,
              didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        
        // Check for sign in error
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        if(self.PageName == "Login") {
            // Post notification after user successfully sign in
            NotificationCenter.default.post(name: .signInGoogleCompleted, object: nil)
        }
        else {
            // Post notification after user successfully sign in
            NotificationCenter.default.post(name: .signUpGoogleCompleted, object: nil)
        }
        
        
    }
}

// MARK:- Notification names
extension Notification.Name {
    
    /// Notification when user successfully sign in using Google
    static var signInGoogleCompleted: Notification.Name {
        return .init(rawValue: #function)
    }
    
    static var signUpGoogleCompleted: Notification.Name {
        return .init(rawValue: #function)
    }
}

