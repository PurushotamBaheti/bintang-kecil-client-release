//
//  NewHomeVC.swift
//  BintangKecil
//
//  Created by Purushotam on 07/09/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import AVFoundation

class NewHomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,AVPlayerViewControllerDelegate,UICollectionViewDelegateFlowLayout,categoriesAddDelegate {

    var latestVideoArr = [VideoListModel]()
    var dataArray  = [VideoCategoryModel]()
    @IBOutlet weak var tblView: UITableView!
    var playerController = AVPlayerViewController()
    var isScrollSet : Bool = false
    let cellPercentWidth: CGFloat = 0.7
    private let downloadManager = SDDownloadManager.shared
    private let refreshControl = UIRefreshControl()


    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblView.estimatedRowHeight = 80
        self.tblView.rowHeight = UITableView.automaticDimension

        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.tabBarController?.tabBar.layer.shadowRadius = 5
        self.tabBarController?.tabBar.layer.shadowOpacity = 0.65
        self.tabBarController?.tabBar.layer.masksToBounds = false

        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        tblView.alwaysBounceVertical = true
        tblView.refreshControl = refreshControl

    }

    @objc
    private func didPullToRefresh(_ sender: Any) {
        // Do you your api calls in here, and then asynchronously remember to stop the
        // refreshing when you've got a result (either positive or negative)
        dataArray.removeAll()
        self.latestVideoArr.removeAll()
        self.tblView.reloadData()
        self.fetchVideoList()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override func viewWillAppear(_ animated: Bool) {

        dataArray.removeAll()
        self.latestVideoArr.removeAll()
        self.tblView.reloadData()
        self.fetchVideoList()

    }

    @IBAction func SearchAction(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.Categories = ""
        appDelegate.CategoriesNames = ""
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
        vc.sort = "2"
        vc.order = "asc"
        vc.searchStr = ""
        vc.isFromSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func FilterAction(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "categories") as! CategoriesVC
        vc.category_type = 1
        vc.type = 2
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)

    }

    func categoriesUpdatedSuccessfully() {
        self.perform(#selector(pushHomePageWithDelay), with: nil, afterDelay: 1.0)
    }

    @IBAction func SortingControl(_ sender: Any){

        let alert = UIAlertController(title: "Sort By", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: "Name (A-Z)", style: .default , handler:{ (UIAlertAction)in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.Categories = ""
            appDelegate.CategoriesNames = ""
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
            vc.sort = "1"
            vc.order = "asc"
            vc.searchStr = ""
            self.navigationController?.pushViewController(vc, animated: true)


        }))

        alert.addAction(UIAlertAction(title: "Name (Z-A)", style: .default , handler:{ (UIAlertAction)in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.Categories = ""
            appDelegate.CategoriesNames = ""
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
            vc.sort = "1"
            vc.order = "desc"
            vc.searchStr = ""
            self.navigationController?.pushViewController(vc, animated: true)

        }))

        alert.addAction(UIAlertAction(title: "Date (Ascending)", style: .default , handler:{ (UIAlertAction)in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.Categories = ""
            appDelegate.CategoriesNames = ""
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
            vc.sort = "2"
            vc.order = "asc"
            vc.searchStr = ""
            self.navigationController?.pushViewController(vc, animated: true)

        }))

        alert.addAction(UIAlertAction(title: "Date (Descending)", style: .default , handler:{ (UIAlertAction)in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.Categories = ""
            appDelegate.CategoriesNames = ""
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
            vc.sort = "2"
            vc.order = "desc"
            vc.searchStr = ""
            self.navigationController?.pushViewController(vc, animated: true)


        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))

        self.present(alert, animated: true, completion: {
        })

    }

    @objc func pushHomePageWithDelay(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
        vc.sort = "2"
        vc.order = "asc"
        vc.searchStr = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }


    func fetchVideoList(){
        var parameter = [String:Any]()
        let isChild = UserDefaults.standard.getChild()
        if isChild{
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "childdata") as! Data) as! ChildModel
            parameter["child_id"] = decodedData.id

        }
        let loadmessage  = "Loading.."
        self.showLoaderwithMessage(message: loadmessage)
        postRequest(strUrl: "home-video-list", param: parameter, success: { (json) in
            if json["success"].stringValue == "true"{
                self.refreshControl.endRefreshing()
                if json["message"].stringValue == "No Records Found"{
                    self.ShowAlert(message: "No Record Found.")
                }
                else{
                    let arrData = json["data"].dictionaryValue["latest-videos"]!.arrayValue
                    if(arrData.count > 0){
                        for ind in 0...arrData.count - 1{
                            let wrapper = VideoListModel.init(fromJson: arrData[ind])
                            if let _ = UserDefaults.standard.object(forKey: "video"){
                                let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "video") as! Data) as! [MediaModel]
                                for ind in 0...decodedData.count - 1{
                                    let savedWrapper = decodedData[ind]
                                    if(wrapper.id == savedWrapper.id){
                                        wrapper.isDownloaded = true
                                    }
                                }
                            }
                            self.latestVideoArr.append(wrapper)
                        }
                        self.tblView.reloadData()
                    }
                    // parsing category video list

                    let arrData2 = json["data"].dictionaryValue["categories"]!.arrayValue
                    if(arrData2.count > 0){
                        for ind in 0...arrData2.count - 1{
                            let wrapper = VideoCategoryModel.init(fromJson: arrData2[ind])
                            if(wrapper.videoList.count > 0){
                                self.dataArray.append(wrapper)
                            }
                        }
                        self.tblView.reloadData()
                    }
                }
            }
            else{
                self.refreshControl.endRefreshing()
                let message = json["message"].stringValue
                self.showToast(message: message)
                if(message == "Your account may be deactivated from admin. please contact to admin support!"){
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0){
                        UserDefaults.standard.removeObject(forKey:"childdata")
                        UserDefaults.standard.removeObject(forKey: "userdata")
                        UserDefaults.standard.setChild(false)
                        UserDefaults.standard.setLogin(false)
                        UserDefaults.standard.synchronize()
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                        appDelegate.window?.rootViewController = dashboard
                    }
                }
            }
            self.hideLoader()
        })
        {
            (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func resetDownloadState(){
        for ind in 0...latestVideoArr.count - 1{
            let wrapper = latestVideoArr[ind]
            if let _ = UserDefaults.standard.object(forKey: "video"){
                let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "video") as! Data) as! [MediaModel]
                for ind in 0...decodedData.count - 1{
                    let savedWrapper = decodedData[ind]
                    if(wrapper.id == savedWrapper.id){
                        wrapper.isDownloaded = true
                    }
                }
            }
            self.latestVideoArr[ind] = wrapper
        }

        for x in 0...dataArray.count - 1{
            let videoArr = dataArray[x].videoList
            var tempVideoArr = [VideoListModel]()
            for y in 0...videoArr.count - 1{
                let wrapper = videoArr[y]
                if let _ = UserDefaults.standard.object(forKey: "video"){
                    let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "video") as! Data) as! [MediaModel]
                    for ind in 0...decodedData.count - 1{
                        let savedWrapper = decodedData[ind]
                        if(wrapper.id == savedWrapper.id){
                            wrapper.isDownloaded = true
                        }
                    }
                }
                tempVideoArr.append(wrapper)
            }

            dataArray[x].videoList = tempVideoArr
        }
        self.tblView.reloadData()
    }

    @objc func ViewAllLatestVideoClicked(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.Categories = ""
        appDelegate.CategoriesNames = ""
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
        vc.sort = "2"
        vc.order = "asc"
        vc.searchStr = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @objc func ViewAllCategoryVideoClicked(_ sender:UIButton){
        let wrapper = self.dataArray[sender.tag]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.Categories = "\(wrapper.id ?? 0)"
        appDelegate.CategoriesNames = wrapper.name
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "home") as! HomeVC
        vc.sort = "2"
        vc.order = "asc"
        vc.searchStr = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1 + self.dataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "latestVideoCell", for: indexPath as IndexPath) as! latestVideoCell
            cell.collectionView.tag = indexPath.row
            cell.collectionView.dataSource = self
            cell.collectionView.delegate = self
            cell.collectionView.reloadData()
            cell.viewAllBtn.addTarget(self, action: #selector(ViewAllLatestVideoClicked), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "latestVideoCategoryCell", for: indexPath as IndexPath) as! latestVideoCategoryCell

            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self

            let videoList = self.dataArray[indexPath.row - 1].videoList
            cell.collectionView.reloadData()
            cell.collectionView.tag = indexPath.row
            cell.CategoryLbl.text = self.dataArray[indexPath.row - 1].name
            cell.viewAllBtn.tag = indexPath.row - 1
            if(videoList.count > 2){
                cell.viewAllBtn.addTarget(self, action: #selector(ViewAllCategoryVideoClicked(_:)), for: .touchUpInside)
                cell.viewAllBtn.isHidden = false
            }
            else{
                cell.viewAllBtn.isHidden = true
            }
            cell.selectionStyle = .none
            return cell
        }

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if(collectionView.tag == 0){
            let wrapper = self.latestVideoArr[indexPath.row]
            let urldata = URL(string: wrapper.video_file_url)
            let player = AVPlayer(url:urldata!)
            playerController = AVPlayerViewController()
            playerController.player = player
            playerController.allowsPictureInPicturePlayback = true
            playerController.delegate = self
            let session = AVAudioSession.sharedInstance()
                   do{
                       // try session.setCategory(AVAudioSession.Category.playback)
                    try session.setCategory(.playAndRecord, options: [.defaultToSpeaker,.allowBluetoothA2DP,.allowAirPlay,.defaultToSpeaker])
                       try       session.setActive(true, options: .init())

                   }
                   catch{
                   }
            playerController.player?.play()
            self.present(playerController,animated:true,completion:nil)
        }
        else{
            let wrapper  = self.dataArray[collectionView.tag - 1]
            let tempWrapper  = wrapper.videoList
            let videoWrapper  = tempWrapper[indexPath.row]

            let urldata = URL(string: videoWrapper.video_file_url)
            let player = AVPlayer(url:urldata!)
            playerController = AVPlayerViewController()
            playerController.player = player
            playerController.allowsPictureInPicturePlayback = true
            playerController.delegate = self
            let session = AVAudioSession.sharedInstance()
            do{
                
            try session.setCategory(.playAndRecord, options: [.defaultToSpeaker,.allowBluetoothA2DP,.allowAirPlay])
            try       session.setActive(true, options: .init())

            }
            catch{

            }
            playerController.player?.play()
            self.present(playerController,animated:true,completion:nil)
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if(collectionView.tag == 0){
            return self.latestVideoArr.count
        }
        else{
            return self.dataArray[collectionView.tag - 1].videoList.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if(collectionView.tag == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: "LatestVideoCollectionViewCell"), for: indexPath) as! LatestVideoCollectionViewCell

            let wrapper = self.latestVideoArr[indexPath.row]
            cell.posterImg.contentMode = .scaleAspectFill
            cell.posterImg.sd_setImage(with: URL(string: wrapper.video_poster_image_url!), placeholderImage: UIImage(named: "child"))
            cell.nameLbl.text = wrapper.name
            cell.posterImg.clipsToBounds = true
            cell.contentView.layer.cornerRadius = 8.0
            cell.contentView.clipsToBounds = true

            cell.downloadBtn.tag = indexPath.row
            cell.downloadBtn.addTarget(self, action: #selector(downloadBtnClicked(_:)), for: .touchUpInside)
            cell.downloadBtn.isEnabled = true
            if(wrapper.isDownloaded == true){
                cell.downloadImgView.image = UIImage.init(named: "tick")
                cell.downloadBtn.isHidden = true
            }
            else{
                cell.downloadImgView.image = UIImage.init(named: "download")
                cell.downloadBtn.isHidden = false
            }

            cell.dropShadow(UIColor.lightGray)
         //   self.applyCurvedShadow(view: cell)

            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: "LatestVideoCollectionViewCell"), for: indexPath) as! LatestVideoCollectionViewCell

            let tempArr = self.dataArray[collectionView.tag - 1].videoList
            let wrapper = tempArr[indexPath.row]
            cell.posterImg.contentMode = .scaleAspectFill
            cell.posterImg.sd_setImage(with: URL(string: wrapper.video_poster_image_url!), placeholderImage: UIImage(named: "child"))
            cell.nameLbl.text = wrapper.name
            cell.posterImg.clipsToBounds = true
            cell.contentView.layer.cornerRadius = 8.0
            cell.contentView.clipsToBounds = true

            let section  = (collectionView.tag - 1) * 10
            let tag = section + indexPath.row
            cell.downloadBtn.tag = tag
            cell.downloadBtn.addTarget(self, action: #selector(downloadVideoClicked(_:)), for: .touchUpInside)
            cell.downloadBtn.isEnabled = true
            if(wrapper.isDownloaded == true){
                cell.downloadImgView.image = UIImage.init(named: "tick")
                cell.downloadBtn.isHidden = true
            }
            else{
                cell.downloadImgView.image = UIImage.init(named: "download")
                cell.downloadBtn.isHidden = false
            }

            cell.dropShadow(UIColor.lightGray)


            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if(collectionView.tag != 0){
            return CGSize(width: 130 , height: 138)
        }
        else{
            let size  = CGSize(
                width: ScreenWidth * cellPercentWidth,
                height: 138.0
            )
            return size
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {

    }

    func applyCurvedShadow(view: UIView) {
           let size = view.bounds.size
           let width = size.width
           let height = size.height
           let depth = CGFloat(11.0)
           let lessDepth = 0.8 * depth
           let curvyness = CGFloat(5)
           let radius = CGFloat(1)
           let path = UIBezierPath()

           // top left
        path.move(to: CGPoint(x: radius, y: height))

           // top right
        path.addLine(to: CGPoint(x: width - 2*radius, y: height))

           // bottom right + a little extra
        path.addLine(to: CGPoint(x: width - 2*radius, y: height + depth))

           // path to bottom left via curve
        path.addCurve(to: CGPoint(x: radius, y: height + depth),
               controlPoint1: CGPoint(x: width - curvyness, y: height + lessDepth - curvyness),
               controlPoint2: CGPoint(x: curvyness, y: height + lessDepth - curvyness))

           let layer = view.layer
        layer.shadowPath = path.cgPath
        layer.shadowColor = UIColor.black.cgColor
           layer.shadowOpacity = 0.3
           layer.shadowRadius = radius
           layer.shadowOffset = CGSize(width: 0, height: -3)
       }

    @objc func downloadBtnClicked(_ sender: UIButton){
        sender.alpha = 0.3
        sender.isEnabled = false
        self.showToast(message: "Download Started..")
        let wrapper = self.latestVideoArr[sender.tag]
        var urlstr = ""
        var directory = ""
        urlstr = wrapper.video_file_url
        directory = "video"

        let audioUrl = URL(string: urlstr)
        let dir = audioUrl?.lastPathComponent

        let request = URLRequest(url: URL(string: urlstr)!)

        self.downloadManager.showLocalNotificationOnBackgroundDownloadDone = true
        self.downloadManager.localNotificationText = "Downloads Are Completed."

        let _ = self.downloadManager.downloadFile(withRequest: request, inDirectory: directory, withName: dir, shouldDownloadInBackground: true, onProgress: { (progress) in
            let percentage = String(format: "%.1f %", (progress * 100))
            debugPrint("Background progress : \(percentage)")
        }) { [weak self] (error, url) in
            if let error = error {
                print("Error is \(error as NSError)")
            } else {
                if let _ = url {

                    DispatchQueue.main.async { [unowned self] in

                        if let _ = UserDefaults.standard.object(forKey: directory){
                            var decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: directory) as! Data) as! [MediaModel]
                            let mediaWrapper = MediaModel.init(id: wrapper.id, image: wrapper.video_poster_image_url, name: wrapper.name, file: dir!, category: wrapper.category, date: (self?.currentDate())!,type:directory)
                            decodedData.append(mediaWrapper)
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: decodedData)
                            UserDefaults.standard.set(encodedData, forKey: directory)
                            UserDefaults.standard.synchronize()

                        }
                        else{
                            let mediaWrapper = MediaModel.init(id: wrapper.id, image: wrapper.video_poster_image_url, name: wrapper.name, file: dir!, category: wrapper.category, date: (self?.currentDate())!,type:directory)
                            var mediaArray = [MediaModel]()
                            mediaArray.append(mediaWrapper)
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: mediaArray)
                            UserDefaults.standard.set(encodedData, forKey: directory)
                            UserDefaults.standard.synchronize()

                        }
                        self!.resetDownloadState()
                    }
                }
            }
        }


    }

    @objc func downloadVideoClicked(_ sender: UIButton){

        let tag = sender.tag
        let section  = tag / 10
        let row = tag % 10

        let tempArr = self.dataArray[section].videoList
        let wrapper = tempArr[row]

        sender.alpha = 0.3
        sender.isEnabled = false
        self.showToast(message: "Download Started..")
        var urlstr = ""
        var directory = ""
        urlstr = wrapper.video_file_url
        directory = "video"

        let audioUrl = URL(string: urlstr)
        let dir = audioUrl?.lastPathComponent

        let request = URLRequest(url: URL(string: urlstr)!)

        self.downloadManager.showLocalNotificationOnBackgroundDownloadDone = true
        self.downloadManager.localNotificationText = "Downloads Are Completed."

        let _ = self.downloadManager.downloadFile(withRequest: request, inDirectory: directory, withName: dir, shouldDownloadInBackground: true, onProgress: { (progress) in
            let percentage = String(format: "%.1f %", (progress * 100))
            debugPrint("Background progress : \(percentage)")
        }) { [weak self] (error, url) in
            if let error = error {
                print("Error is \(error as NSError)")
            } else {
                if let url = url {
                    print("Downloaded file's url is \(url.path)")
                    DispatchQueue.main.async { [unowned self] in

                        if let _ = UserDefaults.standard.object(forKey: directory){
                            var decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: directory) as! Data) as! [MediaModel]
                            let mediaWrapper = MediaModel.init(id: wrapper.id, image: wrapper.video_poster_image_url, name: wrapper.name, file: dir!, category: wrapper.category, date: (self?.currentDate())!,type:directory)
                            decodedData.append(mediaWrapper)
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: decodedData)
                            UserDefaults.standard.set(encodedData, forKey: directory)
                            UserDefaults.standard.synchronize()

                        }
                        else {
                            let mediaWrapper = MediaModel.init(id: wrapper.id, image: wrapper.video_poster_image_url, name: wrapper.name, file: dir!, category: wrapper.category, date: (self?.currentDate())!,type:directory)
                            var mediaArray = [MediaModel]()
                            mediaArray.append(mediaWrapper)
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: mediaArray)
                            UserDefaults.standard.set(encodedData, forKey: directory)
                            UserDefaults.standard.synchronize()

                        }
                        self!.resetDownloadState()
                    }
                }
            }
        }
    }

    func currentDate() -> String{
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: NSLocale.current.identifier)
        dateFormatter.dateFormat = "dd MMM YYYY"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        return dateString
    }


}

import CenteredCollectionView
class latestVideoCell: UITableViewCell {

    @IBOutlet var viewAllBtn : UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var centeredCollectionViewFlowLayout: CenteredCollectionViewFlowLayout!
    let cellPercentWidth: CGFloat = 0.7


    override func awakeFromNib() {
        super.awakeFromNib()

        // Get the reference to the CenteredCollectionViewFlowLayout (REQURED)
        centeredCollectionViewFlowLayout = (collectionView.collectionViewLayout as! CenteredCollectionViewFlowLayout)

        // Modify the collectionView's decelerationRate (REQURED)
        collectionView.decelerationRate = UIScrollView.DecelerationRate.fast

        // Configure the required item size (REQURED)
        centeredCollectionViewFlowLayout.itemSize = CGSize(
            width: ScreenWidth * cellPercentWidth,
            height: 148.0
        )

        // Configure the optional inter item spacing (OPTIONAL)
        centeredCollectionViewFlowLayout.minimumLineSpacing = 20
        self.perform(#selector(setSrollviewWithDelay), with: nil, afterDelay: 1.2)
    }

    @objc func setSrollviewWithDelay()
    {
        self.centeredCollectionViewFlowLayout.scrollToPage(index: 1, animated: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }

}

class latestVideoCategoryCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    var dataSource = [VideoListModel]()
    @IBOutlet var CategoryLbl : UILabel!
    @IBOutlet var viewAllBtn : UIButton!
    let margin: CGFloat = 10

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.collectionView.dataSource = self

        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

        flowLayout.minimumInteritemSpacing = 10
        flowLayout.minimumLineSpacing = 10
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {

    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: "LatestVideoCollectionViewCell"), for: indexPath) as! LatestVideoCollectionViewCell

        let wrapper = self.dataSource[indexPath.row]
        cell.posterImg.contentMode = .scaleAspectFill
        cell.posterImg.sd_setImage(with: URL(string: wrapper.video_poster_image_url!), placeholderImage: UIImage(named: "video-dummy"))
        cell.nameLbl.text = wrapper.name
        cell.posterImg.clipsToBounds = true
        cell.contentView.layer.cornerRadius = 8.0

        let tag = ((collectionView.tag - 1) * 10) + indexPath.row
        cell.downloadBtn.tag = tag
        //   cell.downloadBtn.addTarget(self, action: #selector(downloadVideoClicked(_:)), for: .touchUpInside)
        cell.downloadBtn.isEnabled = true
        if(wrapper.isDownloaded == true){
            cell.downloadImgView.image = UIImage.init(named: "tick")
            cell.downloadBtn.isHidden = true
        }
        else{
            cell.downloadImgView.image = UIImage.init(named: "download")
            cell.downloadBtn.isHidden = false
        }


        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130 , height: 148)

    }

}
