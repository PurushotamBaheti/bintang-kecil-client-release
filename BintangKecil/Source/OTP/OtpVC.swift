//
//  OtpVC.swift
//  BintangKecil
//
//  Created by Owebest on 06/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

import FirebaseCore
import FirebaseAuth

class OtpVC: UIViewController,OTPDelegate {

    @IBOutlet weak var navHeight: NSLayoutConstraint!
    @IBOutlet weak var otpContainerView: UIView!
    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var resentBtn: UIButton!
    @IBOutlet weak var troubleShootBtn: UIButton!
    @IBOutlet weak var timerBtn: UIButton!
    let otpStackView = OTPStackView()
    
    var verificationID : String? = nil
    var name = ""
    var mobile = ""
    var email = ""
    var password = ""
    var socialId = ""
    var isFromSignUp : Bool = false
    var FinalOTP = ""
    var signupType = 0
    var isOTPThroughMail : Bool = false
    var validation = Validation()
    var timeLimit = 20

    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseNavBar()
        self.customiseUI()

        // Do any additional setup after loading the view.
        sendOTP()
        
    }
    
    func sendOTP(){
        if mobile != ""{
            Auth.auth().settings?.isAppVerificationDisabledForTesting = false
            PhoneAuthProvider.provider().verifyPhoneNumber(mobile, uiDelegate: nil, completion: {verificationID, error in
                if(error != nil){
                    return
                }
                else{
                    self.verificationID = verificationID
                }
            })
        }
    }
    @IBAction func VerifyBtnClicked(){

        if(self.isOTPThroughMail == false){

            if verificationID != nil{
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID!, verificationCode: FinalOTP)

                Auth.auth().signIn(with: credential, completion: {authData, error in
                    if error != nil{
                        print(error.debugDescription)
                        self.showToast(message: error!.localizedDescription)
                    }
                    else{
                        if(self.isFromSignUp == true){
                            self.callSignupApi()
                        }
                        else{
                            let mobileStr = self.mobile.replacingOccurrences(of:"+", with:"")
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "setpassword") as! SetPasswordVC
                            vc.userMobileNumber = mobileStr
                            self.navigationController?.pushViewController(vc, animated: false)

                        }
                    }

                })
            }

        }
        else {

            self.callVerifyOTPApi()

        }

    }

    @IBAction func TroubleshootBtnClicked(){
        self.isOTPThroughMail = true
        self.callOTPApi()
    }

    public func alertWithTextField(title: String? = nil, message: String? = nil, placeholder: String? = nil, completion: @escaping ((String) -> Void) = { _ in }) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField() { newTextField in
            newTextField.placeholder = placeholder
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in completion("") })
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
            if
                let textFields = alert.textFields,
                let tf = textFields.first,
                let result = tf.text
            { completion(result) }
            else
            { completion("") }
        })
        navigationController?.present(alert, animated: true)
    }
    

    func customiseUI(){
        otpContainerView.addSubview(otpStackView)
        otpStackView.delegate = self
        otpStackView.heightAnchor.constraint(equalTo: otpContainerView.heightAnchor).isActive = true
        otpStackView.centerXAnchor.constraint(equalTo: otpContainerView.centerXAnchor).isActive = true
        otpStackView.centerYAnchor.constraint(equalTo: otpContainerView.centerYAnchor).isActive = true
        verifyBtn.isEnabled = false
        verifyBtn.alpha = 0.45
        resentBtn.isEnabled = false
        resentBtn.alpha = 0.45
        troubleShootBtn.isEnabled = true
        troubleShootBtn.alpha = 1.0
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in

            let str = String(format:"%02d",self.timeLimit)
            let value = "00:"+str
            self.timerBtn.setTitle(value, for: .normal)
            self.timeLimit = self.timeLimit - 1
            if self.timeLimit == 0 {
                timer.invalidate()
                self.timerBtn.setTitle("", for: .normal)
            }
        }


        self.perform(#selector(enableResendBtn), with: nil, afterDelay: 20)

    }
    func setTimerValue(){
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            let str = String(format:"%02d",self.timeLimit)
            let value = "00:"+str
            self.timerBtn.setTitle(value, for: .normal)
            self.timeLimit = self.timeLimit - 1
            if self.timeLimit == 0 {
                timer.invalidate()
                self.timerBtn.setTitle("", for: .normal)
            }
        }
    }

    @objc func enableResendBtn(){
        resentBtn.isEnabled = true
        resentBtn.alpha = 1.0
        troubleShootBtn.isEnabled = true
        troubleShootBtn.alpha = 1.0
    }

    @IBAction func ResentOTPClicked(){
        resentBtn.isEnabled = false
        resentBtn.alpha = 0.45
        self.perform(#selector(enableResendBtn), with: nil, afterDelay: 20)
        self.timeLimit = 20
        self.setTimerValue()
        sendOTP()
    }

    func customiseNavBar(){
        if(ScreenHeight > 811){
            navHeight.constant = 84
        }
    }

    func didChangeValidity(isValid: Bool) {
        if (isValid == true){
            verifyBtn.isEnabled = true
            verifyBtn.alpha = 1.0
            FinalOTP = otpStackView.getOTP()
        }
    }

    @IBAction func goBack(sender: AnyObject) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

    func callSignupApi(){
        var token = ""
        if let tokenstr = UserDefaults.standard.value(forKey: "fcmId"){
            token = tokenstr as! String
        }

        var parameter = [String:Any]()
        parameter["type"] = signupType
        parameter["email"] = self.email
        let mobileStr = self.mobile.replacingOccurrences(of:"+", with:"")
        parameter["mobile"] = mobileStr
        parameter["password"] = self.password
        parameter["osType"] = "ios"
        parameter["macId"] = DEVICE_ID
        parameter["tokenId"] = token
        parameter["name"] = self.name
        parameter["social_id"] = socialId

        showLoader()
       // postRequest(strUrl: "register", param: parameter, success: { (json) in
         postRequest(strUrl: "ios-register", param: parameter, success: { (json) in
            print(json)
            self.hideLoader()
            if json["status"].stringValue == "200"{

                let userWrapper = UserModal.init(id: json["data"]["id"].intValue, image: json["data"]["image"].stringValue, email: json["data"]["email"].stringValue, image_thumbnail: json["data"]["image_thumbnail"].stringValue, accessToken: json["data"]["accessToken"].stringValue, user_code: json["data"]["user_code"].stringValue, name: json["data"]["name"].stringValue, mobile: json["data"]["mobile"].stringValue)

                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userWrapper)
                UserDefaults.standard.set(encodedData, forKey: "userdata")

                UserDefaults.standard.setLogin(true)
                UserDefaults.standard.synchronize()
                self.view.endEditing(true)
                let window = UIApplication.shared.keyWindow
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let dashboard = storyBoard.instantiateViewController(withIdentifier: "tabbar")
                window?.rootViewController = dashboard
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func callOTPApi() {
        var parameter = [String:Any]()
        parameter["email"] = self.email
        if(self.isFromSignUp == true){
         parameter["event"] = 1
        }
        else{
          parameter["event"] = 4
        }
        
        showLoader()
        postRequest(strUrl: "send-otp-on-email", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {

                let message = "An OTP has been sent to " + self.email + " .Please check your Email for the same."
                self.showAlert("Bintang Kecil", message: message)

            }
            else {
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func callVerifyOTPApi() {

        self.view.endEditing(true)
        var parameter = [String:Any]()
        parameter["email"] = self.email

        if(self.isFromSignUp == true){
         parameter["event"] = 1
        }
        else{
          parameter["event"] = 4
        }
        parameter["otp"] = FinalOTP

        showLoader()
        postRequest(strUrl: "verify-email-otp", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {

                if(self.isFromSignUp == true){
                    self.callSignupApi()
                }
                else{
                    let mobileStr = self.mobile.replacingOccurrences(of:"+", with:"")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "setpassword") as! SetPasswordVC
                    vc.userMobileNumber = mobileStr
                    self.navigationController?.pushViewController(vc, animated: false)

                }

            }
            else {
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

}
