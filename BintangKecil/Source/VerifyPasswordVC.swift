//
//  VerifyPasswordVC.swift
//  BintangKecil
//
//  Created by Purushotam on 25/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
protocol verifyPasswordDelegate{
    func PasswordVerifiedSuccessfully()
}

class VerifyPasswordVC: UIViewController {

    var delegate : verifyPasswordDelegate?
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var showPasswordBtn: UIButton!
    var isShowPassword : Bool = false
    var validation = Validation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    @IBAction func ShowPasswordClicked(){
        if(isShowPassword == false){
            isShowPassword = true
            self.passwordTxtField.isSecureTextEntry = false
            showPasswordBtn.setImage(UIImage.init(named:"password_hide_black"), for: .normal)
        }
        else{
            isShowPassword = false
            self.passwordTxtField.isSecureTextEntry = true
            showPasswordBtn.setImage(UIImage.init(named:"password_visible_black"), for: .normal)
        }
    }

    @IBAction func OKBtnClicked(){
        if(self.ValidateTextFields() == true){
            self.callVerifyPassswordApi()
        }
    }

    @IBAction func CancelBtnClicked(){
        self.dismiss(animated: true, completion: nil)
    }

    func ValidateTextFields() -> Bool{
        if((passwordTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_PASSWORD)
            return false
        }
        else if(validation.validatePassword(password:self.passwordTxtField.text!) == false){
            self.showToast(message: MessageString.INVALID_PASSWORD)
            return false
        }
        return true
    }

    func callVerifyPassswordApi(){
        var parameter = [String:Any]()
        parameter["password"] = self.passwordTxtField.text!

        showLoader()
        postRequest(strUrl: "verify-password", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                self.dismiss(animated: true, completion: {
                    self.delegate?.PasswordVerifiedSuccessfully()
                })

            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
}
