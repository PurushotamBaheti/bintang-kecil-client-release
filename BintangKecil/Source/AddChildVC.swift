//
//  AddChildVC.swift
//  BintangKecil
//
//  Created by Purushotam on 25/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

protocol AddChildDelegate{
    func childAdddedSuccessfully()
}

class AddChildVC: BaseViewController {

    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var dateofBirthTxtField: UITextField!
    let datePickerView: UIDatePicker! = UIDatePicker()
    var delegate : AddChildDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomiseUI()
        createDatePicker()
    }

    func CustomiseUI(){
        nameTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        dateofBirthTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
    }

    @IBAction func DateofBirthClicked(){

    }

    @IBAction func AddChildClicked(){
        if(self.ValidateTextFields() == true){
            self.CallAddChildApi()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    func createDatePicker(){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        dateofBirthTxtField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(donePressed))
        dateofBirthTxtField.inputView = datePickerView
        datePickerView.datePickerMode = .date
        self.datePickerView.maximumDate = Date()

    }

    @objc func donePressed(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        dateofBirthTxtField.text = formatter.string(from: datePickerView.date)
        self.view.endEditing(true)
    }

    func ValidateTextFields() -> Bool{
        if((nameTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_CHILD_NAME)
            return false
        }
        else if((dateofBirthTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
            self.showToast(message: MessageString.EMPTY_DOB)
            return false
        }
        return true
    }

    func CallAddChildApi(){
        var parameter = [String:Any]()
        parameter["name"] = self.nameTxtField.text!
        parameter["dob"] = self.dateofBirthTxtField.text!

        let TotalArray = [[String : Any]]()

        showLoader()

        uploadData(strUrl: "children", arrImage: TotalArray, param: parameter, success: { (json) in
            if json["status"].stringValue == "200"{
                self.showToast(message: "Child added successfully!")
                self.delegate?.childAdddedSuccessfully()
                self.navigationController?.popViewController(animated: true)

            }
            else if(json["status"].stringValue == "201" && json["message"].stringValue == "User does not have active subscription plan!"){
                self.showSubscriptonAlert()
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }
            self.hideLoader()

        }, failure: {
            (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg as! String)
        })
    }

    func showSubscriptonAlert(){
        let dialogMessage = UIAlertController(title: "Subscription Alert!", message: "You cannot add any child  because you have not bought any subscription with us.Please subscribe now to add child and get them access to activities.Do you want to purchase subscription now?", preferredStyle: .alert)

        // Create OK button with action handler
        let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in

            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPlanVC") as! PaymentPlanVC
            vc.isFromAddChild = true
            self.navigationController?.pushViewController(vc, animated: true)


        })

        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
        }

        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)

        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }

}
