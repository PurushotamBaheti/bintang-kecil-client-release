//
//  SetPasswordVC.swift
//  BintangKecil
//
//  Created by Owebest on 13/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class SetPasswordVC: BaseViewController {

    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var confirmPasswordTxtField: UITextField!
    @IBOutlet weak var showNewPasswordBtn: UIButton!
    @IBOutlet weak var showConfirmPasswordBtn: UIButton!

    var validation = Validation()
    var isShowNewPassword : Bool = false
    var isShowConfirmPassword : Bool = false
    var userMobileNumber = ""
    var isFromChangePassword : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        confirmPasswordTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    @IBAction func ShowNewPasswordClicked() {
        if(isShowNewPassword == false) {
            isShowNewPassword = true
            self.passwordTxtField.isSecureTextEntry = false
            showNewPasswordBtn.setImage(UIImage.init(named:"password_hide_black"), for: .normal)
        }
        else {
            isShowNewPassword = false
            self.passwordTxtField.isSecureTextEntry = true
            showNewPasswordBtn.setImage(UIImage.init(named:"password_visible_black"), for: .normal)
        }
    }

    @IBAction func ShowConfirmPasswordClicked() {
        if(isShowConfirmPassword == false) {
            isShowConfirmPassword = true
            self.confirmPasswordTxtField.isSecureTextEntry = false
            showConfirmPasswordBtn.setImage(UIImage.init(named:"password_hide_black"), for: .normal)
        }
        else {
            isShowConfirmPassword = false
            self.confirmPasswordTxtField.isSecureTextEntry = true
            showConfirmPasswordBtn.setImage(UIImage.init(named:"password_visible_black"), for: .normal)
        }
    }

    @IBAction func SubmitBtnClicked() {
        self.view.endEditing(true)
        if(self.ValidateTextFields() == true) {
            callResetApi()
        }
    }

    func ValidateTextFields() -> Bool {
        if((passwordTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_NEW_PASSWORD)
            return false
        }
        else if(validation.validatePassword(password:self.passwordTxtField.text!) == false) {
            self.showToast(message: MessageString.INVALID_PASSWORD)
            return false
        }
        else if((confirmPasswordTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_CONFIRM_PASSWORD)
            return false
        }
        else if(passwordTxtField.text != confirmPasswordTxtField.text) {
            self.showToast(message: MessageString.MISMATCH_NEW_CONFIRM_PASSWORD)
            return false
        }
        return true
    }

    func callResetApi() {
        var parameter = [String:Any]()
        parameter["mobile"] = self.userMobileNumber
        parameter["password"] = passwordTxtField.text!
        parameter["confirm_password"] = confirmPasswordTxtField.text!

        showLoader()
        postRequest(strUrl: "reset-password", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200" {
                if(self.isFromChangePassword == false) {
                    self.showToast(message:"Password reset successfully.Please login again to verify.")
                    self.navigationController?.popToRootViewController(animated:true)
                }
                else {
                    let alertController = UIAlertController(title: "Alert", message: "Password set successfully.Please login again to verify.", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                        UserDefaults.standard.setLogin(false)
                        UserDefaults.standard.synchronize()
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let dashboard = storyBoard.instantiateViewController(withIdentifier: "loginnav")
                        appDelegate.window?.rootViewController = dashboard
                    }

                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion: nil)

                }
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
    
}
