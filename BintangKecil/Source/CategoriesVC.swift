//
//  CategoriesVC.swift
//  BintangKecil
//
//  Created by Purushotam on 26/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

protocol categoriesAddDelegate{
    func categoriesUpdatedSuccessfully()
}

class CategoriesVC: BaseViewController {

    var type = 0
    var categoryScroll = UIScrollView()
    var dataArray = [CategoryModel]()
    var delegate : categoriesAddDelegate?
    var categoryArray = [String]()
    var category_type = 0 // 0 for Media category HomePage

    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.tabBarController?.tabBar.isHidden = true

        if category_type == 1{
            if(appDelegate.Categories.count > 0){
                let categories = appDelegate.Categories
                categoryArray = categories.components(separatedBy:",")
            }
            self.callCategoriesListApi()
        }
        else if category_type == 2{
            if(appDelegate.KaraokeNames.count > 0){
                let categories = appDelegate.KaraokeId
                categoryArray = categories.components(separatedBy:",")
            }
            self.fetchKaraokeCategory()
        }
        self.createScrollUI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    func createScrollUI(){
        var ycoordinate : CGFloat = 64
        if(ScreenHeight > 811){
            ycoordinate = 84
        }
        categoryScroll.frame = CGRect.init(x:20, y: ycoordinate + 20, width: ScreenWidth-40, height: ScreenHeight - 40 - ycoordinate)
        categoryScroll.backgroundColor = UIColor.clear
        self.view.addSubview(categoryScroll)
    }

    func callCategoriesListApi(){
        var parameter = [String:Any]()
        parameter["type"] = self.type

        showLoader()
        postRequest(strUrl: "media-category-list", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                let dataArr = json["data"].arrayValue
                if(dataArr.count > 0){
                    for ind in 0...dataArr.count - 1{
                        let wrapper = CategoryModel.init(fromJson: dataArr[ind])
                        let catId = "\(wrapper.id ?? 0)"
                        if(self.categoryArray.contains(catId)){
                            wrapper.isSelected = true
                        }
                        self.dataArray.append(wrapper)
                    }
                    self.AddCategoriesinUI()
                }
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }
    
    func fetchKaraokeCategory(){
        let parameter = [String:Any]()

        showLoader()
        postRequest(strUrl: "karaoke-category-list", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"{
                let dataArr = json["data"].arrayValue
                if(dataArr.count > 0){
                    for ind in 0...dataArr.count - 1{
                        let wrapper = CategoryModel.init(fromJson: dataArr[ind])
                        let catId = "\(wrapper.id ?? 0)"
                        if(self.categoryArray.contains(catId)){
                            wrapper.isSelected = true
                        }
                        self.dataArray.append(wrapper)
                    }
                    self.AddCategoriesinUI()
                }
            }
            else{
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.hideLoader()
            self.ShowAlert(message: msg)
        }
    }

    func AddCategoriesinUI(){
        let subViews = self.categoryScroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }

        var xcoordinate : CGFloat = 0
        var yCoordinate: CGFloat = 0
        let gap : CGFloat = 16

        for ind in 0...self.dataArray.count - 1{
            let wrapper = self.dataArray[ind]
            let size = wrapper.name.size(withAttributes:[.font: UIFont.init(name:"Noteworthy-Bold", size: 15.0)!])
            let textsize = size.width  + 26 + 48
            if((xcoordinate + textsize + gap) > self.categoryScroll.frame.width){
                yCoordinate = yCoordinate + 40 + 10
                xcoordinate = 0
            }

            let boxView = UIView()
            boxView.frame = CGRect.init(x:xcoordinate, y: yCoordinate, width: textsize, height: 38.0)
          //  boxView.backgroundColor = UIColor.white
           // boxView.layer.borderWidth = 0.5
         //   boxView.layer.borderColor = UIColor.lightGray.cgColor
            boxView.backgroundColor = UIColor.init(red:237.0/255.0, green: 103.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            boxView.layer.cornerRadius = 19
            self.categoryScroll.addSubview(boxView)

            let nameLbl = UILabel()
            nameLbl.frame = CGRect.init(x:0, y: 4, width: textsize, height: 30)
            nameLbl.text = wrapper.name!
            nameLbl.textColor = UIColor.white
            nameLbl.font = UIFont.init(name:"Noteworthy-Bold", size: 15.0)
            nameLbl.backgroundColor = UIColor.clear
            nameLbl.textAlignment = .center
            boxView.addSubview(nameLbl)

            let innerImgView = UIImageView()
            innerImgView.frame = CGRect.init(x:nameLbl.frame.size.width - 36, y: 3, width: 32, height: 32)
            innerImgView.image = UIImage.init(named:"tick")
            innerImgView.contentMode = .scaleAspectFit
            boxView.addSubview(innerImgView)
            if(wrapper.isSelected == false){
                nameLbl.frame = CGRect.init(x:0, y: 4, width: textsize, height: 30)
                innerImgView.isHidden = true
            }

            let btn = UIButton()
            btn.frame = CGRect.init(x:0, y: 0, width: textsize, height: 38.0)
            btn.tag = ind
            btn.addTarget(self, action: #selector(SelectCategoriesBtnAction(_:)), for: .touchUpInside)
            boxView.addSubview(btn)
            xcoordinate = xcoordinate + textsize + gap

        }
        self.categoryScroll.contentSize = CGSize.init(width:0, height: yCoordinate + 50)
    }

    @objc func SelectCategoriesBtnAction(_ sender: UIButton){
        let wrapper = self.dataArray[sender.tag]
        if(wrapper.isSelected == false){
            wrapper.isSelected = true
        }
        else{
            wrapper.isSelected = false
        }
        self.dataArray[sender.tag] = wrapper
        self.AddCategoriesinUI()
    }

    @IBAction func ResetBtnClicked(){
        let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to Reset all filters", preferredStyle: .alert)

        // Create OK button with action handler
        let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            if self.category_type == 1{
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.Categories = ""
                appDelegate.CategoriesNames = ""
                self.delegate?.categoriesUpdatedSuccessfully()
                self.navigationController?.popViewController(animated: true)
            }
            else{
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.KaraokeNames = ""
                appDelegate.KaraokeId = ""
                self.delegate?.categoriesUpdatedSuccessfully()
                self.navigationController?.popViewController(animated: true)
            }
        })

        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
        }
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)

        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }

    @IBAction func DoneBtnClicked(){
        if(self.dataArray.count > 0){
            var str  = ""
            var str2 = ""
            for ind in 0...self.dataArray.count - 1{
                let wrapper = self.dataArray[ind]
                if(wrapper.isSelected == true){
                    str = str + "\(wrapper.id ?? 0)" + ","
                    str2 = str2 + wrapper.name! + ","
                }
            }
            if(str.count > 0){
                let trimmedstr = str.dropLast()
                let trimmedstr2 = str2.dropLast()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if(self.category_type == 2){
                    appDelegate.KaraokeId = String(trimmedstr)
                    appDelegate.KaraokeNames = String(trimmedstr2)
                }
                else{
                    appDelegate.Categories = String(trimmedstr)
                    appDelegate.CategoriesNames =  String(trimmedstr2)
                }
                self.delegate?.categoriesUpdatedSuccessfully()
                self.navigationController?.popViewController(animated: true)
            }
            else{
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if(self.category_type == 2){
                    appDelegate.KaraokeId  = ""
                    appDelegate.KaraokeNames = ""
                }
                else{
                    appDelegate.Categories = ""
                    appDelegate.CategoriesNames = ""
                }
                self.delegate?.categoriesUpdatedSuccessfully()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

}
