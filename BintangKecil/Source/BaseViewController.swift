//
//  BaseViewController.swift
//  BintangKecil
//
//  Created by Purushotam on 15/09/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    @IBOutlet weak var navHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseNavBar()

        // Do any additional setup after loading the view.
    }

    func customiseNavBar(){
        if(ScreenHeight > 811){
            navHeight.constant = 84
        }
    }

    @IBAction func backAction(sender: AnyObject) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.CategoriesNames = ""
        appDelegate.Categories = ""
        self.navigationController?.popViewController(animated: true)
    }

}
