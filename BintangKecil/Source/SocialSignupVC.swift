//
//  SignupVC.swift
//  BintangKecil
//
//  Created by Owebest on 04/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON
import SKCountryPicker

class SocialSignupVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var navHeight: NSLayoutConstraint!
    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var checkboxBtn: UIButton!
    var isTermsConditionsSelected : Bool = false

    @IBOutlet weak var logoTop: NSLayoutConstraint!
    @IBOutlet weak var centreViewTop: NSLayoutConstraint!
    @IBOutlet weak var logoHeight: NSLayoutConstraint!
    @IBOutlet weak var countryCodeTxtField: UITextField!

    var isShowPassword : Bool = false
    var validation = Validation()
    var socialId = ""
    var userName = ""
    var userEmail = ""
    var userMobile = ""
    var type = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.IntialiseUI()
        // Do any additional setup after loading the view.
    }

    @IBAction func CountryBtnClicked(){
      presentCountryPickerScene()

    }

    func IntialiseUI() {
        userNameTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        emailTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        mobileTxtField.placeholderColor(UIColor.init(red:0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        userNameTxtField.text = self.userName
        emailTxtField.text = self.userEmail
        mobileTxtField.text = self.userMobile

        if(ScreenHeight > 811) {
            navHeight.constant = 84
        }
        else if (ScreenHeight < 569){
            logoTop.constant = 30.5
            centreViewTop.constant = 25
            logoHeight.constant = 130.0

        }
    }


    @IBAction func SignUpClicked() {
        self.view.endEditing(true)
        if(self.ValidateTextFields() == true) {
            self.callCredentialsExistApi()
        }
    }

    @IBAction func PrivacyPolicyClicked() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
        vc.pageTitle = "Privacy Policy"
        vc.pageUrl = PRIVACY_URL
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func TermsConditionsClicked() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebPage") as! WebPage
        vc.pageTitle = "Our Terms"
        vc.pageUrl = TERMS_URL
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func termsConditionCheckBoxAction() {
        if(self.isTermsConditionsSelected == false) {
            self.isTermsConditionsSelected = true
            self.checkboxBtn.setImage(UIImage.init(named:"checkbox_selected"), for: .normal)
        }
        else{
            self.checkboxBtn.setImage(UIImage.init(named:"checkbox"), for: .normal)
            self.isTermsConditionsSelected = false
        }
    }

    func ValidateTextFields() -> Bool {
        if((userNameTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_NAME)
            return false
        }
        else if((emailTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_EMAILADDRESS)
            return false
        }
        else if(validation.validateEmailId(emailID: self.emailTxtField.text!) == false) {
            self.showToast(message: MessageString.INVALID_EMAIL)
            return false
        }
        else if((mobileTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            self.showToast(message: MessageString.EMPTY_MOBILE)
            return false
        }

            /*
             else if(mobileTxtField.text?.prefix(1) != "+")
             {
             self.showToast(message: MessageString.INVALID_MOBILE_FORMAT)
             return false
             }
             */

        else if(validation.validaPhoneNumber(phoneNumber: self.mobileTxtField.text!) == false) {
            self.showToast(message: MessageString.INVALID_MOBILE)
            return false
        }
        else if(self.isTermsConditionsSelected == false) {
            self.showToast(message: MessageString.SELECT_TERMS)
            return false
        }

        return true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    func callCredentialsExistApi() {
        var parameter = [String:Any]()
        parameter["email"] =  self.emailTxtField.text!
        var mobilenum = self.countryCodeTxtField.text! +  self.mobileTxtField.text!
        mobilenum = mobilenum.replacingOccurrences(of:"+", with: "")
        // mobilenum = mobilenum.replacingOccurrences(of:"-", with: "")
        parameter["mobile"] = mobilenum

        showLoader()
        postRequest(strUrl: "check-email-mobile-exist", param: parameter, success: { (json) in
            self.hideLoader()
            if json["status"].stringValue == "200"
            {
                let mobilenumb = self.countryCodeTxtField.text! + self.mobileTxtField.text!

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "otp") as! OtpVC
                vc.isFromSignUp = true
                vc.mobile = mobilenumb
                vc.socialId = self.socialId
                vc.name = self.userNameTxtField.text!
                vc.password = ""
                vc.email = self.emailTxtField.text!
                vc.signupType = self.type
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else {
                let message = json["message"].stringValue
                self.showToast(message: message)
            }

        }) { (msg) in
            self.ShowAlert(message: msg)
        }
    }

    @IBAction func BackBtnClicked() {
        self.navigationController?.popViewController(animated: true)
    }

}

/// MARK: - Private Methods
private extension SocialSignupVC {

    func presentCountryPickerScene(withSelectionControlEnabled selectionControlEnabled: Bool = true) {
        switch selectionControlEnabled {
        case true:
            // Present country picker with `Section Control` enabled
            _ = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
                guard let self = self else { return }
                self.countryCodeTxtField.text = country.dialingCode!
            }

        case false:
            _ = CountryPickerController.presentController(on: self) { [weak self] (country: Country) in
                guard let self = self else { return }
                self.countryCodeTxtField.text = country.dialingCode!
            }

        }
    }
}
