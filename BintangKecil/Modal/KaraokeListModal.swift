//
//  KaraokeListModal.swift
//  BintangKecil
//
//  Created by Devesh on 25/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON

class KaraokeListModal: NSObject {

    var id: Int?
    var tags: String?
    var fileURL: String?
    var category, lrcFile, title, file: String?
    var createdDateTime, filteredContent, fileName, content: String?
    var lrcFileURL: String?
    var lrcFileName: String?
    var original_file_url : String?

    init(id: Int?, tags: String?, fileURL: String?, category: String?, lrcFile: String?, title: String?, file: String?, createdDateTime: String?, filteredContent: String?, fileName: String?, content: String?, lrcFileURL: String?, lrcFileName: String?,original_file_url:String?) {
        self.id = id
        self.tags = tags
        self.fileURL = fileURL
        self.category = category
        self.lrcFile = lrcFile
        self.title = title
        self.file = file
        self.createdDateTime = createdDateTime
        self.filteredContent = filteredContent
        self.fileName = fileName
        self.content = content
        self.lrcFileURL = lrcFileURL
        self.lrcFileName = lrcFileName
        self.original_file_url = original_file_url
    }
}
