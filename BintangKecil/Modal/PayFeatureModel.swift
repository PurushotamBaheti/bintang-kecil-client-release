//
//  PayFeatureModel.swift
//  BintangKecil
//
//  Created by Purushotam on 25/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON

class PayFeatureModel: NSObject {
    var id : Int!
    var icon : String!
    var type : String!
    var value : String!
    var name : String!
    var slug : String!

    override init() {
        super.init()
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        icon = json["icon"].stringValue
        type = json["type"].stringValue
        if let val = json["value"].string{
            self.value = val
        }
        else{
            let val = json["value"].intValue
            self.value = "\(val)"
        }
        name = json["name"].stringValue
        slug = json["slug"].stringValue
    }
}

class PaymentPlanModel: NSObject {
    var id : Int!
    var amount : String!
    var desc : String?
    var features = [PayFeatureModel]()
    var currency_code : String!
    var name : String!
    var currency : String!

    override init() {
        super.init()
    }

    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        amount = json["amount"].stringValue
        desc = json["description"].stringValue
        currency_code = json["currency_code"].stringValue
        name = json["name"].stringValue
        currency = json["currency"].stringValue

        let dataJsonArray = json["features"].arrayValue
        if dataJsonArray.count > 0 {
            for ind in 0...dataJsonArray.count - 1{
                let wrapper = PayFeatureModel.init(fromJson: dataJsonArray[ind])
                if(wrapper.type == "integer"  && wrapper.value != "0"){
                    self.features.append(wrapper)
                }
                else if(wrapper.type == "boolean" && wrapper.value == "1"){
                    self.features.append(wrapper)
                }
            }
        }
    }
}

