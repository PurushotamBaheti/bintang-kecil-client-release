//
//  ForumModel.swift
//  BintangKecil
//
//  Created by Purushotam on 13/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON

class ForumModel: NSObject {

    var id : Int!
    var title : String!
    var created_at : String!
    var file_thumbnail_url : String!
    var file_url : String!
    var filtered_content : String!
    var content : String!
    var user_name : String!
    var user_id : Int!

    override init() {
        super.init()
    }

    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        user_id = json["user_id"].intValue
        title = json["title"].stringValue
        created_at = json["created_at"].stringValue
        file_thumbnail_url = json["file_thumbnail_url"].stringValue
        file_url = json["file_url"].stringValue
        filtered_content = json["filtered_content"].stringValue
        content = json["content"].stringValue
        user_name = json["user_name"].stringValue
    }
}
