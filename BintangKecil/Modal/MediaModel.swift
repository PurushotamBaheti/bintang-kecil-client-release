//
//  MediaModel.swift
//  BintangKecil
//
//  Created by Purushotam on 09/07/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class MediaModel: NSObject,NSCoding {

    var id : Int!
    var name : String!
    var file : String!
    var category : String!
    var date : String!
    var image : String?
    var type : String!

    init(id:Int,image:String?,name: String,file: String, category: String,date:String,type:String) {
        self.id = id
        self.name = name
        self.category = category
        self.image = image
        self.file = file
        self.date = date
        self.type = type
    }
    required init?(coder aDecoder: NSCoder){
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.image = aDecoder.decodeObject(forKey: "image") as? String
        self.file = aDecoder.decodeObject(forKey: "file") as? String
        self.category = aDecoder.decodeObject(forKey: "category") as? String
        self.date = aDecoder.decodeObject(forKey: "date") as? String
        self.type = aDecoder.decodeObject(forKey: "type") as? String
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.image, forKey: "image")
        aCoder.encode(self.file, forKey: "file")
        aCoder.encode(self.category, forKey: "category")
        aCoder.encode(self.date, forKey: "date")
        aCoder.encode(self.type, forKey: "type")

    }
}
