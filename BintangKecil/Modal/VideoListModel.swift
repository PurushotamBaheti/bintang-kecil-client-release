//
//  VideoListModel.swift
//  BintangKecil
//
//  Created by Purushotam on 07/09/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON

class VideoListModel: NSObject {

     var id : Int!
     var name : String!
     var category : String!
     var video_file_url : String!
     var video_poster_image_url : String!
     var isDownloaded: Bool!

     override init() {
         super.init()
     }
     init(fromJson json: JSON!){
               if json.isEmpty{
                   return
               }
     id = json["id"].intValue
     name = json["name"].stringValue
     category = json["category"].stringValue
     video_file_url = json["video_file_url"].stringValue
     video_poster_image_url = json["video_poster_image_url"].stringValue
     isDownloaded = false

    }
}
