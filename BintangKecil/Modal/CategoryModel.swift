//
//  CategoryModel.swift
//  BintangKecil
//
//  Created by Purushotam on 27/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryModel: NSObject {
    var id : Int!
    var name : String!
    var isSelected : Bool!

    override init() {
        super.init()
    }

    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        name = json["name"].stringValue
        isSelected = false

    }
}
