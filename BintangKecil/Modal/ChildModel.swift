//
//  ChildModel.swift
//  BintangKecil
//
//  Created by Purushotam on 25/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation

class ChildModel: NSObject,NSCoding {
    var id : Int!
    var name : String!
    var image_url : String?

    init(id:Int,name: String,image:String?) {
        self.id = id
        self.name = name
        self.image_url = image
    }
    required init?(coder aDecoder: NSCoder){
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.image_url = aDecoder.decodeObject(forKey: "image_url") as? String
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.image_url, forKey: "image_url")
    }
}
