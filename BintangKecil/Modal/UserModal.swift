//
//  UserModal.swift
//  BintangKecil
//
//  Created by Purushotam on 24/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation

class UserModal: NSObject,NSCoding {

        var id : Int!
        var image : String!
        var email : String!
        var image_thumbnail : String!
        var accessToken : String!
        var user_code : String!
        var name : String!
        var mobile : String!

        init(id:Int,image:String,email:String,image_thumbnail:String,accessToken : String,user_code : String,name: String, mobile: String) {
               self.id = id
               self.name = name
               self.mobile = mobile
               self.image = image
               self.email = email
               self.image_thumbnail = image_thumbnail
               self.accessToken = accessToken
               self.user_code = user_code

           }
           required init?(coder aDecoder: NSCoder){
            self.id = aDecoder.decodeObject(forKey: "id") as? Int
               self.name = aDecoder.decodeObject(forKey: "name") as? String
               self.mobile = aDecoder.decodeObject(forKey: "mobile") as? String
               self.image = aDecoder.decodeObject(forKey: "image") as? String
               self.email = aDecoder.decodeObject(forKey: "email") as? String
               self.image_thumbnail = aDecoder.decodeObject(forKey: "image_thumbnail") as? String
               self.accessToken = aDecoder.decodeObject(forKey: "accessToken") as? String
               self.user_code = aDecoder.decodeObject(forKey: "user_code") as? String

           }

           func encode(with aCoder: NSCoder) {
            aCoder.encode(self.id, forKey: "id")
            aCoder.encode(self.name, forKey: "name")
            aCoder.encode(self.mobile, forKey: "mobile")
            aCoder.encode(self.email, forKey: "email")
            aCoder.encode(self.image, forKey: "image")
            aCoder.encode(self.image_thumbnail, forKey: "image_thumbnail")
            aCoder.encode(self.accessToken, forKey: "accessToken")
            aCoder.encode(self.user_code, forKey: "user_code")

           }

}
