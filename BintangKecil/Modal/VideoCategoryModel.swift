//
//  VideoCategoryModel.swift
//  BintangKecil
//
//  Created by Purushotam on 08/09/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON

class VideoCategoryModel: NSObject {

    var id : Int!
    var name : String!
    var videoList = [VideoListModel]()

    override init() {
        super.init()
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        name = json["name"].stringValue
        let dataJsonArray = json["videos"].arrayValue
        if dataJsonArray.count > 0 {
            for ind in 0...dataJsonArray.count - 1{
                let wrapper = VideoListModel.init(fromJson: dataJsonArray[ind])
                if let _ = UserDefaults.standard.object(forKey: "video"){
                    let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "video") as! Data) as! [MediaModel]
                    for ind in 0...decodedData.count - 1{
                        let savedWrapper = decodedData[ind]
                        if(wrapper.id == savedWrapper.id){
                            wrapper.isDownloaded = true
                        }
                    }
                }
                self.videoList.append(wrapper)
            }
        }
    }

}
