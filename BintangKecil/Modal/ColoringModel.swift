//
//  ColoringModel.swift
//  BintangKecil
//
//  Created by Purushotam on 26/08/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON

class ColoringModel: NSObject {
    var id : Int!
    var title : String!
    var category : String!
    var file_url : String!
    var backgroundMusic : String?

    override init() {
        super.init()
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        title = json["title"].stringValue
        category = json["category"].stringValue
        file_url = json["file_url"].stringValue

        let backgroundMusicDict = json["background_music"].dictionaryValue
        if let music = backgroundMusicDict["file_url"]{
            self.backgroundMusic = music.stringValue
        }
        else{
            self.backgroundMusic = ""
        }
    }
}
