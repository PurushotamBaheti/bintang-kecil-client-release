//
//  NotificationModel.swift
//  BintangKecil
//
//  Created by Purushotam on 10/09/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationModel: NSObject {

    var id : Int!
    var title : String!
    var body : String!
    var time : String!
    var url : String?
    var type : Int!

    override init() {
        super.init()
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        title = json["title"].stringValue
        type = json["type"].intValue
        body = json["body"].stringValue
        time = json["time"].stringValue
        url = json["url"].stringValue
    }
}
