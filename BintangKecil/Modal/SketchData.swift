//
//  SketchData.swift
//  Croquipad
//
//  Created by Pankaj Dhiman on 30/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import Foundation

class SketchData {
    var identifier : String!
    var lastModifiedDate : Date!
    var imageUrl : String?
    var metaDataUrl : String?
    var parentFolderName : String?
    var info : [String : Any]?
    
    init(identifier: String, lastModifiedDate: Date) {
        self.identifier = identifier
        self.lastModifiedDate = lastModifiedDate
    }

}
