//
//  LayerData.swift
//  Croquipad
//
//  Created by Pankaj Dhiman on 12/17/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

struct LayerData {
    var name : String
    var view : UIView
    var isHidden = true

}
