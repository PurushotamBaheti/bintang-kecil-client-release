
//  MyButton.swift
//  PatientPics
//
//  Created by jai kalra on 09/08/19.
//  Copyright © 2019 OWeBest.com. All rights reserved.
//

import Foundation
import UIKit


class MyView: UIView {

    @IBInspectable var addBottomShadow:Bool = false{
        didSet{
            if addBottomShadow{
                self.layer.masksToBounds = false
                self.layer.shadowRadius = 4
                self.layer.shadowOpacity = 1
                self.layer.shadowColor = UIColor.gray.cgColor
                self.layer.shadowOffset = CGSize(width: 0 , height:2)
            }
        }
    }
    
    @IBInspectable var addShadow:Bool = false{
        didSet{
            if addShadow{
                dropShadow()
            }
        }
    }
    @IBInspectable var MakeCircle:Bool = false{
        didSet{
            if MakeCircle{
                self.layer.cornerRadius = self.frame.height / 2
            }
        }
    }

}
