//
//  ViewBottomShadow.swift
//  BintangKecil
//
//  Created by Purushotam on 22/06/20.
//  Copyright © 2020 Purushotam Baheti. All rights reserved.
//

import UIKit

class ViewBottomShadow: UIView {
      init() {
        super.init(frame: .zero)
        backgroundColor = .white
        layer.masksToBounds = false
        layer.shadowRadius = 2
        layer.shadowOpacity = 1
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 0 , height:2)
      }

      required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
      }

}
