//
//  GlobalObjects.swift
//  FitnessApp
//
//  Created by Ankit Bagda on 26/03/19.
//  Copyright © 2019 Ritesh Jain. All rights reserved.
//

import Foundation
import UIKit

//GlobalObjectes
//local

let BASE_URL = "http://bintang.betademo.net/api/v1/"
//let BASE_URL = "http://dev-bintang.betademo.net/api/v1/"
//let BASE_URL = "https://www.cpanelbintang.com/api/v1/"

let imageUrl = ""
let TERMS_URL = "https://www.cpanelbintang.com/page/terms-and-condition"
let PRIVACY_URL = "https://www.cpanelbintang.com/page/privacy-policy"
let ABOUT_US_URL = "https://www.cpanelbintang.com/page/about-us"
let FAQ_URL = "https://www.cpanelbintang.com/page/faq"


let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
let DEVICE_TYPE = "iphone"
let DEVICE_ID = UIDevice.current.identifierForVendor?.uuidString
let APP_THEME_COLOR = UIColor(displayP3Red: 0/255, green: 211/255, blue: 205/255, alpha: 1.0)
let GOOGLE_CLIENT_ID = "528352480551-sc3v75ms4ojdkimkkm47fecg77l5ds1f.apps.googleusercontent.com"
let TWITTER_CONSUMER_KEY = "H6sZoge5cwW4gG3moQgWkjjMd"
let TWITTER_CONSUMER_SECRET = "LDGwKx3TmRZuc5mG2dz1PU3AJjhFj2QuC5Zr9HhQuO0hFQijOi"
let ACCESS_TOKEN = "bintang-kecil-eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9"
let COUNTRYCODE = "+62"

//let LICENSE_KEY = "351d1724d042b6e68df1b23dec434cda4f49b45fe79da1b2fda0861c16018fe05a0bb2bab6a40668"
let LICENSE_KEY = "173f3a04d262799d4a9dd9bb6682c1ef30ad12edc6d0a9690dca6be91da69660326fc0730a210050"

let ScreenHeight = UIScreen.main.bounds.height
let ScreenWidth = UIScreen.main.bounds.width

//let ROLE_ID =

func AppGlobalFont(_ fontSize:CGFloat,isBold:Bool) -> UIFont {
    let fontName : String!
    fontName = (isBold) ? "iCiel Gotham-Medium" : "iCiel Gotham-Thin"
    return UIFont(name: fontName, size: fontSize)!
}

func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
    
    let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
    if JSONSerialization.isValidJSONObject(value) {
        do{
            let data = try JSONSerialization.data(withJSONObject: value, options: options)
            if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                return string as String
            }
        }catch {
            print("error")
        }
    }
    return ""
}

struct MessageString{
    static  let INTERNET_CONNECTION_MESSAGE = "Please check your internet connection!"
    static  let EMPTY_EMAIL_MOBILE = "Please enter your Mobile Number or Email Address."
    static  let EMPTY_PASSWORD = "Please enter your password."
    static  let INVALID_MOBILE = "Please enter valid Mobile Number."
    static  let INVALID_MOBILE_FORMAT = "Please enter valid Mobile Number followed by plus(+) sign."
    static  let INVALID_EMAIL = "Please enter valid Email Address."
    static  let INVALID_PASSWORD = "Password should be min 6 alphanumeric character with 1 upper case character."
    static  let EMPTY_NAME = "Please enter your Name."
    static  let EMPTY_EMAILADDRESS = "Please enter your Email Address."
    static  let EMPTY_MOBILE = "Please enter your Mobile Number."
    static  let EMPTY_NEW_PASSWORD = "Please enter your new password."
    static  let EMPTY_CONFIRM_PASSWORD = "Please enter your confirm password."
    static  let MISMATCH_NEW_CONFIRM_PASSWORD = "Password and confirm password should be same."
    static  let SELECT_TERMS = "Please accpet Terms and Condiations and Privacy Policy."
    static  let EMPTY_OLD_PASSWORD = "Please enter your Old Password."
    static  let EMPTY_IMAGE = "Please add your Profile Picture."
    static  let EMPTY_CHILD_NAME = "Please enter your Child Name."
    static  let EMPTY_DOB = "Please select your Child's Date of Birth."
    static  let EMPTY_FORUM_TITLE = "Please enter Title."
    static  let EMPTY_FORUM_DESCRIPTION = "Please enter Description."
    static  let EMPTY_PROMOCODE = "Please enter Promo Code."
    static  let NO_Activities_English_Msg = "Subscribe now and many fun activities are awaiting for you!."
    static  let NO_Activities_Bhasha_Msg = "Langganan dulu yuk Sobat, banyak aktivitas seru menantimu!."


}

