//
//  ApiManager.swift
//  Evolet
//
//  Created by Raghu on 19/07/19.
//  Copyright © 2019 cloud. All rights reserved.
//

import Foundation
import Alamofire
import Alamofire_SwiftyJSON
import SwiftyJSON
import UIKit

func getRequest(strUrl : String , success: @escaping (JSON)->Void , failure:@escaping (_ err:String)-> Void){
    
    guard Reachability.isConnectedToNetwork() else{
        failure("Please check your internet connection!")
        return
    }

    let url = URL(string: BASE_URL+strUrl)!
    var header = [String:String]()
    let login = UserDefaults.standard.getLogin()
    if login{
        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
        let bearerToken = "Bearer " + decodedData.accessToken
        header = ["Accesstoken":ACCESS_TOKEN,"Authorization":bearerToken,"Accept":"application/json"]
    }
    else{
        header = ["Accesstoken":ACCESS_TOKEN,"Accept":"application/json"]
    }
    
    Alamofire.request(url, method: .get, parameters: [:], headers: header).responseSwiftyJSON { (response) in
        if response.result.value != nil{
            switch response.result{
            case .success:
                success(response.result.value!)
            case .failure:
                failure("")
            }
        }
        else{
            failure("something went wrong".localizedLowercase)
        }
    }
}

func postRequest(strUrl : String , param :[String:Any] , success: @escaping (JSON)->Void , failure:@escaping (_ err:String)-> Void) {

    guard Reachability.isConnectedToNetwork() else{
        failure(MessageString.INTERNET_CONNECTION_MESSAGE)
        return
    }

    let url = URL(string: BASE_URL+strUrl)!
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    let login = UserDefaults.standard.getLogin()
    if login{

        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
        let bearerToken = "Bearer " + decodedData.accessToken
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(ACCESS_TOKEN, forHTTPHeaderField: "Accesstoken")
        request.setValue(bearerToken, forHTTPHeaderField: "Authorization")
    }
    else{
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(ACCESS_TOKEN, forHTTPHeaderField: "Accesstoken")
    }

    request.timeoutInterval = 90 //Second
    request.httpBody = try! JSONSerialization.data(withJSONObject: param, options: [])

    Alamofire.request(request).responseSwiftyJSON {
        response in
        if response.result.value != nil{
            switch response.result{
            case .success:
                success(response.result.value!)
            case .failure:
                failure("")
            }
        }
        else{
            failure("something went wrong".localizedLowercase)
        }
    }
}

func uploadData(strUrl : String ,arrImage : [[String:Any]], param :[String:Any] , success: @escaping (JSON)->Void , failure:@escaping (Error)-> Void) {

    let url = URL(string: BASE_URL+strUrl)!

    var header = [String:String]()
    let login = UserDefaults.standard.getLogin()
    if login{
        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
        let bearerToken = "Bearer " + decodedData.accessToken
        header = ["Accesstoken":ACCESS_TOKEN,"Authorization":bearerToken,"Accept":"application/json"]
    }
    else{
        header = ["Accesstoken":ACCESS_TOKEN,"Accept":"application/json","Accept":"application/json"]
    }

    Alamofire.upload(multipartFormData: { multipartFormData in
        // import image to request
        for imageData in arrImage {

            if let img = imageData["img"] as? UIImage{
                let key = imageData["key"] //image
                let data = img.jpegData(compressionQuality: 0.5)
                if data != nil{
                    multipartFormData.append(data!, withName: key as! String, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                }
            }

            if let doc = imageData["video"] as? Data{
                multipartFormData.append(doc, withName: "file" , fileName: "\(Date().timeIntervalSince1970).mp4", mimeType: "mp4")
            }
        }
        for (key, value) in param {
            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
        }
    }, to: url,headers:header,

       encodingCompletion: { encodingResult in
        switch encodingResult {
        case .success(let upload, _, _):
            upload.responseSwiftyJSON(completionHandler: { (response) in
                success(response.result.value!)
            })
        case .failure(let error):
            print(error)
        }
    })
}


func uploadColorData(strUrl : String ,arrImage : [[String:Any]], param :[String:Any] , success: @escaping (JSON)->Void , failure:@escaping (Error)-> Void) {

    let url = URL(string: BASE_URL+strUrl)!

    var header = [String:String]()
    let login = UserDefaults.standard.getLogin()
    if login{

        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userdata") as! Data) as! UserModal
        let bearerToken = "Bearer " + decodedData.accessToken
        header = ["Accesstoken":ACCESS_TOKEN,"Authorization":bearerToken,"Accept":"application/json"]
    }
    else{
        header = ["Accesstoken":ACCESS_TOKEN,"Accept":"application/json","Accept":"application/json"]
    }

    Alamofire.upload(multipartFormData: { multipartFormData in
        // import image to request
        for imageData in arrImage {

            if let img = imageData["img"] as? UIImage{
                let key = imageData["key"] //image
                let data = img.jpegData(compressionQuality: 1.0)
                if data != nil{
                    multipartFormData.append(data!, withName: key as! String, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                }
            }

            if let doc = imageData["video"] as? Data{
                multipartFormData.append(doc, withName: "file" , fileName: "\(Date().timeIntervalSince1970).mp4", mimeType: "mp4")
            }
        }
        for (key, value) in param {
            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
        }
    }, to: url,headers:header,

       encodingCompletion: { encodingResult in
        switch encodingResult {
        case .success(let upload, _, _):
            upload.responseSwiftyJSON(completionHandler: { (response) in
                success(response.result.value!)
            })
        case .failure(let error):
            print(error)
        }

    })
}

func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> Void){
    let documentsUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!

    let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

    if FileManager().fileExists(atPath: destinationUrl.path){
        completion(destinationUrl.path, nil)
    }
    else{
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request, completionHandler:{
            data, response, error in
            if error == nil {
                if let response = response as? HTTPURLResponse {
                    if response.statusCode == 200 {
                        if let data = data{
                            if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic){
                                completion(destinationUrl.path, error)
                            }
                            else{
                                completion(destinationUrl.path, error)
                            }
                        }
                        else {
                            completion(destinationUrl.path, error)
                        }
                    }
                }
            }
            else{
                completion(destinationUrl.path, error)
            }
        })
        task.resume()
    }
}
