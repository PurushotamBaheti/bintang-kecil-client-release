//
//  BrushBrush.swift
//  DrawKit
//
//  Created by Pankaj Dhiman on 17/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

@IBDesignable
class BrushButton : UIButton {
    
    // brush properties
    var color: UIColor!
    var thickness: CGFloat!
    var opacity: CGFloat = 0.0
    var alphaa: CGFloat!
    var type : DrawingTool!

    var image: UIImage!
    var diameter: CGFloat!
 
    override var isSelected: Bool {
        willSet(selectedValue) {
            
            super.isSelected = selectedValue
            
            self.alpha = self.isSelected ? 1.0 : 0.7
        }
    }
    
    override var isEnabled: Bool {
        willSet(enabledValue) {
            super.isEnabled = enabledValue
            
            // if button is disabled, selected color should be changed to clear color
            let selected = self.isSelected
            self.isSelected = selected
        }
    }
    
    
    // MARK: - Public Methods

    init(diameter: CGFloat, image: UIImage, thickness: CGFloat, color: UIColor, alpha:CGFloat,tool: DrawingTool) {
        super.init(frame: CGRect.init(x: 0, y: 0, width: diameter, height: diameter))
        self.initialize(diameter, image: image, thickness: thickness, color:color, alpha:alpha, tool:tool)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    internal func update(_ color: UIColor) {
        self.color = color
        self.isSelected = super.isSelected
        
        self.backgroundColor = color.withAlphaComponent(self.opacity)
    }
    
    // MARK: - Private Methods
 
    fileprivate func initialize(_ diameter: CGFloat, image: UIImage, thickness: CGFloat, color: UIColor, alpha: CGFloat, tool: DrawingTool) {
       
        self.image = image
        self.color = color
        self.thickness = thickness
        self.diameter = diameter
        self.alphaa = alpha
        self.type = tool
        
        self.setImage(image, for: .normal)
        self.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.backgroundColor = color.withAlphaComponent(opacity)
        self.showsTouchWhenHighlighted = true
        self.alpha = self.isSelected ? 1.0 : 0.7
    }
    
    
    fileprivate func image(_ name: String) -> UIImage? {
        let podBundle = Bundle(for: self.classForCoder)
        if let bundleURL = podBundle.url(forResource: "NXDrawKit", withExtension: "bundle") {
            if let bundle = Bundle(url: bundleURL) {
                let image = UIImage(named: name, in: bundle, compatibleWith: nil)
                return image
            }
        }
        return nil
    }

}
