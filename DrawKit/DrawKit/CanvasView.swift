//
//  CanvasView.swift
//  ZoomSample
//
//  Created by Pankaj Dhiman on 29/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit


open class CanvasView: UIView {

    
    // MARK: Inits
    
    var dirtyRectViews: [UIView]!

    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.drawsAsynchronously = true
        
        let dirtyRectView = { () -> UIView in
            let view = UIView(frame: CGRect(x: -10, y: -10, width: 0, height: 0))
           // view.layer.borderColor = UIColor.red.cgColor
            view.layer.borderWidth = 0.5
            view.isUserInteractionEnabled = false
            view.isHidden = true
            self.addSubview(view)
            return view
        }
        dirtyRectViews = [dirtyRectView(), dirtyRectView()]
        
        self.backgroundColor = UIColor.blue

    }
    
    public required  init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
