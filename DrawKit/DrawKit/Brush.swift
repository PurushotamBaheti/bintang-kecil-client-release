//
//  Brush.swift
//  DrawKit
//
//  Created by Pankaj Dhiman on 13/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

@objc protocol paintBrushDelegate{
    @objc optional func setPaintBrush(_ isPaintBrush:Bool)
}

open class Brush : NSObject{
    
    open var color: UIColor = UIColor.black {
        
        willSet(colorValue) {
            color = colorValue
            isEraser = color.isEqual(UIColor.clear)
            blendMode = isEraser ? .clear : .normal
        }
    }

    open var isPaintBrush : Bool = false
        {
        willSet(paintBrush)
        {
            isPaintBrush = paintBrush
        }
    }
    
    open var width: CGFloat = 5.0
    open var alpha: CGFloat = 1.0
    
    var tool : DrawingTool!

    internal var blendMode: CGBlendMode = .normal
    internal var isEraser: Bool = false
}
