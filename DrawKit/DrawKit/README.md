# TouchCanvas: Using UITouch efficiently and effectively

TouchCanvas illustrates responsive touch handling using coalesced and predictive touches (when available) via a simple drawing app. The sample uses force information (when available) to change line thickness. Apple Pencil and finger touches are distinguished via different colors. In addition, Apple Pencil only data is demonstrated through the use of estimated properties and updates providing the actual property data including the azimuth and altitude of the Apple Pencil while in use.

Drawing Tools :
Tool            MaxWidth        Alpha           Range 
Pencile         6.0             0.8             1 - 6
Pen             8.0             1.0             2 - 12
Crayon          10.0            1.0             5 - 15
Paint Brush     20.0            0.5             10 - 25
Eraser          16              1.0             10 - 20
Paint Roller    30              0.8             10 - 35


## Requirements

### Build

Xcode 8.0 or later, iOS 10.0 SDK or later

### Runtime

iOS 9.1

Copyright (C) Uyloo Solution Private Limited. All rights reserved.
