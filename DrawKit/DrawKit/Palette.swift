//
//  Palette.swift
//  DrawKit
//
//  Created by Pankaj Dhiman on 13/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

enum DrawingTool : Int{
    
    case  none = -1
    case  pencil
    case  pen
    case  crayon
    case  paintBrush
    case  eraser
    case  paintRoller
}


@objc public protocol PaletteDelegate
{
    @objc optional func didChangeBrushAlpha(_ alpha:CGFloat)
    @objc optional func didChangeBrushWidth(_ width:CGFloat)
    @objc optional func didChangeBrushColor(_ color:UIColor)
    
    @objc optional func colorWithTag(_ tag: NSInteger) -> UIColor?
    @objc optional func alphaWithTag(_ tag: NSInteger) -> CGFloat
    @objc optional func widthWithTag(_ tag: NSInteger) -> CGFloat
}


open class Palette: UIView, colorDelegate
{
    open weak var delegate: PaletteDelegate?
    fileprivate var brush: Brush = Brush()
    
    fileprivate let buttonDiameter = UIScreen.main.bounds.width / 10.0
    fileprivate let buttonPadding = UIScreen.main.bounds.width / 25.0
    fileprivate let columnCount = 4
    fileprivate let brushColumnCount = 3

    fileprivate var brushButtonList = [BrushButton]()
    fileprivate var colorButtonList = [CircleButton]()
    fileprivate var alphaButtonList = [CircleButton]()
    fileprivate var widthButtonList = [CircleButton]()
    
    fileprivate var totalHeight: CGFloat = 0.0;
    
    fileprivate weak var brushPaletteView: UIView?
    fileprivate weak var colorPaletteView: UIView?
    fileprivate weak var colorMixtureView: UIView?
    fileprivate weak var alphaPaletteView: UIView?
    fileprivate weak var widthPaletteView: UIView?
    
    // MARK: - Public Methods
    public init() {
        super.init(frame: CGRect.zero)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open func currentBrush() -> Brush {
        return self.brush
    }
    
    private func initialize() {
        
    }
    
    // MARK: - Private Methods
    override open var intrinsicContentSize : CGSize {
        let size: CGSize = CGSize(width: UIScreen().bounds.size.width, height: self.totalHeight)
        return size;
    }
    
    open func setup() {

        self.backgroundColor = UIColor.init(red: 0.22, green: 0.22, blue: 0.21, alpha: 1.0)
        //self.backgroundColor = UIColor(colorLiteralRed: 0.22, green: 0.22, blue: 0.21, alpha: 1.0)
        
        self.setupBrushView()
        self.setupAlphaView()
        self.setupWidthView()
        self.setupColorView()
        self.setupColorMixtureView()
       
        self.setupDefaultValues()
    }
    
    open func paletteHeight() -> CGFloat {
        return self.totalHeight
    }
    
    fileprivate func setupBrushView() {
        
        let view = UIView()
        self.addSubview(view)
        self.brushPaletteView = view
        
        let columnCount : CGFloat = 3.0
        let diameter : CGFloat = self.bounds.width / 3.0
        let padding = (self.bounds.width - diameter * columnCount) / (columnCount + 1)
        
        var button: BrushButton?
        for index in 1...6 {

            let brushData = self.brushWithTag(index)
            button = BrushButton(diameter: diameter, image: brushData.0, thickness: brushData.1, color: UIColor.clear, alpha:brushData.2, tool: brushData.3)
            
            button?.frame = self.brushButtonRect(index: index, diameter: diameter, padding: padding, columnCount:NSInteger(columnCount))
            button?.addTarget(self, action:#selector(Palette.onClickBrushPicker(_:)), for: .touchUpInside)
            self.brushPaletteView!.addSubview(button!)
            self.brushButtonList.append(button!)
        }
        
        let lineImage = UIImage(named: "line", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let line = UIImageView(image: lineImage)
        self.brushPaletteView!.addSubview(line)
        line.frame = CGRect(x: 5 , y: (button?.frame.maxY)! + padding , width: self.frame.width - 10 , height: 1.0)
        
        self.brushPaletteView?.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: line.frame.maxY)
        
        self.totalHeight = self.totalHeight + line.frame.maxY

       // brushPaletteView?.backgroundColor = UIColor.magenta
    }

    fileprivate func setupColorView() {
        let view = UIView()
        self.addSubview(view)
        self.colorPaletteView = view
        
        let columnCount : CGFloat = 4.0
        let diameter : CGFloat = self.bounds.width / 5.0
        let padding = (self.bounds.width - diameter * columnCount) / (columnCount + 1)
        
        var button: CircleButton?
        for index in 1...12 {
            let color: UIColor = self.color(index)
            button = CircleButton(diameter: diameter , color: color, opacity: 1.0)
            button?.frame = self.brushButtonRect(index: index, diameter: diameter, padding: padding, columnCount:NSInteger(columnCount))
            button?.addTarget(self, action:#selector(Palette.onClickColorPicker(_:)), for: .touchUpInside)
            self.colorPaletteView!.addSubview(button!)
            self.colorButtonList.append(button!)
        }
        
        
        let lineImage = UIImage(named: "line", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let line = UIImageView(image: lineImage)
        self.colorPaletteView!.addSubview(line)
        line.frame = CGRect(x: 5 , y: (button?.frame.maxY)! + padding , width: self.frame.width - 10 , height: 1.0)
        
        self.colorPaletteView?.frame = CGRect(x: 0, y: self.totalHeight, width: self.frame.width, height: button!.frame.maxY + padding)
        
        self.totalHeight = self.totalHeight + (self.colorPaletteView?.frame.height)!

       // colorPaletteView?.backgroundColor = UIColor.blue
    }
    
    fileprivate func setupColorMixtureView() {

        let view = UIView()
        self.addSubview(view)
        self.colorMixtureView = view
        
        let padding : CGFloat = 8.0
        let height : CGFloat = 30.0
        
        let colorChoice1 = ColorPicker(frame: CGRect(x: padding, y: padding, width: self.frame.width - 2*padding, height: height))
        colorChoice1.delegate = self
        self.colorMixtureView?.addSubview(colorChoice1)
        
        colorChoice1.layer.cornerRadius = height/2
        colorChoice1.layer.masksToBounds = true;
        
        colorChoice1.layer.borderColor = UIColor.gray.cgColor;
        colorChoice1.layer.borderWidth = 0.5;
        
        let colorChoice2 = ColorPicker(frame: CGRect(x: padding, y: colorChoice1.frame.maxY + padding, width: self.frame.width - 2*padding, height: height))
        colorChoice2.delegate = self
        self.colorMixtureView?.addSubview(colorChoice2)
       
        colorChoice2.layer.cornerRadius = height/2;
        colorChoice2.layer.masksToBounds = true;
        
        colorChoice2.layer.borderColor = UIColor.gray.cgColor;
        colorChoice2.layer.borderWidth = 0.5;
        
        let colorChoice3 = ColorPicker(frame: CGRect(x: padding, y: colorChoice2.frame.maxY + padding, width: self.frame.width - 2*padding, height: height))
        colorChoice3.delegate = self
        self.colorMixtureView?.addSubview(colorChoice3)
        
        colorChoice3.layer.cornerRadius = height/2;
        colorChoice3.layer.masksToBounds = true;
        
        colorChoice3.layer.borderColor = UIColor.gray.cgColor;
        colorChoice3.layer.borderWidth = 0.5;
        
        self.colorMixtureView?.frame = CGRect(x: 0, y: self.totalHeight, width: self.frame.width, height: colorChoice3.frame.maxY + padding)

        self.totalHeight = self.totalHeight + (self.colorMixtureView?.frame.height)!
    }
    
    func pickedColor(_ color: UIColor) {

        self.brush.color = color
    }
    
    fileprivate func brushButtonRect(index: NSInteger, diameter: CGFloat, padding: CGFloat, columnCount:NSInteger) -> CGRect {
        var rect: CGRect = CGRect.zero
        let indexValue = index - 1
        
        rect.origin.x = (CGFloat(indexValue % columnCount) * diameter) + padding + (CGFloat(indexValue % columnCount) * padding)
        rect.origin.y = (CGFloat(indexValue / columnCount) * diameter) + padding + (CGFloat(indexValue / columnCount) * padding)
       
        rect.size = CGSize(width: diameter, height: diameter)
        
        return rect
    }

    
    fileprivate func colorButtonRect(index: NSInteger, diameter: CGFloat, padding: CGFloat) -> CGRect {
        var rect: CGRect = CGRect.zero
        let indexValue = index - 1
        let count = self.columnCount
        rect.origin.x = (CGFloat(indexValue % count) * diameter) + padding + (CGFloat(indexValue % count) * padding)
        rect.origin.y = (CGFloat(indexValue / count) * diameter) + padding + (CGFloat(indexValue / count) * padding)
        rect.size = CGSize(width: diameter, height: diameter)
        
        return rect
    }
    
    fileprivate func setupAlphaView() {
        let view = UIView()
        self.addSubview(view)
        self.alphaPaletteView = view
        
        let totalHeight : CGFloat = 50.0
        let sliderHeight : CGFloat = 30.0
        let ovelWidth : CGFloat = 25.0
        let paddingX : CGFloat = 5.0
        
        let leftImage = UIImage(named: "oval", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let leftOval = UIImageView(image: leftImage)
        self.alphaPaletteView!.addSubview(leftOval)
        leftOval.frame = CGRect(x: paddingX + 3 , y: (totalHeight - ovelWidth)/2 + 3 , width: ovelWidth - 6, height: ovelWidth - 6)
        
        let slider = UISlider (frame: CGRect(x: 2*paddingX + leftOval.frame.size.width + 3, y: (totalHeight - sliderHeight)/2 ,width:self.frame.width - (ovelWidth*2 + paddingX*4), height: sliderHeight))
        slider.minimumValue = 0
        slider.maximumValue = 100
        slider.tintColor = UIColor.white
        slider.isContinuous = true
        slider.value = 80
        slider.addTarget(self, action:#selector(widthValueDidChange), for: .valueChanged)
        self.alphaPaletteView!.addSubview(slider)
        
        let rightImage = UIImage(named: "oval", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let rightOval = UIImageView(image: rightImage)
        self.alphaPaletteView!.addSubview(rightOval)
        rightOval.frame = CGRect(x: slider.frame.maxX + paddingX, y: (totalHeight - ovelWidth)/2 , width: ovelWidth, height: ovelWidth)
        
        self.alphaPaletteView?.frame = CGRect(x: 0, y: self.totalHeight, width: (self.brushPaletteView?.frame.width)!, height: totalHeight)
        
        self.totalHeight = self.totalHeight + (self.alphaPaletteView?.frame.height)!

       // alphaPaletteView?.backgroundColor = UIColor.brown
    }
    
    fileprivate func alphaButtonRect(index: NSInteger, diameter: CGFloat, padding: CGFloat) -> CGRect {
        var rect: CGRect = CGRect.zero
        let indexValue = index - 1
        rect.origin.x = padding
        rect.origin.y = CGFloat(indexValue) * diameter + padding + (CGFloat(indexValue) * padding)
        rect.size = CGSize(width: diameter, height: diameter)
        
        return rect
    }
    
    fileprivate func setupWidthView() {
        let view = UIView()
        self.addSubview(view)
        self.widthPaletteView = view
        
        let totalHeight : CGFloat = 50.0
        let sliderHeight : CGFloat = 30.0
        let ovelWidth : CGFloat = 25.0
        let paddingX : CGFloat = 5.0
        
        let leftImage = UIImage(named: "medical_hide", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let leftOval = UIImageView(image: leftImage)
        leftOval.contentMode = UIView.ContentMode.scaleAspectFit
        self.widthPaletteView!.addSubview(leftOval)
        leftOval.frame = CGRect(x: paddingX, y: (totalHeight - ovelWidth)/2, width: ovelWidth, height: ovelWidth)
        
        let slider = UISlider (frame: CGRect(x: 2*paddingX + leftOval.frame.size.width, y: (totalHeight - sliderHeight)/2,width:self.frame.width - (ovelWidth*2 + paddingX*4), height: sliderHeight))
        slider.minimumValue = 0
        slider.maximumValue = 100
        slider.tintColor = UIColor.white
        slider.isContinuous = true
        slider.value = 60
        slider.addTarget(self, action:#selector(alphaValueDidChange), for: .valueChanged)
        self.widthPaletteView!.addSubview(slider)
        
        let rightImage = UIImage(named: "medical", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let rightOval = UIImageView(image: rightImage)
        rightOval.contentMode = UIView.ContentMode.scaleAspectFit
        self.widthPaletteView!.addSubview(rightOval)
        rightOval.frame = CGRect(x: slider.frame.maxX + paddingX, y: (totalHeight - ovelWidth)/2, width: ovelWidth, height: ovelWidth)
        
        let lineImage = UIImage(named: "line", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let line = UIImageView(image: lineImage)
        self.widthPaletteView!.addSubview(line)
        line.frame = CGRect(x: 5 , y: (2*slider.frame.minY + slider.frame.height) , width: self.frame.width - 10 , height: 1.0)
        
        self.widthPaletteView?.frame = CGRect(x: 0, y: self.totalHeight, width: (self.frame.width), height: totalHeight + line.frame.height)
        
        self.totalHeight = self.totalHeight + (self.widthPaletteView?.frame.height)!
        
       // widthPaletteView?.backgroundColor = UIColor.orange
    }
    
    fileprivate func widthButtonRect(_ diameter: CGFloat, padding: CGFloat, lastY: CGFloat) -> CGRect {
        var rect: CGRect = CGRect.zero
        rect.origin.x = padding + ((self.buttonDiameter - diameter) / 2)
        rect.origin.y = lastY + padding
        rect.size = CGSize(width: diameter, height: diameter)
        
        return rect
    }
    
    fileprivate func setupDefaultValues() {
        
        let button: CircleButton = self.colorButtonList.first!
        button.isSelected = true
        
        let brushButton: BrushButton = self.brushButtonList.first!
        brushButton.isSelected = true

        self.brush.color = button.color!
        self.brush.width = brushButton.thickness
        self.brush.alpha = brushButton.alphaa
        
        self.brush.tool = brushButton.type
    }
    
    open func setupValues(info: [String:Any]) {
        

    }

    /**
    ["BrushType":"0",
    "Color":"4736272",
    "Thickness":20.0,
    "Alpha":0.80,
 */
    
    @objc fileprivate func onClickBrushPicker(_ button: BrushButton) {

        self.brush.width = button.thickness
        self.brush.alpha = button.alphaa
        self.brush.tool = button.type
        self.brush.isPaintBrush = button.type == DrawingTool.paintBrush ? true : false
        
        
        self.resetBrushSelected(self.brushButtonList, button: button)
    }
    
    @objc fileprivate func onClickColorPicker(_ button: CircleButton) {
       
        self.brush.color = button.color!;
        let shouldEnable = !self.brush.color.isEqual(UIColor.clear)
        
        self.resetButtonSelected(self.colorButtonList, button: button)
        self.updateColorOfButtons(self.widthButtonList, color: button.color!)
        self.updateColorOfButtons(self.alphaButtonList, color: button.color!, enable: shouldEnable)
        
        self.delegate?.didChangeBrushColor?(self.brush.color)
    }
    
    @objc fileprivate func onClickAlphaPicker(_ button: CircleButton) {
        self.brush.alpha = button.opacity!
        self.resetButtonSelected(self.alphaButtonList, button: button)
        
        self.delegate?.didChangeBrushAlpha?(self.brush.alpha)
    }
    
    @objc fileprivate func onClickWidthPicker(_ button: CircleButton) {
        self.brush.width = button.diameter!;
        self.resetButtonSelected(self.widthButtonList, button: button)
        
        self.delegate?.didChangeBrushWidth?(self.brush.width)
    }
    
    fileprivate func resetBrushSelected(_ list: [BrushButton], button: BrushButton) {
        var count = 0
        for aButton: BrushButton in list {
            aButton.isSelected = aButton.isEqual(button)
            count = count + 1
        }
    }
    
    fileprivate func resetButtonSelected(_ list: [CircleButton], button: CircleButton) {
        for aButton: CircleButton in list {
            aButton.isSelected = aButton.isEqual(button)
        }
    }
    
    fileprivate func updateColorOfButtons(_ list: [CircleButton], color: UIColor, enable: Bool = true) {
        for aButton: CircleButton in list {
            aButton.update(color)
            aButton.isEnabled = enable
        }
    }
    
    @objc fileprivate func alphaValueDidChange(sender:UISlider!) {
        
        let tool : DrawingTool = self.brush.tool
        switch tool {
        case .pencil:
            self.brush.alpha = CGFloat(sender.value) / 100.0
            break
        case .pen:
            self.brush.alpha = CGFloat(sender.value) / 100.0
            break
        case .crayon:
            self.brush.alpha = CGFloat(sender.value) / 100.0
            break
        case .paintBrush:
            self.brush.alpha = CGFloat(sender.value) / 100.0
            break
        case .eraser:
            self.brush.alpha = CGFloat(sender.value) / 100.0
            break
        case .paintRoller:
            self.brush.alpha = CGFloat(sender.value) / 100.0
            break
        default: break
            
        }
    }
    
    @objc fileprivate func widthValueDidChange(sender:UISlider!) {
               
        let tool : DrawingTool = self.brush.tool
        switch tool {
        case .pencil:
            self.brush.width = 1.0 + (CGFloat(sender.value) * 5.0) / 100.0
            self.brush.isPaintBrush = false

            break
        case .pen:
            self.brush.width = 2.0 + (CGFloat(sender.value) * 10.0) / 100.0
            self.brush.isPaintBrush = false

            break
        case .crayon:
            self.brush.width = 5.0 + (CGFloat(sender.value) * 10.0) / 100.0
            self.brush.isPaintBrush = false

            break
        case .paintBrush:
            self.brush.width = 10.0 + (CGFloat(sender.value) * 15.0) / 100.0
            self.brush.isPaintBrush = true
            break
        case .eraser:
            self.brush.width = 10.0 + (CGFloat(sender.value) * 10.0) / 100.0
            self.brush.isPaintBrush = false

            break
        case .paintRoller:
            self.brush.width = 20 + (CGFloat(sender.value) * 15.0) / 100.0
            self.brush.isPaintBrush = false

            break
        default: break
            
        }
    }

    fileprivate func color(_ tag: NSInteger) -> UIColor {
        if let color = self.delegate?.colorWithTag?(tag)  {
            return color
        }
        
        return self.colorWithTag(tag)
    }
    
    fileprivate func brushWithTag(_ tag: NSInteger) -> (UIImage, CGFloat, CGFloat, DrawingTool) {

        switch(tag) {
        case 1:
            return (UIImage(named: "pencil")!, 6.0, 0.8, DrawingTool.pencil)
        case 2:
            return (UIImage(named: "pen")!, 8.0, 1.0, DrawingTool.pen)
        case 3:
            return (UIImage(named: "crayon")!, 10.0, 1.0, DrawingTool.crayon)
        case 4:
            return (UIImage(named: "paint_brush")!, 20.0, 0.6, DrawingTool.paintBrush)
        case 5:
            return (UIImage(named: "eraser")!, 16, 1.0, DrawingTool.eraser)
        default:
            return (UIImage(named: "paint_roller")!,30, 0.9, DrawingTool.paintRoller)
        }
    }
    
    
    fileprivate func colorWithTag(_ tag: NSInteger) -> UIColor {
        switch(tag) {
        case 1:
            return UIColor.black
        case 2:
            return UIColor.darkGray
        case 3:
            return UIColor.gray
        case 4:
            return UIColor.white
        case 5:
            return UIColor(red: 0.8, green: 0.2, blue: 0.2, alpha: 1.0)
        case 6:
            return UIColor.orange
        case 7:
            return UIColor.green
        case 8:
            return UIColor(red: 0.15, green: 0.47, blue: 0.23, alpha: 1.0)
        case 9:
            return UIColor(red: 0.2, green: 0.3, blue: 1.0, alpha: 1.0)
        case 10:
            return UIColor(red: 0.2, green: 0.8, blue: 1.0, alpha: 1.0)
        case 11:
            return UIColor(red: 0.62, green: 0.32, blue: 0.17, alpha: 1.0)
        case 12:
            return UIColor.yellow
        default:
            return UIColor.black
        }
    }
    
    fileprivate func opacity(_ tag: NSInteger) -> CGFloat {
        if let opacity = self.delegate?.alphaWithTag?(tag) {
            if 0 > opacity || opacity > 1 {
                return CGFloat(tag) / 3.0
            }
            return opacity
        }
        
        return CGFloat(tag) / 3.0
    }
    
    fileprivate func brushWidth(_ tag: NSInteger) -> CGFloat {
        if let width = self.delegate?.widthWithTag?(tag) {
            if 0 > width || width > self.buttonDiameter {
                return self.buttonDiameter * (CGFloat(tag) / 4.0)
            }
            return width
        }
        return self.buttonDiameter * (CGFloat(tag) / 4.0)
    }
}
