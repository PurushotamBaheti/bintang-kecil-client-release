//  Copyright (c) 2017 9elements GmbH <contact@9elements.com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit

/// The `ColorPickerView` provides a way to pick colors.
/// It contains three elements - a hue picker, a brightness and saturation picker and an alpha
/// picker. It has full support for wide colors.
@IBDesignable @objc(IMGLYColorPickerView) open class OpacityPicker: UIControl {
    
    // MARK: - Properties
    
    /// The currently selected color
    @IBInspectable public var color: UIColor {
        get {
            return UIColor(
                alpha: alphaPickerView.pickedAlpha
            )
        }
        
        set {
            if color != newValue {
                let hsb = newValue.hsb
                
                var alpha: CGFloat = 0
                newValue.getWhite(nil, alpha: &alpha)
                alphaPickerView.displayedColor = newValue
                alphaPickerView.pickedAlpha = alpha
                
                updateMarkersToMatchColor()
            }
        }
    }

    private let alphaPickerView = AlphaPickerView()
    
    // MARK: - Initializers
    
    /// :nodoc:
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    /// :nodoc:
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        configureAlphaPickerView()
        configureConstraints()
    }
    
    private func configureAlphaPickerView() {
        alphaPickerView.translatesAutoresizingMaskIntoConstraints = false
        alphaPickerView.addTarget(self, action: #selector(alphaPickerChanged(_:)), for: .valueChanged)
        addSubview(alphaPickerView)
    }
    
    private func configureConstraints() {
        let views = [
            "alphaPickerView": alphaPickerView,
            ]
        
        var constraints = [NSLayoutConstraint]()

        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[alphaPickerView]-0-|", options: [], metrics: nil, views: views))
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "|-0-[alphaPickerView]-0-|", options: [], metrics: nil, views: views))
        
        NSLayoutConstraint.activate(constraints)

        
    }
    
    // MARK: - UIView
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        updateMarkersToMatchColor()
    }
    
    @objc private func alphaPickerChanged(_ alphaPickerView: AlphaPickerView) {
        sendActions(for: .valueChanged)
    }

    // MARK: - Markers
    
    private func updateMarkersToMatchColor() {
        alphaPickerView.updateMarkerPosition()
    }
}

