//
//  Drawing.swift
//  DrawKit
//
//  Created by Pankaj Dhiman on 13/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

open class Drawing: NSObject {
    open var stroke: UIImage?
    open var background: UIImage?
    
    public init(stroke: UIImage? = nil, background: UIImage? = nil) {
        self.stroke = stroke
        self.background = background
    }
}
