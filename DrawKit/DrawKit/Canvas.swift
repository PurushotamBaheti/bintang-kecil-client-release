//
//  Canvas.swift
//  DrawKit
//
//  Created by Pankaj Dhiman on 13/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

@objc public protocol CanvasDelegate
{
    @objc optional func canvas(_ canvas: Canvas, didUpdateDrawing drawing: Drawing, mergedImage image: UIImage?)
    @objc optional func canvas(_ canvas: Canvas, didSaveDrawing drawing: Drawing, mergedImage image: UIImage?)
    
    @objc optional func canvas(_ canvas: Canvas, willBegunDrawing drawing: Drawing)
    @objc optional func canvas(_ canvas: Canvas, WillEndDrawing drawing: Drawing)
    
    func brush() -> Brush?
}


open class Canvas: UIView, UITableViewDelegate {
    
    open weak var delegate: CanvasDelegate?
    
    fileprivate var canvasId: String?
    
    fileprivate var mainImageView = UIImageView() // conains sketch file
    fileprivate var tempImageView = UIImageView() // contains paper background file
    
    // fileprivate var currentImageView = UIImageView() // contains current drawing file
    
    var tempImage = UIImageView() //Extra
    var previewImage : UIImage! //Extra
    var lastPoint = CGPoint.zero //Extra
    
    open var currentView = UIView()
    var currentIndex = 0
    open var layerView = [UIView]()
    
    
    var listOfCanvas = [Int : [UIImageView]]() //Extre
    open var arrCurrentImageView = [UIImageView]() //Extre
    open var arrTempImageView = [UIImageView]() //Extre
    
    fileprivate var backgroundImageView = UIImageView() // contains texture file
    
    fileprivate var brush = Brush() //
    fileprivate let session = Session()
    fileprivate var drawing = Drawing()
    
    fileprivate let path = UIBezierPath()
    fileprivate let scale = UIScreen.main.scale
    
    fileprivate var saved = false
    fileprivate var pointMoved = false
    fileprivate var pointIndex = 0
    fileprivate var points = [CGPoint?](repeating: CGPoint.zero, count: 5)
    
    let myImage = UIImage(named: "colo_2.png")
    
    let brushOpacity : CGFloat = 0.7
    
    var isPencil : Bool = false
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public init(canvasId:String, backgroundImage:UIImage, sketchImage:UIImage, layers:[UIView]?) {
        super.init(frame: CGRect.zero)
        
        self.path.lineCapStyle = .butt
        self.canvasId = canvasId
        
        // clear background
        self.tempImageView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        // default hide the paper
        self.backgroundImageView.isHidden = true
        
        // use paper to show here
        self.addSubview(self.backgroundImageView)
        
        self.backgroundImageView.contentMode = .scaleAspectFit
        self.backgroundImageView.autoresizingMask = [.flexibleHeight ,.flexibleWidth]
        self.backgroundImageView.image = backgroundImage
        
        
        // use sketch to show here
        self.addSubview(self.mainImageView)
        self.mainImageView.contentMode = .scaleAspectFit
        self.mainImageView.autoresizingMask = [.flexibleHeight ,.flexibleWidth]
        // self.mainImageView.frame = CGRect.init(x: 70, y: 0, width: width - 140, height: height - 64)
        self.mainImageView.image = sketchImage
        
        // use this image to store temporary drawing
        self.addSubview(self.tempImageView)
        self.tempImageView.autoresizingMask = [.flexibleHeight ,.flexibleWidth]
        
        if let layers = layers {
            
            for layer in layers {
                self.layerView.append(layer)
                self.addSubview(layer)
            }
            
            if layers.count > 0 {
                currentView = self.layerView[0]
                currentIndex = 0
                
                self.addSubview(currentView)
            }
        }
        else {
            
            // default first layer
            currentView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
            self.layerView.append(currentView)
            currentIndex = 0
            
            self.addSubview(currentView)
        }
    }
    
    
    fileprivate func compare(_ image1: UIImage?, isEqualTo image2: UIImage?) -> Bool {
        if (image1 == nil && image2 == nil) {
            return true
        } else if (image1 == nil || image2 == nil) {
            return false
        }
        
        let data1 = image1!.pngData()
        let data2 = image2!.pngData()
        
        if (data1 == nil || data2 == nil) {
            return false
        }
        
        return (data1! == data2)
    }
    
    fileprivate func currentDrawing() -> Drawing {
        return Drawing(stroke: self.mainImageView.image, background: self.backgroundImageView.image)
    }
    
    fileprivate func updateByLastSession() {
        let lastSession = self.session.lastSession()
        self.mainImageView.image = lastSession?.stroke
        self.backgroundImageView.image = lastSession?.background
    }
    
    fileprivate func didUpdateCanvas() {
        let mergedImage = self.mergePathsAndImages()
        let currentDrawing = self.currentDrawing()
        self.delegate?.canvas?(self, didUpdateDrawing: currentDrawing, mergedImage: mergedImage)
    }
    
    fileprivate func didSaveCanvas() {
        let mergedImage = self.mergePathsAndImages()
        self.delegate?.canvas?(self, didSaveDrawing: self.drawing, mergedImage: mergedImage)
    }
    
    fileprivate func isStrokeEqual() -> Bool {
        return self.compare(self.drawing.stroke, isEqualTo: self.mainImageView.image)
    }
    
    fileprivate func isBackgroundEqual() -> Bool {
        return self.compare(self.drawing.background, isEqualTo: self.backgroundImageView.image)
    }
    
    
    // MARK: - UITouch delegates
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if let touch = touches.first{
            lastPoint = touch.location(in: self)
        }
        
        self.saved = false
        self.pointMoved = false
        self.pointIndex = 0
        
        let touch = touches.first!
        self.points[0] = touch.location(in: self)
        
        self.delegate?.canvas?(self, willBegunDrawing: currentDrawing())
    }
    
    
    
    override open func touchesEstimatedPropertiesUpdated(_ touches: Set<UITouch>) {
        //FOR APPLE PENCIL
        if(!isPencil){
            isPencil = true
            //MARK: - ALERT CODE
            
        }
        if(brush.tool == DrawingTool.paintBrush || brush.isPaintBrush) {
            
            if let touch = touches.first {
                let currentPoint = touch.location(in: self)
                self.drawLine(fromPoint: self.lastPoint,toPoint: currentPoint)
                self.lastPoint = currentPoint
            }
        }
        else {
            
            if let touch = touches.first {
                let currentPoint = touch.location(in: self)
                self.drawLine(fromPoint: self.lastPoint,toPoint: currentPoint)
                self.lastPoint = currentPoint
            }
        }
        
    }
    
    /*
     * Smooth Freehand Drawing on iOS
     * http://code.tutsplus.com/tutorials/ios-sdk_freehand-drawing--mobile-13164
     *
     */
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if(!isPencil){
            if(brush.tool == DrawingTool.paintBrush || brush.isPaintBrush) {
                
                if let touch = touches.first {
                    let currentPoint = touch.location(in: self)
                    self.drawLine(fromPoint: self.lastPoint,toPoint: currentPoint)
                    self.lastPoint = currentPoint
                }
            }
            else {
                
                if let touch = touches.first {
                    let currentPoint = touch.location(in: self)
                    self.drawLine(fromPoint: self.lastPoint,toPoint: currentPoint)
                    self.lastPoint = currentPoint
                }
            }
        }
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.touchesEnded(touches, with: event)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if(self.brush.isPaintBrush) {
            self.tempImageView.alpha = brushOpacity
        }
        else {
            
            if self.brush.isEraser {
                // should draw on screen for being erased
                
                // PDH : ?????
                for tempView in currentView.subviews {
                    
                    let img = tempView as! UIImageView
                    img.alpha = tempView.alpha
                    img.image?.draw(in: self.bounds)
                    
                }
            }
            self.tempImageView.alpha = self.brush.alpha
        }
        
        let image = UIImageView(image: self.tempImageView.image)
        
        if(brush.isPaintBrush) {
            image.alpha = brushOpacity
        }
        else {
            image.alpha = self.brush.alpha
        }
        
        self.currentView.addSubview(image)
        self.tempImageView.image = nil
        
        self.delegate?.canvas?(self, WillEndDrawing: currentDrawing())
        
        super.touchesEnded(touches, with: event)
    }
    
    
    
    // MARK: - Private  Methods
    
    fileprivate func strokePath() {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        
        if(self.brush.isPaintBrush) {
            self.tempImageView.alpha = 0.3
        }
        else
        {
            if self.brush.isEraser {
                // should draw on screen for being erased
                
                // PDH : ????
                for tempView in currentView.subviews {
                    let img = tempView as! UIImageView
                    img.image?.draw(in: self.bounds)
                    //    self.currentImageView.image?.draw(in: self.bounds)
                }
                self.mainImageView.image?.draw(in: self.bounds)
            }
            self.tempImageView.alpha = self.brush.alpha
        }
    }
    
    fileprivate func mergePaths() {
        
        UIGraphicsBeginImageContext(mainImageView.frame.size)
        
        self.mainImageView.image?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height), blendMode: CGBlendMode.normal, alpha: 1.0)
        
        if(self.brush.isPaintBrush) {
            self.tempImageView.image?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height), blendMode: CGBlendMode.normal, alpha: 0.3)
        }
        else {
            
            if(brush.isEraser) {
                self.tempImageView.image?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height), blendMode: CGBlendMode.clear, alpha: 1.0)
            }
            else {
                self.tempImageView.image?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height), blendMode: CGBlendMode.normal, alpha: 1.0)
            }
        }
        
        // PDH : why in main view ??????
        self.mainImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.tempImageView.image = nil
    }
    
    
    fileprivate func mergePathsAndImages() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        
        // PDH : why in backgroundImageView
        if self.mainImageView.image != nil {
            let rect = self.centeredBackgroundImageRect()
            self.mainImageView.image?.draw(in: rect)            // draw background image
        }
        
        // hide background sketch when saving this drawing
        self.backgroundImageView.isHidden = true
        
        UIImage(view: self).draw(in: self.bounds)
        //self.mainImageView.image?.draw(in: self.bounds)               // draw stroke
        
        let mergedImage = UIGraphicsGetImageFromCurrentImageContext()   // merge
        
        UIGraphicsEndImageContext()
        
        return mergedImage!
    }
    
    fileprivate func centeredBackgroundImageRect() -> CGRect {
        
        if self.frame.size.equalTo((self.mainImageView.image?.size)!) {
            return self.frame
        }
        
        let selfWidth = self.frame.width
        let selfHeight = self.frame.height
        let imageWidth = self.mainImageView.image?.size.width
        let imageHeight = self.mainImageView.image?.size.height
        
        let widthRatio = selfWidth / imageWidth!
        let heightRatio = selfHeight / imageHeight!
        let scale = min(widthRatio, heightRatio)
        let resizedWidth = (scale * imageWidth!) 
        let resizedHeight = scale * imageHeight!
        
        var rect = CGRect.zero
        rect.size = CGSize(width: resizedWidth, height: resizedHeight)
        
        if selfWidth > resizedWidth {
            rect.origin.x = (selfWidth - resizedWidth) / 2
        }
        
        if selfHeight > resizedHeight {
            rect.origin.y = (selfHeight - resizedHeight) / 2
        }
        
        return rect
    }
    
    // MARK: - Layers support methods
    
    // hide and show layer at index
    public func layerHiddenAtIndex(_ atIndex:Int) -> UIView {
        
        if layerView.count > atIndex  {
            layerView[atIndex].isHidden = !layerView[atIndex].isHidden
            let layer = layerView[atIndex]
            if layer.isHidden == true {
                layer.removeFromSuperview()
                var newIndex = atIndex - 1
                
                if newIndex < 0 {
                    
                    var isAllHidden = true
                    for view in layerView {
                        if view.isHidden == false {
                            isAllHidden = false
                            break
                        }
                    }
                    
                    if isAllHidden == true { // if empty add default layer
                        return self.addLayer()
                    }
                    else {
                        newIndex = atIndex + 1
                    }
                }
                
                currentView = layerView[newIndex]
                self.bringSubviewToFront(currentView)
                currentIndex = newIndex
            }
            else {
                self.addSubview(layer)
                currentView = layer
                currentIndex = atIndex
            }
        }
        
        return currentView
    }
    
    // set current layers
    public func setCurrentLayerAtIndex(_ index : Int) -> UIView {
        
        if layerView.count > index  {
            currentView = layerView[index]
            self.bringSubviewToFront(currentView)
            currentIndex = index
        }
        
        return currentView
    }
    
    // add new layer at 0th index
    open func addLayer() -> UIView {
        
        currentView = UIView(frame: self.currentView.frame)
        currentIndex = 0
        self.layerView.insert(currentView, at: 0)
        self.addSubview(currentView)
        
        return currentView
    }
    
    open func deleteLayerAtIndex(_ index:Int) -> UIView {
        //         if layerView.count > index && index > 0 {
        
        if layerView.count > index {
            let layer = layerView[index]
            layer.removeFromSuperview()
            layerView.remove(at: index)
            var newIndex = index - 1
            
            if newIndex < 0 {
                if layerView.count == 0 { // if empty add default layer
                    return self.addLayer()
                }
                else {
                    newIndex = index + 1
                }
            }
            
            currentView = layerView[newIndex]
            self.bringSubviewToFront(currentView)
            currentIndex = newIndex
        }
        
        return currentView
    }
    
    // MARK: - ==========================
    // return all layers
    public func returnData() -> [UIView] {
        return layerView
    }
    
    // return current layer
    public func returnCurrent() -> UIView {
        return currentView
    }
    
    //MARK: - Extra Methods
    func drawLine(fromPoint: CGPoint, toPoint: CGPoint) {
        
        if(!self.brush.isEraser) {
            
            let targetImageView = self.tempImageView
            UIGraphicsBeginImageContext(self.frame.size)
            targetImageView.image?.draw(in: self.bounds)
            
            if(brush.tool == DrawingTool.paintBrush || brush.isPaintBrush){
                let context = UIGraphicsGetCurrentContext()
                // PDH : what is previdew image here ?????
                previewImage = myImage?.tinted(color: self.brush.color)
                context?.draw(previewImage.cgImage!, in: CGRect(x: fromPoint.x - (self.brush.width/2), y: fromPoint.y - (self.brush.width/2), width:self.brush.width , height:self.brush.width))
                
                targetImageView.image = UIGraphicsGetImageFromCurrentImageContext()
                targetImageView.alpha = brushOpacity
                UIGraphicsEndImageContext()
            }
            else{
                let context = UIGraphicsGetCurrentContext()
                context?.move(to: fromPoint)
                context?.addLine(to: toPoint)
                context?.setLineCap(CGLineCap.round)
                context?.setLineWidth(self.brush.width)
                context?.setStrokeColor(self.brush.color.cgColor)
                context?.setBlendMode(CGBlendMode.copy)
                context?.strokePath()
                targetImageView.image = UIGraphicsGetImageFromCurrentImageContext()
                targetImageView.alpha = self.brush.alpha
                UIGraphicsEndImageContext()
            }
        }
        else{
            for subView in currentView.subviews{
                let targetImageView = subView as! UIImageView
                UIGraphicsBeginImageContext(self.frame.size)
                targetImageView.image?.draw(in: self.bounds)
                
                let context = UIGraphicsGetCurrentContext()
                
                context?.move(to: fromPoint)
                context?.addLine(to: toPoint)
                context?.setLineCap(CGLineCap.round)
                context?.setLineWidth(self.brush.width)
                
                context?.setStrokeColor(UIColor.clear.cgColor)
                context?.setBlendMode(CGBlendMode.clear)
                
                context?.strokePath()
                targetImageView.image = UIGraphicsGetImageFromCurrentImageContext()
                //  targetImageView.alpha = 1.0
                UIGraphicsEndImageContext()
            }
        }
        
    }
    
    
    // MARK: - Public Methods
    open func updateDrawing(brushSize:Float, color:UIColor, opacity:Float, isEraser:Bool, isPaint : Bool,brushType : Int) {
        
        self.brush.width =  CGFloat(brushSize)
        self.brush.alpha = CGFloat(opacity)
        self.brush.color = isEraser ? UIColor.clear : color
        self.brush.isPaintBrush = isPaint
        //Chanage Here
        
        switch brushType {
        case 0:
            self.brush.tool = DrawingTool.pencil
            break
        case 1:
            self.brush.tool = DrawingTool.crayon
            break
        case 2:
            self.brush.tool = DrawingTool.pencil
            break
        case 3:
            self.brush.tool = DrawingTool.paintBrush
            break
        case 4:
            self.brush.tool = DrawingTool.eraser
            break
            
        default:
            self.brush.tool = DrawingTool.pencil
            break
        }
    }
    
    // hide show paper image
    open func mainImageHidden(_ hidden: Bool) {
        self.mainImageView.isHidden = hidden
    }
    
    // hide show sketch image
    open func backgroundHidden(_ hidden: Bool) {
        self.backgroundImageView.isHidden = hidden
    }
    
    open func update(_ backgroundImage: UIImage?) {
        // self.backgroundImageView.image = backgroundImage
        self.session.append(self.currentDrawing())
        self.saved = self.canSave()
        self.didUpdateCanvas()
    }
    
    open func currentFilters() -> [UIImageView] {
        
        return arrCurrentImageView
    }
    
    open func selectedLayer(selectedLayer:UIImageView)
    {
        self.tempImageView = selectedLayer
    }
    
    open func undo() {
        
        if(currentView.subviews.count > 0){
            let topview = currentView.subviews.last
            topview!.removeFromSuperview()
        }
        if(arrTempImageView.count > 0) {
            let view = arrTempImageView[arrTempImageView.count - 1]
            view.removeFromSuperview()
            arrTempImageView.remove(at: arrTempImageView.count - 1)
        }
    }
    
    open func redo() {
        if(arrTempImageView.count < arrCurrentImageView.count)
        {
            arrTempImageView.append(arrCurrentImageView[arrTempImageView.count])
            let view = arrTempImageView[arrTempImageView.count - 1]
            self.addSubview(view)
        }
    }
    
    open func clear() {
        
        if(currentView.subviews.count > 0){
            for tempView in currentView.subviews {
                let img = tempView as! UIImageView
                img.removeFromSuperview()
            }
        }
    }
    
    
    
    open func save() {
        self.drawing.stroke = UIImage(view: self)
        self.drawing.background = self.backgroundImageView.image
        self.saved = true
        self.didSaveCanvas()
    }
    
    open func canUndo() -> Bool {
        return self.session.canUndo()
    }
    
    open func canRedo() -> Bool {
        return self.session.canRedo()
    }
    
    open func canClear() -> Bool {
        return self.session.canReset()
    }
    
    open func canSave() -> Bool {
        return !(self.isStrokeEqual() && self.isBackgroundEqual())
    }
}



extension String {
    func floatValue() -> Float? {
        if let floatval = Float(self) {
            return floatval
        }
        return nil
    }
}

extension UIImage {
    func getPixelColor(pos: CGPoint) -> UIColor {
        
        let pixelData = self.cgImage!.dataProvider!.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4
        
        let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
        let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
    
    func tinted(color:UIColor) -> UIImage? {
        let image = self
        let rect:CGRect = CGRect(origin: CGPoint(x: 0, y: 0), size: image.size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, image.scale)
        let context = UIGraphicsGetCurrentContext()!
        image.draw(in: rect)
        context.setFillColor(color.cgColor)
        context.setBlendMode(.sourceAtop)
        context.fill(rect)
        if let result:UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return result
        }
        else {
            return nil
        }
    }
    
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}



