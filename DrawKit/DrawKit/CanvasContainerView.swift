//
//  CanvasContainerView.swift
//  DrawKit
//
//  Created by Pankaj Dhiman on 28/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//

import UIKit

open class CanvasContainerView : UIView {
    
    let canvasSize: CGSize
    let canvasView: UIView
    
    public var documentView: UIView? {
        willSet {
            if let previousView = documentView {
                previousView.removeFromSuperview()
            }
        }
        didSet {
            if let newView = documentView {
                newView.frame = canvasView.bounds
                canvasView.addSubview(newView)
            }
        }
    }
    
    public required init(canvasSize: CGSize) {
        
        self.canvasSize = canvasSize
        
        let canvasFrame = CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height)
        
        canvasView = UIView(frame:canvasFrame)
        canvasView.backgroundColor = UIColor.white
        canvasView.translatesAutoresizingMaskIntoConstraints = true
        canvasView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        super.init(frame:canvasFrame)
        self.backgroundColor = UIColor.white
        self.addSubview(canvasView)
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
