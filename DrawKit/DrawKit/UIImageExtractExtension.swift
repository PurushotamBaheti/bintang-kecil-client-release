//
//  UIImageExtractExtension.swift
//  DrawKit
//
//  Created by Pankaj Dhiman on 13/08/17.
//  Copyright © 2017 Uyloo. All rights reserved.
//


import UIKit

public extension UIImage {
     func asPNGData() -> Data? {
        return self.pngData()
    }
    
     func asJPEGData(_ quality: CGFloat) -> Data? {
        return self.jpegData(compressionQuality: quality);
    }
    
     func asPNGImage() -> UIImage? {
        if let data = self.asPNGData() {
            return UIImage.init(data: data)
        }
        
        return nil
    }
    
     func asJPGImage(_ quality: CGFloat) -> UIImage? {
        if let data = self.asJPEGData(quality) {
            return UIImage.init(data: data)
        }
        
        return nil
    }
}
